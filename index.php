<?php include 'include/head-landing.php';?>

<?php include 'include/header-landing.php';?>

<section class="hero-banner">
	<div class="container">
		<div class="row align-items-center">
			<div class="hero-banner__col hero-banner__col--1">
				<div class="hero-banner__image">
					<img src="assets/images/hero-banner-mobile.png">
				</div>
				<div class="hero-banner__download d-flex align-items-center">
					<a href="#" class="d-flex align-items-center">
						<svg xmlns="http://www.w3.org/2000/svg" width="26.857" height="30" viewBox="0 0 26.857 30">
							<path class="a" d="M36.615,12.861,14.682.324A2.458,2.458,0,0,0,13.462,0h-.025c-.057,0-.113,0-.169.008l-.076.008q-.066.007-.132.018l-.048.007h0A2.459,2.459,0,0,0,11,2.462V27.538a2.459,2.459,0,0,0,2.014,2.42h0l.043.006q.071.012.141.02l.071.007c.056,0,.111.007.167.008h.028a2.458,2.458,0,0,0,1.22-.324L36.615,17.138A2.462,2.462,0,0,0,37.855,15h0A2.462,2.462,0,0,0,36.615,12.861Zm-8.674-2.49-2.181,2.861L20.212,5.953Zm7.611,4.907-15.34,8.769,9.6-12.6,5.735,3.278a.319.319,0,0,1,0,.554Z" transform="translate(-10.998)"/>
						</svg>
						<div class="hero-banner__button-text">
							<small>Download from</small>
							<span>PLAY STORE</span>
						</div>
					</a>
					<a href="#" class="d-flex align-items-center">
						<svg xmlns="http://www.w3.org/2000/svg" width="25.625" height="30" viewBox="0 0 25.625 30">
							<g transform="translate(-37.336)"><path class="a" d="M8.2,30c-2.2-.019-3.877-2.186-5.136-4.114C-.453,20.5-.824,14.175,1.346,10.811A7.781,7.781,0,0,1,7.608,7.026c2.327,0,3.791,1.284,5.719,1.284,1.869,0,3.007-1.288,5.7-1.288a7.594,7.594,0,0,1,5.732,3.043,6.614,6.614,0,0,0,.868,11.947,21.611,21.611,0,0,1-1.937,3.637c-1.258,1.927-3.03,4.329-5.229,4.346a4.869,4.869,0,0,1-2.295-.632,6.033,6.033,0,0,0-2.809-.632C10.725,28.745,10.163,30,8.241,30ZM12.385,7.219a6.115,6.115,0,0,1,1.493-4.751A7.04,7.04,0,0,1,18.436,0a6.6,6.6,0,0,1-1.451,4.869,5.986,5.986,0,0,1-4.469,2.351Z" transform="translate(37.336)"/></g>
						</svg>
						<div class="hero-banner__button-text">
							<small>Download from</small>
							<span>APP STORE</span>
						</div>
					</a>
				</div>
			</div>
			<div class="hero-banner__col hero-banner__col--2">
				<div class="hero-banner__content">
					<h1>Find or Create a Activities Nearby</h1>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
					<a href="#" class="btn-custom btn-black">Get Started</a>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="features bg-light">
	<div class="container">
		<div class="title">
			<h2>Features</h2>
			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type</p>
		</div>
		<div class="row justify-content-between">
			<div class="features__col">
				<div class="features__box box-shadow2 bg-white">
					<img src="assets/images/near-you.svg">
					<h3>Find Activities Near You</h3>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been</p>
				</div>
			</div>
			<div class="features__col">
				<div class="features__box box-shadow2 bg-white">
					<img src="assets/images/create-new-activity.svg">
					<h3>Create New Activity</h3>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been</p>
				</div>
			</div>
			<div class="features__col">
				<div class="features__box box-shadow2 bg-white">
					<img src="assets/images/connect-participants.svg">
					<h3>Connect with participants</h3>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been</p>
				</div>
			</div>
			<div class="features__col">
				<div class="features__box box-shadow2 bg-white">
					<img src="assets/images/participate-tournament.svg">
					<h3>Participate in Tournament</h3>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been</p>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="testimonial">
	<div class="container">
		<div class="title">
			<h2>Testimonials</h2>
		</div>
		<div class="row align-items-center">
			<div class="testimonial__col testimonial__col--1">
				<div class="testimonial__images">
					<div class="testimonial__image-row testimonial__image-row--1">
						<div class="testimonial__image-box">
							<a href="#testimonial1"><img src="assets/images/client1.jpg"></a>
						</div>
						<div class="testimonial__image-box">
							<a href="#testimonial2"><img src="assets/images/client2.jpg"></a>
						</div>
						<div class="testimonial__image-box">
							<a href="#testimonial3"><img src="assets/images/client3.jpg"></a>
						</div>
					</div>
					<div class="testimonial__image-row testimonial__image-row--2">
						<div class="testimonial__image-box">
							<a href="#testimonial4"><img src="assets/images/client4.jpg"></a>
						</div>
						<div class="testimonial__image-box">
							<a href="#testimonial5"><img src="assets/images/client5.jpg"></a>
						</div>
						<div class="testimonial__image-box">
							<a href="#testimonial6"><img src="assets/images/client6.jpg"></a>
						</div>
						<div class="testimonial__image-box">
							<a href="#testimonial7"><img src="assets/images/client7.jpg"></a>
						</div>
					</div>
					<div class="testimonial__image-row testimonial__image-row--3">
						<div class="testimonial__image-box">
							<a href="#testimonial8"><img src="assets/images/client8.jpg"></a>
						</div>
						<div class="testimonial__image-box">
							<a href="#testimonial9"><img src="assets/images/client9.jpg"></a>
						</div>
						<div class="testimonial__image-box">
							<a href="#testimonial10"><img src="assets/images/client10.jpg"></a>
						</div>
						<div class="testimonial__image-box">
							<a href="#testimonial11"><img src="assets/images/client11.jpg"></a>
						</div>
					</div>
					<div class="testimonial__image-row testimonial__image-row--4">
						<div class="testimonial__image-box">
							<a href="#testimonial12"><img src="assets/images/client12.jpg"></a>
						</div>
						<div class="testimonial__image-box">
							<a href="#testimonial13"><img src="assets/images/client13.jpg"></a>
						</div>
						<div class="testimonial__image-box">
							<a href="#testimonial14"><img src="assets/images/client14.jpg"></a>
						</div>
					</div>
				</div>
			</div>
			<div class="testimonial__col testimonial__col--2">
				<div class="testimonial__content-wrap">
					<div class="testimonial__content-block box-shadow2" id="testimonial1">
						<div class="testimonial__content">
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed interdum id lectus ac eleifend. Fusce tincidunt erat lectus, eget euismod nibh porta nec. Donec sed felis pellentesque, vulputate sapien vitae, pulvinar lectus. Maecenas vitae vestibulum turpis, sit amet tincidunt sem. Nunc ac nibh at nisi imperdiet pellentesque. Proin vestibulum sit amet metus eu tristique. Nulla sed suscipit arcu. Phasellus sodales, ex vel eleifend porta, massa dui</p>
							<div class="testimonial__client d-flex align-items-center justify-content-center">
								<img src="assets/images/client1.jpg">
								<span>Alexander	Peake</span>
							</div>
						</div>
					</div>
					<div class="testimonial__content-block box-shadow2" id="testimonial2">
						<div class="testimonial__content">
							<p>Lorem ipsum Donec sed felis pellentesque, vulputate sapien vitae, pulvinar lectus. Maecenas vitae vestibulum turpis, sit amet tincidunt sem. Nunc ac nibh at nisi imperdiet pellentesque. Proin vestibulum sit amet metus eu tristique. Nulla sed suscipit arcu. Phasellus sodales, ex vel eleifend porta, massa dui</p>
							<div class="testimonial__client d-flex align-items-center justify-content-center">
								<img src="assets/images/client2.jpg">
								<span>Gavin	Nolan</span>
							</div>
						</div>
					</div>
					<div class="testimonial__content-block box-shadow2" id="testimonial3">
						<div class="testimonial__content">
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed interdum id lectus ac eleifend. Fusce tincidunt erat lectus, eget euismod nibh porta nec. Donec sed felis pellentesque, vulputate sapien vitae, pulvinar lectus. Maecenas vitae vestibulum turpis, sit amet tincidunt sem.</p>
							<div class="testimonial__client d-flex align-items-center justify-content-center">
								<img src="assets/images/client3.jpg">
								<span>Elizabeth	Bower</span>
							</div>
						</div>
					</div>
					<div class="testimonial__content-block box-shadow2" id="testimonial4">
						<div class="testimonial__content">
							<p>Nunc ac nibh at nisi imperdiet pellentesque. Proin vestibulum sit amet metus eu tristique. Nulla sed suscipit arcu. Phasellus sodales, ex vel eleifend porta, massa dui</p>
							<div class="testimonial__client d-flex align-items-center justify-content-center">
								<img src="assets/images/client4.jpg">
								<span>James	Underwood</span>
							</div>
						</div>
					</div>
					<div class="testimonial__content-block box-shadow2" id="testimonial5">
						<div class="testimonial__content">
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed interdum id lectus ac eleifend. Fusce tincidunt erat lectus, eget euismod nibh porta nec. Donec sed felis pellentesque, vulputate sapien vitae, pulvinar lectus. Maecenas vitae vestibulum turpis, sit amet tincidunt sem. Nunc ac nibh at nisi imperdiet pellentesque. Proin vestibulum sit amet metus eu tristique. Nulla sed suscipit arcu. Phasellus sodales, ex vel eleifend porta, massa dui</p>
							<div class="testimonial__client d-flex align-items-center justify-content-center">
								<img src="assets/images/client5.jpg">
								<span>Dan Hill</span>
							</div>
						</div>
					</div>
					<div class="testimonial__content-block box-shadow2" id="testimonial6">
						<div class="testimonial__content">
							<p>euismod nibh porta nec. Donec sed felis pellentesque, vulputate sapien vitae, pulvinar lectus. Maecenas vitae vestibulum turpis, sit amet tincidunt sem. Nunc ac nibh at nisi imperdiet pellentesque. Proin vestibulum sit amet metus eu tristique. Nulla sed suscipit arcu. Phasellus sodales, ex vel eleifend porta, massa dui</p>
							<div class="testimonial__client d-flex align-items-center justify-content-center">
								<img src="assets/images/client6.jpg">
								<span>Audie Yose</span>
							</div>
						</div>
					</div>
					<div class="testimonial__content-block box-shadow2" id="testimonial7">
						<div class="testimonial__content">
							<p>Lorem ipsum Donec sed felis pellentesque, vulputate sapien vitae, pulvinar lectus. Maecenas vitae vestibulum turpis, sit amet tincidunt sem. Nunc ac nibh at nisi imperdiet pellentesque. Proin vestibulum sit amet metus eu tristique. Nulla sed suscipit arcu. Phasellus sodales, ex vel eleifend porta, massa dui</p>
							<div class="testimonial__client d-flex align-items-center justify-content-center">
								<img src="assets/images/client7.jpg">
								<span>Austin Peake</span>
							</div>
						</div>
					</div>
					<div class="testimonial__content-block box-shadow2" id="testimonial8">
						<div class="testimonial__content">
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed interdum id lectus ac eleifend. Fusce tincidunt erat lectus, eget euismod nibh porta nec. Donec sed felis pellentesque, vulputate sapien vitae, pulvinar lectus. Maecenas vitae vestibulum turpis, sit amet tincidunt sem.</p>
							<div class="testimonial__client d-flex align-items-center justify-content-center">
								<img src="assets/images/client8.jpg">
								<span>Barry Kade</span>
							</div>
						</div>
					</div>
					<div class="testimonial__content-block box-shadow2" id="testimonial9">
						<div class="testimonial__content">
							<p>Nunc ac nibh at nisi imperdiet pellentesque. Proin vestibulum sit amet metus eu tristique. Nulla sed suscipit arcu. Phasellus sodales, ex vel eleifend porta, massa dui</p>
							<div class="testimonial__client d-flex align-items-center justify-content-center">
								<img src="assets/images/client9.jpg">
								<span>MJoe	White</span>
							</div>
						</div>
					</div>
					<div class="testimonial__content-block box-shadow2" id="testimonial10">
						<div class="testimonial__content">
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed interdum id lectus ac eleifend. Fusce tincidunt erat lectus, eget euismod nibh porta nec. Donec sed felis pellentesque, vulputate sapien vitae, pulvinar lectus. Maecenas vitae vestibulum turpis, ex vel eleifend porta, massa dui</p>
							<div class="testimonial__client d-flex align-items-center justify-content-center">
								<img src="assets/images/client10.jpg">
								<span>Robert Lawrence</span>
							</div>
						</div>
					</div>
					<div class="testimonial__content-block box-shadow2" id="testimonial11">
						<div class="testimonial__content">
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed interdum id lectus ac eleifend. Fusce tincidunt erat lectus, eget euismod nibh porta nec. Donec sed felis pellentesque, vulputate sapien vitae, pulvinar lectus. Maecenas vitae vestibulum turpis, sit amet tincidunt sem. Nunc ac nibh at nisi imperdiet pellentesque. Proin vestibulum sit amet metus eu tristique. Nulla sed suscipit arcu. Phasellus sodales, ex vel eleifend porta, massa dui</p>
							<div class="testimonial__client d-flex align-items-center justify-content-center">
								<img src="assets/images/client11.jpg">
								<span>Jacob	Stewart</span>
							</div>
						</div>
					</div>
					<div class="testimonial__content-block box-shadow2" id="testimonial12">
						<div class="testimonial__content">
							<p>Lorem ipsum Donec sed felis pellentesque, vulputate sapien vitae, pulvinar lectus. Maecenas vitae vestibulum turpis, sit amet tincidunt sem. Nunc ac nibh at nisi imperdiet pellentesque. Proin vestibulum sit amet metus eu tristique. Nulla sed suscipit arcu. Phasellus sodales, ex vel eleifend porta, massa dui</p>
							<div class="testimonial__client d-flex align-items-center justify-content-center">
								<img src="assets/images/client12.jpg">
								<span>Fiona	Grant</span>
							</div>
						</div>
					</div>
					<div class="testimonial__content-block box-shadow2" id="testimonial13">
						<div class="testimonial__content">
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed interdum id lectus ac eleifend. Fusce tincidunt erat lectus, eget euismod nibh porta nec. Donec sed felis pellentesque, vulputate sapien vitae, pulvinar lectus. Maecenas vitae vestibulum turpis, sit amet tincidunt sem.</p>
							<div class="testimonial__client d-flex align-items-center justify-content-center">
								<img src="assets/images/client13.jpg">
								<span>Edward Wilson</span>
							</div>
						</div>
					</div>
					<div class="testimonial__content-block box-shadow2" id="testimonial14">
						<div class="testimonial__content">
							<p>Nunc ac nibh at nisi imperdiet pellentesque. Proin vestibulum sit amet metus eu tristique. Nulla sed suscipit arcu. Phasellus sodales, ex vel eleifend porta, massa dui</p>
							<div class="testimonial__client d-flex align-items-center justify-content-center">
								<img src="assets/images/client14.jpg">
								<span>Colin	Watson</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="downloadapp bg-light">
	<div class="container">
		<div class="row align-items-center">
			<div class="downloadapp__col downloadapp__col--1">
				<div class="downloadapp__content">
					<div class="title">
						<h2>Download Our Mobile App</h2>
					</div>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed interdum id lectus ac eleifend. Fusce tincidunt erat lectus, eget euismod nibh porta nec. Donec sed felis pellentesque, vulputate sapien vitae, pulvinar lectus. Maecenas vitae vestibulum turpis, sit amet tincidunt sem. Nunc ac nibh at nisi imperdiet pellentesque.</p>
					<div class="downloadapp__buttons d-flex align-items-center">
						<a href="#" class="d-flex align-items-center">
							<svg xmlns="http://www.w3.org/2000/svg" width="26.857" height="30" viewBox="0 0 26.857 30">
								<path class="a" d="M36.615,12.861,14.682.324A2.458,2.458,0,0,0,13.462,0h-.025c-.057,0-.113,0-.169.008l-.076.008q-.066.007-.132.018l-.048.007h0A2.459,2.459,0,0,0,11,2.462V27.538a2.459,2.459,0,0,0,2.014,2.42h0l.043.006q.071.012.141.02l.071.007c.056,0,.111.007.167.008h.028a2.458,2.458,0,0,0,1.22-.324L36.615,17.138A2.462,2.462,0,0,0,37.855,15h0A2.462,2.462,0,0,0,36.615,12.861Zm-8.674-2.49-2.181,2.861L20.212,5.953Zm7.611,4.907-15.34,8.769,9.6-12.6,5.735,3.278a.319.319,0,0,1,0,.554Z" transform="translate(-10.998)"></path>
							</svg>
							<div class="downloadapp__button-text">
								<small>Download from</small>
								<span>PLAY STORE</span>
							</div>
						</a>
						<a href="#" class="d-flex align-items-center">
							<svg xmlns="http://www.w3.org/2000/svg" width="25.625" height="30" viewBox="0 0 25.625 30">
								<g transform="translate(-37.336)"><path class="a" d="M8.2,30c-2.2-.019-3.877-2.186-5.136-4.114C-.453,20.5-.824,14.175,1.346,10.811A7.781,7.781,0,0,1,7.608,7.026c2.327,0,3.791,1.284,5.719,1.284,1.869,0,3.007-1.288,5.7-1.288a7.594,7.594,0,0,1,5.732,3.043,6.614,6.614,0,0,0,.868,11.947,21.611,21.611,0,0,1-1.937,3.637c-1.258,1.927-3.03,4.329-5.229,4.346a4.869,4.869,0,0,1-2.295-.632,6.033,6.033,0,0,0-2.809-.632C10.725,28.745,10.163,30,8.241,30ZM12.385,7.219a6.115,6.115,0,0,1,1.493-4.751A7.04,7.04,0,0,1,18.436,0a6.6,6.6,0,0,1-1.451,4.869,5.986,5.986,0,0,1-4.469,2.351Z" transform="translate(37.336)"/></g>
							</svg>
							<div class="downloadapp__button-text">
								<small>Download from</small>
								<span>APP STORE</span>
							</div>
						</a>
					</div>
				</div>
			</div>
			<div class="downloadapp__col downloadapp__col--2">
				<div class="downloadapp__image">
					<img src="assets/images/download-app-mobile.png">
				</div>
			</div>
		</div>
	</div>
</section>

<?php include 'include/footer-landing.php';?>