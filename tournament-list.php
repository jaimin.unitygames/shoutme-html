<?php include 'include/head.php'; ?>

<?php include 'include/header.php'; ?>

<div class="my-activity tournament">
	<div class="container">
		<div class="row">
			<div class="col-lg-9 col-md-8 order-2 order-md-1">
				<nav>
					<div class="nav nav-tabs" id="nav-tab" role="tablist">
						<a class="nav-link active" id="upcoming-tab" data-toggle="tab" href="#upcoming" role="tab" aria-controls="upcoming" aria-selected="true">Upcoming</a>
						<a class="nav-link" id="joined-tab" data-toggle="tab" href="#joined" role="tab" aria-controls="joined" aria-selected="false">Joined</a>
					</div>
				</nav>
			</div>
			<div class="col-lg-3 col-md-4 order-1 order-md-2">
				<div class="text-center text-md-right">
					<div class="activity-filter-icon">
						<img src="assets/images/filter-icon.svg">
						Filter
					</div>
				</div>
				<div class="filter-sidebar">
					<img class="close" src="assets/images/close-icon.svg">
					<a class="clearall" href="#">Clear All</a>
					<div class="top">
						<h3>Tournament Filter</h3>
					</div>
					<div class="filter-sidebar__form">
						<div class="form-group time-available">
							<label>Time Availability</label>
							<div class="input-group d-flex flex-wrap align-items-center justify-content-between">
								<input type="text" class="timepicker form-control" placeholder="hh:mm">
								<span>to</span>
								<input type="text" class="timepicker form-control" placeholder="hh:mm">
							</div>
						</div>
						<div class="form-group select-age">
							<label>Select Age</label>
							<div class="input-group d-flex flex-wrap align-items-center justify-content-between">
								<select class="form-control">
									<option value="selected">From</option>
									<option value="10">10</option>
									<option value="11">11</option>
									<option value="12">12</option>
									<option value="13">13</option>
									<option value="14">14</option>
									<option value="15">15</option>
									<option value="16">16</option>
									<option value="17">17</option>
									<option value="18">18</option>
									<option value="19">19</option>
									<option value="20">20</option>
									<option value="21">21</option>
									<option value="22">22</option>
									<option value="23">23</option>
									<option value="24">24</option>
									<option value="25">25</option>
									<option value="26">26</option>
									<option value="27">27</option>
									<option value="28">28</option>
									<option value="29">29</option>
									<option value="30">30</option>
									<option value="31">31</option>
									<option value="32">32</option>
									<option value="33">33</option>
									<option value="34">34</option>
									<option value="35">35</option>
									<option value="36">36</option>
									<option value="37">37</option>
									<option value="38">38</option>
									<option value="39">39</option>
									<option value="40">40</option>
								</select>
								<span>to</span>
								<select class="form-control">
									<option value="selected">To</option>
									<option value="10">10</option>
									<option value="11">11</option>
									<option value="12">12</option>
									<option value="13">13</option>
									<option value="14">14</option>
									<option value="15">15</option>
									<option value="16">16</option>
									<option value="17">17</option>
									<option value="18">18</option>
									<option value="19">19</option>
									<option value="20">20</option>
									<option value="21">21</option>
									<option value="22">22</option>
									<option value="23">23</option>
									<option value="24">24</option>
									<option value="25">25</option>
									<option value="26">26</option>
									<option value="27">27</option>
									<option value="28">28</option>
									<option value="29">29</option>
									<option value="30">30</option>
									<option value="31">31</option>
									<option value="32">32</option>
									<option value="33">33</option>
									<option value="34">34</option>
									<option value="35">35</option>
									<option value="36">36</option>
									<option value="37">37</option>
									<option value="38">38</option>
									<option value="39">39</option>
									<option value="40">40</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label>Gender</label>
							<div class="input-group d-flex flex-wrap radio-custom">
								<div>
									<input type="radio" id="any" name="gender" value="any" checked="true">
									<label for="any">Any</label>
								</div>
								<div>
									<input type="radio" id="male" name="gender" value="male">
									<label for="male">Male</label>
								</div>
								<div>
									<input type="radio" id="female" name="gender" value="female">
									<label for="female">Female</label>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label>Interested Activities</label>
							<div class="multiple-select">
								<div class="individual-activity">
									<input type="checkbox" id="football" name="activity" value="football">
									<label for="football">Football</label>
								</div>
								<div class="individual-activity">
									<input type="checkbox" id="basketball" name="activity" value="basketball">
									<label for="basketball">Basketball</label>
								</div>
								<div class="individual-activity">
									<input type="checkbox" id="hockey" name="activity" value="hockey">
									<label for="hockey">Hockey</label>
								</div>
								<div class="individual-activity">
									<input type="checkbox" id="rugby" name="activity" value="rugby">
									<label for="rugby">Rugby</label>
								</div>
								<div class="individual-activity">
									<input type="checkbox" id="cricket" name="activity" value="cricket">
									<label for="cricket">Cricket</label>
								</div>
								<div class="individual-activity">
									<input type="checkbox" id="tennis" name="activity" value="tennis">
									<label for="tennis">Tennis</label>
								</div>
							</div>
						</div>
						<div class="form-group">
							<button type="submit" class="btn-custom btn-black">Save <img src="assets/images/arrow.svg"></button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="tab-content" id="nav-tabContent">
			<div class="tab-pane fade show active" id="upcoming" role="tabpanel" aria-labelledby="upcoming-tab">
				<div class="row">
					<div class="col-md-6 col-lg-4 d-flex">
						<div class="tournament__box">
							<a href="#">
								<div class="tournament__box-top">
										<h3>The London Cup</h3>
										<div class="d-flex align-items-center">
											<img src="assets/images/football-ball-icon.svg" alt="">
											Football
										</div>
								</div>
								<div class="tournament__box-center">
									<div class="d-flex align-items-center">
										<img src="assets/images/timer-icon.svg">
										12th Nov 2020 to 20th Nov 2020
									</div>
								</div>
								<div class="tournament__box-bottom">
									<div class="left">
										<div class="box green">
											<img src="assets/images/money-icon.svg">£10
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>
					<div class="col-md-6 col-lg-4 d-flex">
						<div class="tournament__box">
							<a href="#">
								<div class="tournament__box-top">
										<h3>The London Cup</h3>
										<div class="d-flex align-items-center">
											<img src="assets/images/football-ball-icon.svg" alt="">
											Football
										</div>
								</div>
								<div class="tournament__box-center">
									<div class="d-flex align-items-center">
										<img src="assets/images/timer-icon.svg">
										12th Nov 2020 to 20th Nov 2020
									</div>
								</div>
								<div class="tournament__box-bottom">
									<div class="left">
										<div class="box green">
											<img src="assets/images/money-icon.svg">£10
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>
					<div class="col-md-6 col-lg-4 d-flex">
						<div class="tournament__box">
							<a href="#">
								<div class="tournament__box-top">
										<h3>The London Cup</h3>
										<div class="d-flex align-items-center">
											<img src="assets/images/football-ball-icon.svg" alt="">
											Football
										</div>
								</div>
								<div class="tournament__box-center">
									<div class="d-flex align-items-center">
										<img src="assets/images/timer-icon.svg">
										12th Nov 2020 to 20th Nov 2020
									</div>
								</div>
								<div class="tournament__box-bottom">
									<div class="left">
										<div class="box green">
											<img src="assets/images/money-icon.svg">£10
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>
					<div class="col-md-6 col-lg-4 d-flex">
						<div class="tournament__box">
							<a href="#">
								<div class="tournament__box-top">
										<h3>The London Cup</h3>
										<div class="d-flex align-items-center">
											<img src="assets/images/football-ball-icon.svg" alt="">
											Football
										</div>
								</div>
								<div class="tournament__box-center">
									<div class="d-flex align-items-center">
										<img src="assets/images/timer-icon.svg">
										12th Nov 2020 to 20th Nov 2020
									</div>
								</div>
								<div class="tournament__box-bottom">
									<div class="left">
										<div class="box green">
											<img src="assets/images/money-icon.svg">£10
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>
					<div class="col-md-6 col-lg-4 d-flex">
						<div class="tournament__box">
							<a href="#">
								<div class="tournament__box-top">
										<h3>The London Cup</h3>
										<div class="d-flex align-items-center">
											<img src="assets/images/football-ball-icon.svg" alt="">
											Football
										</div>
								</div>
								<div class="tournament__box-center">
									<div class="d-flex align-items-center">
										<img src="assets/images/timer-icon.svg">
										12th Nov 2020 to 20th Nov 2020
									</div>
								</div>
								<div class="tournament__box-bottom">
									<div class="left">
										<div class="box green">
											<img src="assets/images/money-icon.svg">£10
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>
					<div class="col-md-6 col-lg-4 d-flex">
						<div class="tournament__box">
							<a href="#">
								<div class="tournament__box-top">
										<h3>The London Cup</h3>
										<div class="d-flex align-items-center">
											<img src="assets/images/football-ball-icon.svg" alt="">
											Football
										</div>
								</div>
								<div class="tournament__box-center">
									<div class="d-flex align-items-center">
										<img src="assets/images/timer-icon.svg">
										12th Nov 2020 to 20th Nov 2020
									</div>
								</div>
								<div class="tournament__box-bottom">
									<div class="left">
										<div class="box green">
											<img src="assets/images/money-icon.svg">£10
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>
				</div>
			</div>
			<div class="tab-pane fade" id="joined" role="tabpanel" aria-labelledby="joined-tab">
				<div class="row">
					<div class="col-md-6 col-lg-4 d-flex">
						<div class="tournament__box">
							<a href="#">
								<div class="tournament__box-top">
										<h3>The London Cup</h3>
										<div class="d-flex align-items-center">
											<img src="assets/images/football-ball-icon.svg" alt="">
											Football
										</div>
								</div>
								<div class="tournament__box-center">
									<div class="d-flex align-items-center">
										<img src="assets/images/timer-icon.svg">
										12th Nov 2020 to 20th Nov 2020
									</div>
								</div>
								<div class="tournament__box-bottom">
									<div class="left">
										<div class="box green">
											<img src="assets/images/money-icon.svg">£10
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>
					<div class="col-md-6 col-lg-4 d-flex">
						<div class="tournament__box">
							<a href="#">
								<div class="tournament__box-top">
										<h3>The London Cup</h3>
										<div class="d-flex align-items-center">
											<img src="assets/images/football-ball-icon.svg" alt="">
											Football
										</div>
								</div>
								<div class="tournament__box-center">
									<div class="d-flex align-items-center">
										<img src="assets/images/timer-icon.svg">
										12th Nov 2020 to 20th Nov 2020
									</div>
								</div>
								<div class="tournament__box-bottom">
									<div class="left">
										<div class="box green">
											<img src="assets/images/money-icon.svg">£10
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>
					<div class="col-md-6 col-lg-4 d-flex">
						<div class="tournament__box">
							<a href="#">
								<div class="tournament__box-top">
										<h3>The London Cup</h3>
										<div class="d-flex align-items-center">
											<img src="assets/images/football-ball-icon.svg" alt="">
											Football
										</div>
								</div>
								<div class="tournament__box-center">
									<div class="d-flex align-items-center">
										<img src="assets/images/timer-icon.svg">
										12th Nov 2020 to 20th Nov 2020
									</div>
								</div>
								<div class="tournament__box-bottom">
									<div class="left">
										<div class="box green">
											<img src="assets/images/money-icon.svg">£10
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>
					<div class="col-md-6 col-lg-4 d-flex">
						<div class="tournament__box">
							<a href="#">
								<div class="tournament__box-top">
										<h3>The London Cup</h3>
										<div class="d-flex align-items-center">
											<img src="assets/images/football-ball-icon.svg" alt="">
											Football
										</div>
								</div>
								<div class="tournament__box-center">
									<div class="d-flex align-items-center">
										<img src="assets/images/timer-icon.svg">
										12th Nov 2020 to 20th Nov 2020
									</div>
								</div>
								<div class="tournament__box-bottom">
									<div class="left">
										<div class="box green">
											<img src="assets/images/money-icon.svg">£10
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>
					<div class="col-md-6 col-lg-4 d-flex">
						<div class="tournament__box">
							<a href="#">
								<div class="tournament__box-top">
										<h3>The London Cup</h3>
										<div class="d-flex align-items-center">
											<img src="assets/images/football-ball-icon.svg" alt="">
											Football
										</div>
								</div>
								<div class="tournament__box-center">
									<div class="d-flex align-items-center">
										<img src="assets/images/timer-icon.svg">
										12th Nov 2020 to 20th Nov 2020
									</div>
								</div>
								<div class="tournament__box-bottom">
									<div class="left">
										<div class="box green">
											<img src="assets/images/money-icon.svg">£10
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>
					<div class="col-md-6 col-lg-4 d-flex">
						<div class="tournament__box">
							<a href="#">
								<div class="tournament__box-top">
										<h3>The London Cup</h3>
										<div class="d-flex align-items-center">
											<img src="assets/images/football-ball-icon.svg" alt="">
											Football
										</div>
								</div>
								<div class="tournament__box-center">
									<div class="d-flex align-items-center">
										<img src="assets/images/timer-icon.svg">
										12th Nov 2020 to 20th Nov 2020
									</div>
								</div>
								<div class="tournament__box-bottom">
									<div class="left">
										<div class="box green">
											<img src="assets/images/money-icon.svg">£10
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php include 'include/footer.php'; ?>