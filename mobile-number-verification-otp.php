<?php include 'include/head-login-register.php';?>

<?php include 'include/header-login-register.php';?>

<section class="login-register">
	<div class="container">
		<div class="multistep-form">
			<div class="multistep-top d-flex flex-wrap justify-content-between">
				<div class="multistep-top__box multistep-top__box--active">
					<a href="#">
						<div class="multistep-top__icon">
							<svg xmlns="http://www.w3.org/2000/svg" width="11.672" height="20" viewBox="0 0 11.672 20">
								<defs><style>.mbnumv{fill-rule:evenodd;}</style></defs><g transform="translate(-74 -8)"><g transform="translate(74 8)">
								<path class="mbnumv" d="M76.145,8H83.59a2.156,2.156,0,0,1,2.082,2.145v15.71A2.114,2.114,0,0,1,83.59,28H76.145A2.127,2.127,0,0,1,74,25.855V10.145A2.168,2.168,0,0,1,76.145,8Zm3.722,16.972a1.075,1.075,0,0,1,0,2.145,1.073,1.073,0,1,1,0-2.145ZM76.145,10.461H83.59a.622.622,0,0,1,.568.631V23.457a.565.565,0,0,1-.568.568H76.145a.583.583,0,0,1-.631-.568V11.091A.632.632,0,0,1,76.145,10.461Zm2.713-1.451H81a.284.284,0,0,1,0,.568H78.858A.287.287,0,1,1,78.858,9.009Z" transform="translate(-74 -8)"/></g></g>
							</svg>
						</div>
						<span>Mobile Number Verification</span>
					</a>
				</div>
				<div class="multistep-top__box">
					<a href="#">
						<div class="multistep-top__icon">
							<svg xmlns="http://www.w3.org/2000/svg" width="17.99" height="20" viewBox="0 0 17.99 20">
								<g class="a" transform="translate(17 9.501)"><g transform="translate(-17 -9.501)"><path class="b" d="M.485,20l-.192-.752a8.9,8.9,0,0,1,3.52-9.565,6.972,6.972,0,0,0,10.375,0A8.915,8.915,0,0,1,17.7,19.254L17.5,20ZM4,5a5,5,0,1,1,5,5A5,5,0,0,1,4,5Z" transform="translate(0)"/></g></g>
							</svg>
						</div>
						<span>About Yourself</span>
					</a>
				</div>
				<div class="multistep-top__box">
					<a href="#">
						<div class="multistep-top__icon">
							<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20">
								<g class="a" transform="translate(-3 -2)"><g transform="translate(3 2)"><g transform="translate(0 0)"><path class="b" d="M3,203.474v-5.263a1.053,1.053,0,0,1,1.053-1.053H9.316v-1.053a2.105,2.105,0,0,1,4.211,0v1.053h5.263a1.053,1.053,0,0,1,1.053,1.053v5.263H20.9a2.1,2.1,0,0,1,2.1,2.1v0a2.106,2.106,0,0,1-2.105,2.105H19.842v5.263A1.053,1.053,0,0,1,18.789,214H13.526v-1.053a2.105,2.105,0,0,0-4.211,0V214H4.053A1.053,1.053,0,0,1,3,212.947v-5.263H4.053a2.105,2.105,0,0,0,0-4.211Z" transform="translate(-3 -194)"/></g></g></g>
							</svg>
						</div>
						<span>Interested Activities</span>
					</a>
				</div>
				<div class="multistep-top__box">
					<a href="#">
						<div class="multistep-top__icon">
							<svg xmlns="http://www.w3.org/2000/svg" width="17.471" height="20" viewBox="0 0 17.471 20">
								<g class="a" transform="translate(-8.5 -2.5)"><path class="b" d="M24.074,2.5H10.418A1.911,1.911,0,0,0,8.5,4.418V20.582A1.911,1.911,0,0,0,10.418,22.5H24.053a1.911,1.911,0,0,0,1.918-1.918V4.418A1.893,1.893,0,0,0,24.074,2.5ZM13.347,18.074h-.3a.969.969,0,1,1,0-1.939h.3a.969.969,0,0,1,0,1.939Zm0-4.594h-.3a.969.969,0,0,1,0-1.939h.3a.969.969,0,0,1,0,1.939Zm0-4.594h-.3a.967.967,0,0,1-.969-.969.954.954,0,0,1,.969-.969h.3a.967.967,0,0,1,.969.969A.954.954,0,0,1,13.347,8.886Zm8.093,9.189H16.593a.969.969,0,1,1,0-1.939h4.868a.967.967,0,0,1,.969.969A1.014,1.014,0,0,1,21.44,18.074Zm0-4.594H16.593a.969.969,0,1,1,0-1.939h4.868a.967.967,0,0,1,.969.969A1.014,1.014,0,0,1,21.44,13.48Zm0-4.594H16.593a.967.967,0,0,1-.969-.969.954.954,0,0,1,.969-.969h4.868a.967.967,0,0,1,.969.969A.985.985,0,0,1,21.44,8.886Z"/></g>
							</svg>
						</div>
						<span>Final Step</span>
					</a>
				</div>
			</div>
			<div class="multistep-bottom step1">
				<div class="wid-335" style="max-width: 435px;">
					<div class="mobile-otp-icon">
						<img src="assets/images/otp-number.svg">
					</div>
					<div class="form-group mobile-otp">
						<label>Please Enter One Time Password we sent on +44 000 000 0000</label>
						<div class="input-group d-flex justify-content-between">
							<input type="text" name="" class="form-control">
							<input type="text" name="" class="form-control">
							<input type="text" name="" class="form-control">
							<input type="text" name="" class="form-control">
							<input type="text" name="" class="form-control">
							<input type="text" name="" class="form-control">
						</div>
					</div>
					<div class="form-group orange-link">
						<a href="#">Resend OTP</a>
					</div>
					<div class="form-group">
						<button type="submit" class="btn-custom btn-black">Submit <img src="assets/images/arrow.svg"></button>
					</div>
				</div>
			</div>
		</div>
	</div>

</section>

<?php include 'include/footer-login-register.php';?>