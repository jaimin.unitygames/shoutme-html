<?php include 'include/head.php'; ?>

<?php include 'include/header.php'; ?>

<div class="my-activity tournament">
	<div class="container">
		<div class="tournament-schedule">
			<div class="tournament-schedule__top">
				<div class="backbtn">
					<a href="tournament-detail.php" class="btn-custom white-border-btn d-inline-block"><img src="assets/images/arrow-reverse.svg"> Back to Tournament</a>
				</div>
				<div class="white-title">
					<h2>Points Table</h2>
				</div>
			</div>
			<div class="tournament-pointtable">
				<div class="row">
					<div class="col-lg-4 col-sm-6">
						<div class="tournament-pointtable__box w-100">
							<div class="tournament-pointtable__group-name" id="pointtable1">
								<a role="button" data-toggle="collapse" href="#pointtablecollapse1" aria-expanded="true" aria-controls="pointtablecollapse1">
									Group 1<span>(You)</span>
								</a>
							</div>
							<div id="pointtablecollapse1" class="collapse multi-collapse show" aria-labelledby="pointtable1">
								<div class="tournament-pointtable__content">
									<div class="tournament-pointtable__team">
										<h3>Team Name</h3>
										<div class="box">
											<ul class="row1">
												<li><strong>Total Matches</strong>12</li>
												<li><strong>Points</strong>10</li>
											</ul>
											<ul class="row2">
												<li><strong>Win</strong>12</li>
												<li><strong>Loss</strong>12</li>
												<li><strong>Draw</strong>1</li>
											</ul>
										</div>
									</div>
									<div class="tournament-pointtable__team">
										<h3>Team Name</h3>
										<div class="box">
											<ul class="row1">
												<li><strong>Total Matches</strong>12</li>
												<li><strong>Points</strong>10</li>
											</ul>
											<ul class="row2">
												<li><strong>Win</strong>12</li>
												<li><strong>Loss</strong>12</li>
												<li><strong>Draw</strong>1</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-4 col-sm-6">
						<div class="tournament-pointtable__box w-100">
							<div class="tournament-pointtable__group-name" id="pointtable2">
								<a class="collapsed" role="button" data-toggle="collapse" href="#pointtablecollapse2" aria-expanded="false" aria-controls="pointtablecollapse2">
									Group 2
								</a>
							</div>
							<div id="pointtablecollapse2" class="collapse multi-collapse" aria-labelledby="pointtable2">
								<div class="tournament-pointtable__content">
									<div class="tournament-pointtable__team">
										<h3>Team Name</h3>
										<div class="box">
											<ul class="row1">
												<li><strong>Total Matches</strong>12</li>
												<li><strong>Points</strong>10</li>
											</ul>
											<ul class="row2">
												<li><strong>Win</strong>12</li>
												<li><strong>Loss</strong>12</li>
												<li><strong>Draw</strong>1</li>
											</ul>
										</div>
									</div>
									<div class="tournament-pointtable__team">
										<h3>Team Name</h3>
										<div class="box">
											<ul class="row1">
												<li><strong>Total Matches</strong>12</li>
												<li><strong>Points</strong>10</li>
											</ul>
											<ul class="row2">
												<li><strong>Win</strong>12</li>
												<li><strong>Loss</strong>12</li>
												<li><strong>Draw</strong>1</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-4 col-sm-6">
						<div class="tournament-pointtable__box w-100">
							<div class="tournament-pointtable__group-name" id="pointtable3">
								<a class="collapsed" role="button" data-toggle="collapse" href="#pointtablecollapse3" aria-expanded="false" aria-controls="pointtablecollapse3">
									Group 3
								</a>
							</div>
							<div id="pointtablecollapse3" class="collapse multi-collapse" aria-labelledby="pointtable3">
								<div class="tournament-pointtable__content">
									<div class="tournament-pointtable__team">
										<h3>Team Name</h3>
										<div class="box">
											<ul class="row1">
												<li><strong>Total Matches</strong>12</li>
												<li><strong>Points</strong>10</li>
											</ul>
											<ul class="row2">
												<li><strong>Win</strong>12</li>
												<li><strong>Loss</strong>12</li>
												<li><strong>Draw</strong>1</li>
											</ul>
										</div>
									</div>
									<div class="tournament-pointtable__team">
										<h3>Team Name</h3>
										<div class="box">
											<ul class="row1">
												<li><strong>Total Matches</strong>12</li>
												<li><strong>Points</strong>10</li>
											</ul>
											<ul class="row2">
												<li><strong>Win</strong>12</li>
												<li><strong>Loss</strong>12</li>
												<li><strong>Draw</strong>1</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-4 col-sm-6">
						<div class="tournament-pointtable__box w-100">
							<div class="tournament-pointtable__group-name" id="pointtable4">
								<a class="collapsed" role="button" data-toggle="collapse" href="#pointtablecollapse4" aria-expanded="false" aria-controls="pointtablecollapse4">
									Group 4
								</a>
							</div>
							<div id="pointtablecollapse4" class="collapse multi-collapse" aria-labelledby="pointtable4">
								<div class="tournament-pointtable__content">
									<div class="tournament-pointtable__team">
										<h3>Team Name</h3>
										<div class="box">
											<ul class="row1">
												<li><strong>Total Matches</strong>12</li>
												<li><strong>Points</strong>10</li>
											</ul>
											<ul class="row2">
												<li><strong>Win</strong>12</li>
												<li><strong>Loss</strong>12</li>
												<li><strong>Draw</strong>1</li>
											</ul>
										</div>
									</div>
									<div class="tournament-pointtable__team">
										<h3>Team Name</h3>
										<div class="box">
											<ul class="row1">
												<li><strong>Total Matches</strong>12</li>
												<li><strong>Points</strong>10</li>
											</ul>
											<ul class="row2">
												<li><strong>Win</strong>12</li>
												<li><strong>Loss</strong>12</li>
												<li><strong>Draw</strong>1</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-4 col-sm-6">
						<div class="tournament-pointtable__box w-100">
							<div class="tournament-pointtable__group-name" id="pointtable5">
								<a class="collapsed" role="button" data-toggle="collapse" href="#pointtablecollapse5" aria-expanded="false" aria-controls="pointtablecollapse5">
									Group 5
								</a>
							</div>
							<div id="pointtablecollapse5" class="collapse multi-collapse" aria-labelledby="pointtable5">
								<div class="tournament-pointtable__content">
									<div class="tournament-pointtable__team">
										<h3>Team Name</h3>
										<div class="box">
											<ul class="row1">
												<li><strong>Total Matches</strong>12</li>
												<li><strong>Points</strong>10</li>
											</ul>
											<ul class="row2">
												<li><strong>Win</strong>12</li>
												<li><strong>Loss</strong>12</li>
												<li><strong>Draw</strong>1</li>
											</ul>
										</div>
									</div>
									<div class="tournament-pointtable__team">
										<h3>Team Name</h3>
										<div class="box">
											<ul class="row1">
												<li><strong>Total Matches</strong>12</li>
												<li><strong>Points</strong>10</li>
											</ul>
											<ul class="row2">
												<li><strong>Win</strong>12</li>
												<li><strong>Loss</strong>12</li>
												<li><strong>Draw</strong>1</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-4 col-sm-6">
						<div class="tournament-pointtable__box w-100">
							<div class="tournament-pointtable__group-name" id="pointtable6">
								<a class="collapsed" role="button" data-toggle="collapse" href="#pointtablecollapse6" aria-expanded="false" aria-controls="pointtablecollapse6">
									Group 6
								</a>
							</div>
							<div id="pointtablecollapse6" class="collapse multi-collapse" aria-labelledby="pointtable6">
								<div class="tournament-pointtable__content">
									<div class="tournament-pointtable__team">
										<h3>Team Name</h3>
										<div class="box">
											<ul class="row1">
												<li><strong>Total Matches</strong>12</li>
												<li><strong>Points</strong>10</li>
											</ul>
											<ul class="row2">
												<li><strong>Win</strong>12</li>
												<li><strong>Loss</strong>12</li>
												<li><strong>Draw</strong>1</li>
											</ul>
										</div>
									</div>
									<div class="tournament-pointtable__team">
										<h3>Team Name</h3>
										<div class="box">
											<ul class="row1">
												<li><strong>Total Matches</strong>12</li>
												<li><strong>Points</strong>10</li>
											</ul>
											<ul class="row2">
												<li><strong>Win</strong>12</li>
												<li><strong>Loss</strong>12</li>
												<li><strong>Draw</strong>1</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php include 'include/footer.php'; ?>