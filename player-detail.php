<?php include 'include/head.php'; ?>

<?php include 'include/header.php'; ?>

<div class="player-detail">
	<div class="player-detail-top">
		<div class="container">
			<div class="row">
				<div class="player-detail__image-col">
					<div class="player-detail__image">
						<img src="assets/images/client7.jpg" alt="">
					</div>
					<!-- Player Detail - Mobile -->
					<div class="player-detail__main-info">
						<div class="name d-flex align-items-center">John Smith <span>(21)</span> <div class="verified"><i class="fas fa-check-circle"></i></div></div>
						<div class="other d-flex align-items-center">
							<div class="address d-flex align-items-center">
								<i class="fas fa-map-marker-alt"></i> London
							</div>
							<div class="timing d-flex align-items-center">
								<i class="fas fa-clock"></i> 18:00 to 21:00
							</div>
						</div>
					</div>
					<!-- End Player Detail - Mobile -->
					<!-- Player Following - Mobile -->
					<div class="player-detail__following">
						<div class="d-flex align-items-center justify-content-center">
							<a href="#" data-toggle="modal" data-target="#followers">220 <span>followers</span></a>
							<div class="divider">|</div>
							<a href="#" data-toggle="modal" data-target="#following">250 <span>following</span></a>
						</div>
					</div>
					<!-- End Player Following - Mobile -->
				</div>
				<div class="player-detail__info-col">
					<div class="player-detail__top-info d-flex flex-wrap align-items-center justify-content-between">
						<!-- Player Detail - Desktop -->
						<div class="player-detail__main-info">
							<div class="name d-flex align-items-center">John Smith <span>(21)</span> <div class="verified"><i class="fas fa-check-circle"></i></div></div>
							<div class="other d-flex align-items-center">
								<div class="address d-flex align-items-center">
									<i class="fas fa-map-marker-alt"></i> London
								</div>
								<div class="timing d-flex align-items-center">
									<i class="fas fa-clock"></i> 18:00 to 21:00
								</div>
							</div>
						</div>
						<!-- End Player Detail - Desktop -->
						<div class="player-detail__main-action d-flex flex-wrap align-items-center">
							<div class="followbtn">
								<a href="#" class="btn-custom btn-blue-gradient">Follow</a>
							</div>
							<div class="invitebtn">
								<a href="#" class="btn-custom btn-black">Invite</a>
							</div>
							<div class="sendmsgbtn">
								<a href="#" class="btn-custom">Send Message</a>
							</div>
							<div class="nudgebtn">
								<a href="#" class="btn-custom nudge"><img src="assets/images/announcement-icon-blue.svg"></a>
							</div>
						</div>
					</div>
					<div class="player-detail__top-below">
						<!-- Player Following - Desktop -->
						<div class="player-detail__following">
							<div class="d-flex align-items-center justify-content-center">
								<a href="#" data-toggle="modal" data-target="#followers">220 <span>followers</span></a>
								<div class="divider">|</div>
								<a href="#" data-toggle="modal" data-target="#following">250 <span>following</span></a>
							</div>
						</div>
						<!-- End Player Following - Desktop -->
						<div class="player-detail__bio">
							<div class="player-detail__bio-box">
								<div class="top">Bio:</div>
								<div class="text">
									<p>It’s hard to beat a person who never gives up</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Followers Popup -->
	<div class="modal fade followpopup" id="followers" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-body">
					<div class="modal-title">
						<h3>Followers</h3>
					</div>
					<div class="users contentscroll">
						<div class="row">
							<div class="col-md-6">
								<div class="users-box">
									<div class="users-top d-flex align-items-center justify-content-between">
										<div class="left">
											<a href="#" class="d-inline-flex align-items-center">
												<img src="assets/images/client7.jpg" alt="">
												John Smith <span>(21)</span>
											</a>
										</div>
										<div class="right d-flex align-items-center">
											<a href="#"><img src="assets/images/search-announce-icon-blue.svg" alt=""></a>
											<a href="#"><img src="assets/images/add-user-icon.svg" alt=""></a>
											<a href="#"><img src="assets/images/message-icon.svg" alt=""></a>
										</div>
									</div>
									<div class="users-content">
										It’s hard to beat a person who never gives up
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="users-box">
									<div class="users-top d-flex align-items-center justify-content-between">
										<div class="left">
											<a href="#" class="d-inline-flex align-items-center">
												<img src="assets/images/client7.jpg" alt="">
												John Smith <span>(21)</span>
											</a>
										</div>
										<div class="right d-flex align-items-center">
											<a href="#"><img src="assets/images/search-announce-icon-blue.svg" alt=""></a>
											<a href="#"><img src="assets/images/add-user-icon.svg" alt=""></a>
											<a href="#"><img src="assets/images/message-icon.svg" alt=""></a>
										</div>
									</div>
									<div class="users-content">
										It’s hard to beat a person who never gives up
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="users-box">
									<div class="users-top d-flex align-items-center justify-content-between">
										<div class="left">
											<a href="#" class="d-inline-flex align-items-center">
												<img src="assets/images/client7.jpg" alt="">
												John Smith <span>(21)</span>
											</a>
										</div>
										<div class="right d-flex align-items-center">
											<a href="#"><img src="assets/images/search-announce-icon-blue.svg" alt=""></a>
											<a href="#"><img src="assets/images/add-user-icon.svg" alt=""></a>
											<a href="#"><img src="assets/images/message-icon.svg" alt=""></a>
										</div>
									</div>
									<div class="users-content">
										It’s hard to beat a person who never gives up
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="users-box">
									<div class="users-top d-flex align-items-center justify-content-between">
										<div class="left">
											<a href="#" class="d-inline-flex align-items-center">
												<img src="assets/images/client7.jpg" alt="">
												John Smith <span>(21)</span>
											</a>
										</div>
										<div class="right d-flex align-items-center">
											<a href="#"><img src="assets/images/search-announce-icon-blue.svg" alt=""></a>
											<a href="#"><img src="assets/images/add-user-icon.svg" alt=""></a>
											<a href="#"><img src="assets/images/message-icon.svg" alt=""></a>
										</div>
									</div>
									<div class="users-content">
										It’s hard to beat a person who never gives up
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="users-box">
									<div class="users-top d-flex align-items-center justify-content-between">
										<div class="left">
											<a href="#" class="d-inline-flex align-items-center">
												<img src="assets/images/client7.jpg" alt="">
												John Smith <span>(21)</span>
											</a>
										</div>
										<div class="right d-flex align-items-center">
											<a href="#"><img src="assets/images/search-announce-icon-blue.svg" alt=""></a>
											<a href="#"><img src="assets/images/add-user-icon.svg" alt=""></a>
											<a href="#"><img src="assets/images/message-icon.svg" alt=""></a>
										</div>
									</div>
									<div class="users-content">
										It’s hard to beat a person who never gives up
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="users-box">
									<div class="users-top d-flex align-items-center justify-content-between">
										<div class="left">
											<a href="#" class="d-inline-flex align-items-center">
												<img src="assets/images/client7.jpg" alt="">
												John Smith <span>(21)</span>
											</a>
										</div>
										<div class="right d-flex align-items-center">
											<a href="#"><img src="assets/images/search-announce-icon-blue.svg" alt=""></a>
											<a href="#"><img src="assets/images/add-user-icon.svg" alt=""></a>
											<a href="#"><img src="assets/images/message-icon.svg" alt=""></a>
										</div>
									</div>
									<div class="users-content">
										It’s hard to beat a person who never gives up
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="users-box">
									<div class="users-top d-flex align-items-center justify-content-between">
										<div class="left">
											<a href="#" class="d-inline-flex align-items-center">
												<img src="assets/images/client7.jpg" alt="">
												John Smith <span>(21)</span>
											</a>
										</div>
										<div class="right d-flex align-items-center">
											<a href="#"><img src="assets/images/search-announce-icon-blue.svg" alt=""></a>
											<a href="#"><img src="assets/images/add-user-icon.svg" alt=""></a>
											<a href="#"><img src="assets/images/message-icon.svg" alt=""></a>
										</div>
									</div>
									<div class="users-content">
										It’s hard to beat a person who never gives up
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="users-box">
									<div class="users-top d-flex align-items-center justify-content-between">
										<div class="left">
											<a href="#" class="d-inline-flex align-items-center">
												<img src="assets/images/client7.jpg" alt="">
												John Smith <span>(21)</span>
											</a>
										</div>
										<div class="right d-flex align-items-center">
											<a href="#"><img src="assets/images/search-announce-icon-blue.svg" alt=""></a>
											<a href="#"><img src="assets/images/add-user-icon.svg" alt=""></a>
											<a href="#"><img src="assets/images/message-icon.svg" alt=""></a>
										</div>
									</div>
									<div class="users-content">
										It’s hard to beat a person who never gives up
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="users-box">
									<div class="users-top d-flex align-items-center justify-content-between">
										<div class="left">
											<a href="#" class="d-inline-flex align-items-center">
												<img src="assets/images/client7.jpg" alt="">
												John Smith <span>(21)</span>
											</a>
										</div>
										<div class="right d-flex align-items-center">
											<a href="#"><img src="assets/images/search-announce-icon-blue.svg" alt=""></a>
											<a href="#"><img src="assets/images/add-user-icon.svg" alt=""></a>
											<a href="#"><img src="assets/images/message-icon.svg" alt=""></a>
										</div>
									</div>
									<div class="users-content">
										It’s hard to beat a person who never gives up
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="users-box">
									<div class="users-top d-flex align-items-center justify-content-between">
										<div class="left">
											<a href="#" class="d-inline-flex align-items-center">
												<img src="assets/images/client7.jpg" alt="">
												John Smith <span>(21)</span>
											</a>
										</div>
										<div class="right d-flex align-items-center">
											<a href="#"><img src="assets/images/search-announce-icon-blue.svg" alt=""></a>
											<a href="#"><img src="assets/images/add-user-icon.svg" alt=""></a>
											<a href="#"><img src="assets/images/message-icon.svg" alt=""></a>
										</div>
									</div>
									<div class="users-content">
										It’s hard to beat a person who never gives up
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="users-box">
									<div class="users-top d-flex align-items-center justify-content-between">
										<div class="left">
											<a href="#" class="d-inline-flex align-items-center">
												<img src="assets/images/client7.jpg" alt="">
												John Smith <span>(21)</span>
											</a>
										</div>
										<div class="right d-flex align-items-center">
											<a href="#"><img src="assets/images/search-announce-icon-blue.svg" alt=""></a>
											<a href="#"><img src="assets/images/add-user-icon.svg" alt=""></a>
											<a href="#"><img src="assets/images/message-icon.svg" alt=""></a>
										</div>
									</div>
									<div class="users-content">
										It’s hard to beat a person who never gives up
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="users-box">
									<div class="users-top d-flex align-items-center justify-content-between">
										<div class="left">
											<a href="#" class="d-inline-flex align-items-center">
												<img src="assets/images/client7.jpg" alt="">
												John Smith <span>(21)</span>
											</a>
										</div>
										<div class="right d-flex align-items-center">
											<a href="#"><img src="assets/images/search-announce-icon-blue.svg" alt=""></a>
											<a href="#"><img src="assets/images/add-user-icon.svg" alt=""></a>
											<a href="#"><img src="assets/images/message-icon.svg" alt=""></a>
										</div>
									</div>
									<div class="users-content">
										It’s hard to beat a person who never gives up
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="users-box">
									<div class="users-top d-flex align-items-center justify-content-between">
										<div class="left">
											<a href="#" class="d-inline-flex align-items-center">
												<img src="assets/images/client7.jpg" alt="">
												John Smith <span>(21)</span>
											</a>
										</div>
										<div class="right d-flex align-items-center">
											<a href="#"><img src="assets/images/search-announce-icon-blue.svg" alt=""></a>
											<a href="#"><img src="assets/images/add-user-icon.svg" alt=""></a>
											<a href="#"><img src="assets/images/message-icon.svg" alt=""></a>
										</div>
									</div>
									<div class="users-content">
										It’s hard to beat a person who never gives up
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="users-box">
									<div class="users-top d-flex align-items-center justify-content-between">
										<div class="left">
											<a href="#" class="d-inline-flex align-items-center">
												<img src="assets/images/client7.jpg" alt="">
												John Smith <span>(21)</span>
											</a>
										</div>
										<div class="right d-flex align-items-center">
											<a href="#"><img src="assets/images/search-announce-icon-blue.svg" alt=""></a>
											<a href="#"><img src="assets/images/add-user-icon.svg" alt=""></a>
											<a href="#"><img src="assets/images/message-icon.svg" alt=""></a>
										</div>
									</div>
									<div class="users-content">
										It’s hard to beat a person who never gives up
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="btn-close" data-dismiss="modal"><img src="assets/images/close-black-icon.svg"></div>
			</div>
		</div>
	</div>

	<!-- Followers Popup -->
	<div class="modal fade followpopup" id="following" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-body">
					<div class="modal-title">
						<h3>Following</h3>
					</div>
					<div class="users contentscroll">
						<div class="row">
							<div class="col-md-6">
								<div class="users-box">
									<div class="users-top d-flex align-items-center justify-content-between">
										<div class="left">
											<a href="#" class="d-inline-flex align-items-center">
												<img src="assets/images/client7.jpg" alt="">
												John Smith <span>(21)</span>
											</a>
										</div>
										<div class="right d-flex align-items-center">
											<a href="#"><img src="assets/images/search-announce-icon-blue.svg" alt=""></a>
											<a href="#"><img src="assets/images/add-user-icon.svg" alt=""></a>
											<a href="#"><img src="assets/images/message-icon.svg" alt=""></a>
										</div>
									</div>
									<div class="users-content">
										It’s hard to beat a person who never gives up
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="users-box">
									<div class="users-top d-flex align-items-center justify-content-between">
										<div class="left">
											<a href="#" class="d-inline-flex align-items-center">
												<img src="assets/images/client7.jpg" alt="">
												John Smith <span>(21)</span>
											</a>
										</div>
										<div class="right d-flex align-items-center">
											<a href="#"><img src="assets/images/search-announce-icon-blue.svg" alt=""></a>
											<a href="#"><img src="assets/images/add-user-icon.svg" alt=""></a>
											<a href="#"><img src="assets/images/message-icon.svg" alt=""></a>
										</div>
									</div>
									<div class="users-content">
										It’s hard to beat a person who never gives up
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="users-box">
									<div class="users-top d-flex align-items-center justify-content-between">
										<div class="left">
											<a href="#" class="d-inline-flex align-items-center">
												<img src="assets/images/client7.jpg" alt="">
												John Smith <span>(21)</span>
											</a>
										</div>
										<div class="right d-flex align-items-center">
											<a href="#"><img src="assets/images/search-announce-icon-blue.svg" alt=""></a>
											<a href="#"><img src="assets/images/add-user-icon.svg" alt=""></a>
											<a href="#"><img src="assets/images/message-icon.svg" alt=""></a>
										</div>
									</div>
									<div class="users-content">
										It’s hard to beat a person who never gives up
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="users-box">
									<div class="users-top d-flex align-items-center justify-content-between">
										<div class="left">
											<a href="#" class="d-inline-flex align-items-center">
												<img src="assets/images/client7.jpg" alt="">
												John Smith <span>(21)</span>
											</a>
										</div>
										<div class="right d-flex align-items-center">
											<a href="#"><img src="assets/images/search-announce-icon-blue.svg" alt=""></a>
											<a href="#"><img src="assets/images/add-user-icon.svg" alt=""></a>
											<a href="#"><img src="assets/images/message-icon.svg" alt=""></a>
										</div>
									</div>
									<div class="users-content">
										It’s hard to beat a person who never gives up
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="users-box">
									<div class="users-top d-flex align-items-center justify-content-between">
										<div class="left">
											<a href="#" class="d-inline-flex align-items-center">
												<img src="assets/images/client7.jpg" alt="">
												John Smith <span>(21)</span>
											</a>
										</div>
										<div class="right d-flex align-items-center">
											<a href="#"><img src="assets/images/search-announce-icon-blue.svg" alt=""></a>
											<a href="#"><img src="assets/images/add-user-icon.svg" alt=""></a>
											<a href="#"><img src="assets/images/message-icon.svg" alt=""></a>
										</div>
									</div>
									<div class="users-content">
										It’s hard to beat a person who never gives up
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="users-box">
									<div class="users-top d-flex align-items-center justify-content-between">
										<div class="left">
											<a href="#" class="d-inline-flex align-items-center">
												<img src="assets/images/client7.jpg" alt="">
												John Smith <span>(21)</span>
											</a>
										</div>
										<div class="right d-flex align-items-center">
											<a href="#"><img src="assets/images/search-announce-icon-blue.svg" alt=""></a>
											<a href="#"><img src="assets/images/add-user-icon.svg" alt=""></a>
											<a href="#"><img src="assets/images/message-icon.svg" alt=""></a>
										</div>
									</div>
									<div class="users-content">
										It’s hard to beat a person who never gives up
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="users-box">
									<div class="users-top d-flex align-items-center justify-content-between">
										<div class="left">
											<a href="#" class="d-inline-flex align-items-center">
												<img src="assets/images/client7.jpg" alt="">
												John Smith <span>(21)</span>
											</a>
										</div>
										<div class="right d-flex align-items-center">
											<a href="#"><img src="assets/images/search-announce-icon-blue.svg" alt=""></a>
											<a href="#"><img src="assets/images/add-user-icon.svg" alt=""></a>
											<a href="#"><img src="assets/images/message-icon.svg" alt=""></a>
										</div>
									</div>
									<div class="users-content">
										It’s hard to beat a person who never gives up
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="users-box">
									<div class="users-top d-flex align-items-center justify-content-between">
										<div class="left">
											<a href="#" class="d-inline-flex align-items-center">
												<img src="assets/images/client7.jpg" alt="">
												John Smith <span>(21)</span>
											</a>
										</div>
										<div class="right d-flex align-items-center">
											<a href="#"><img src="assets/images/search-announce-icon-blue.svg" alt=""></a>
											<a href="#"><img src="assets/images/add-user-icon.svg" alt=""></a>
											<a href="#"><img src="assets/images/message-icon.svg" alt=""></a>
										</div>
									</div>
									<div class="users-content">
										It’s hard to beat a person who never gives up
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="users-box">
									<div class="users-top d-flex align-items-center justify-content-between">
										<div class="left">
											<a href="#" class="d-inline-flex align-items-center">
												<img src="assets/images/client7.jpg" alt="">
												John Smith <span>(21)</span>
											</a>
										</div>
										<div class="right d-flex align-items-center">
											<a href="#"><img src="assets/images/search-announce-icon-blue.svg" alt=""></a>
											<a href="#"><img src="assets/images/add-user-icon.svg" alt=""></a>
											<a href="#"><img src="assets/images/message-icon.svg" alt=""></a>
										</div>
									</div>
									<div class="users-content">
										It’s hard to beat a person who never gives up
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="users-box">
									<div class="users-top d-flex align-items-center justify-content-between">
										<div class="left">
											<a href="#" class="d-inline-flex align-items-center">
												<img src="assets/images/client7.jpg" alt="">
												John Smith <span>(21)</span>
											</a>
										</div>
										<div class="right d-flex align-items-center">
											<a href="#"><img src="assets/images/search-announce-icon-blue.svg" alt=""></a>
											<a href="#"><img src="assets/images/add-user-icon.svg" alt=""></a>
											<a href="#"><img src="assets/images/message-icon.svg" alt=""></a>
										</div>
									</div>
									<div class="users-content">
										It’s hard to beat a person who never gives up
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="users-box">
									<div class="users-top d-flex align-items-center justify-content-between">
										<div class="left">
											<a href="#" class="d-inline-flex align-items-center">
												<img src="assets/images/client7.jpg" alt="">
												John Smith <span>(21)</span>
											</a>
										</div>
										<div class="right d-flex align-items-center">
											<a href="#"><img src="assets/images/search-announce-icon-blue.svg" alt=""></a>
											<a href="#"><img src="assets/images/add-user-icon.svg" alt=""></a>
											<a href="#"><img src="assets/images/message-icon.svg" alt=""></a>
										</div>
									</div>
									<div class="users-content">
										It’s hard to beat a person who never gives up
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="users-box">
									<div class="users-top d-flex align-items-center justify-content-between">
										<div class="left">
											<a href="#" class="d-inline-flex align-items-center">
												<img src="assets/images/client7.jpg" alt="">
												John Smith <span>(21)</span>
											</a>
										</div>
										<div class="right d-flex align-items-center">
											<a href="#"><img src="assets/images/search-announce-icon-blue.svg" alt=""></a>
											<a href="#"><img src="assets/images/add-user-icon.svg" alt=""></a>
											<a href="#"><img src="assets/images/message-icon.svg" alt=""></a>
										</div>
									</div>
									<div class="users-content">
										It’s hard to beat a person who never gives up
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="users-box">
									<div class="users-top d-flex align-items-center justify-content-between">
										<div class="left">
											<a href="#" class="d-inline-flex align-items-center">
												<img src="assets/images/client7.jpg" alt="">
												John Smith <span>(21)</span>
											</a>
										</div>
										<div class="right d-flex align-items-center">
											<a href="#"><img src="assets/images/search-announce-icon-blue.svg" alt=""></a>
											<a href="#"><img src="assets/images/add-user-icon.svg" alt=""></a>
											<a href="#"><img src="assets/images/message-icon.svg" alt=""></a>
										</div>
									</div>
									<div class="users-content">
										It’s hard to beat a person who never gives up
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="users-box">
									<div class="users-top d-flex align-items-center justify-content-between">
										<div class="left">
											<a href="#" class="d-inline-flex align-items-center">
												<img src="assets/images/client7.jpg" alt="">
												John Smith <span>(21)</span>
											</a>
										</div>
										<div class="right d-flex align-items-center">
											<a href="#"><img src="assets/images/search-announce-icon-blue.svg" alt=""></a>
											<a href="#"><img src="assets/images/add-user-icon.svg" alt=""></a>
											<a href="#"><img src="assets/images/message-icon.svg" alt=""></a>
										</div>
									</div>
									<div class="users-content">
										It’s hard to beat a person who never gives up
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="btn-close" data-dismiss="modal"><img src="assets/images/close-black-icon.svg"></div>
			</div>
		</div>
	</div>

	<div class="player-detail-post">
		<div class="container">
			<div class="row">
				<div class="player-detail-post__col1">
					<div class="player-detail__skills">
						<div class="top">Activity Skills</div>
						<ul>
							<li>
								<div class="player-detail__skill-box">
									<div class="game-icon">
										<img src="assets/images/football-ball-icon.svg">
									</div>
									<div class="game-name">Football</div>
									<div class="skill-level">Expert</div>
								</div>
							</li>
							<li>
								<div class="player-detail__skill-box">
									<div class="game-icon">
										<img src="assets/images/football-ball-icon.svg">
									</div>
									<div class="game-name">Football</div>
									<div class="skill-level">Expert</div>
								</div>
							</li>
							<li>
								<div class="player-detail__skill-box">
									<div class="game-icon">
										<img src="assets/images/football-ball-icon.svg">
									</div>
									<div class="game-name">Football</div>
									<div class="skill-level">Expert</div>
								</div>
							</li>
							<li>
								<div class="player-detail__skill-box">
									<div class="game-icon">
										<img src="assets/images/football-ball-icon.svg">
									</div>
									<div class="game-name">Football</div>
									<div class="skill-level">Expert</div>
								</div>
							</li>
							<li>
								<div class="player-detail__skill-box">
									<div class="game-icon">
										<img src="assets/images/football-ball-icon.svg">
									</div>
									<div class="game-name">Football</div>
									<div class="skill-level">Expert</div>
								</div>
							</li>
							<li>
								<div class="player-detail__skill-box">
									<div class="game-icon">
										<img src="assets/images/football-ball-icon.svg">
									</div>
									<div class="game-name">Football</div>
									<div class="skill-level">Expert</div>
								</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="player-detail-post__col2">
					<div class="feed-filter">
						<select class="form-control multiselect" multiple="multiple">
							<option value="following" selected>Following</option>
							<option value="trending">Trending</option>
							<option value="nearby">Nearby</option>
						</select>
					</div>

					<div class="feed-post-listing">
					
						<div class="feed-post">
							<div class="feed-post__top d-flex align-items-center justify-content-between">
								<div class="feed-post__top-left d-flex align-items-center">
									<a href="#"><img src="assets/images/client7.jpg" class="feed-post__admin-image"></a>
									<div class="feed-post__admin-detail">
										<a href="#"><div class="feed-post__admin-name">John Smith <img src="assets/images/announcement-icon-blue.svg"></div></a>
										<div class="feed-post__date">12:00, 12th Nov 2020</div>
									</div>
								</div>
								<div class="feed-post__top-right">
									<div class="feed-post__option">
										<i class="far fa-bookmark"></i>
									</div>
									<div class="feed-post__report">
										<i class="fas fa-ellipsis-h"></i>
										<div class="feed-post__report-dropdown" data-toggle="modal" data-target="#reportpost">Report Post</div>
									</div>
								</div>
							</div>
							<div class="feed-post__bottom">
								<div class="feed-post__content">Game Night Today!!</div>
								<div class="feed-post__action d-flex align-items-center justify-content-between">
									<div class="feed-post__action-left d-flex align-items-center">
										<div class="like">
											<i class="far fa-thumbs-up"></i> 120
										</div>
										<div class="heart">
											<i class="far fa-heart"></i> 8
										</div>
										<div class="comment" data-toggle="modal" data-target="#postcomment">
											<i class="far fa-comment"></i> 28
										</div>
									</div>
									<div class="feed-post__action-right">
										<div class="share">
											<i class="fas fa-share"></i>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="feed-post">
							<div class="feed-post__top d-flex align-items-center justify-content-between">
								<div class="feed-post__top-left d-flex align-items-center">
									<a href="#"><img src="assets/images/client7.jpg" class="feed-post__admin-image"></a>
									<div class="feed-post__admin-detail">
										<a href="#"><div class="feed-post__admin-name">John Smith <img src="assets/images/announcement-icon-blue.svg"></div></a>
										<div class="feed-post__date">12:00, 12th Nov 2020</div>
									</div>
								</div>
								<div class="feed-post__top-right">
									<div class="feed-post__option">
										<i class="far fa-bookmark"></i>
									</div>
									<div class="feed-post__report">
										<i class="fas fa-ellipsis-h"></i>
										<div class="feed-post__report-dropdown" data-toggle="modal" data-target="#reportpost">Report Post</div>
									</div>
								</div>
							</div>
							<div class="feed-post__bottom">
								<div class="feed-post__content">
									Loved the Match Today!!! Congratulations to the Homeland Team
									<div class="feed-post__tags"><strong>with</strong> with John Smith and 2 Others</div>
									<div class="feed-post__image-video">
										<div class="swiper-container">
											<div class="swiper-wrapper">
												<div class="swiper-slide">
													<img src="assets/images/client7.jpg">
												</div>
												<div class="swiper-slide">
													<img src="assets/images/slider-image.jpg">
												</div>
												<div class="swiper-slide">
													<img src="assets/images/slider-image.jpg">
												</div>
												<div class="swiper-slide">
													<img src="assets/images/slider-image.jpg">
												</div>
												<div class="swiper-slide">
													<img src="assets/images/slider-image.jpg">
												</div>
											</div>
											<div class="swiper-pagination"></div>
											<div class="swiper-overlay"></div>
										</div>
									</div>
								</div>
								<div class="feed-post__action d-flex align-items-center justify-content-between">
									<div class="feed-post__action-left d-flex align-items-center">
										<div class="like">
											<i class="far fa-thumbs-up"></i> 120
										</div>
										<div class="heart">
											<i class="far fa-heart"></i> 8
										</div>
										<div class="comment" data-toggle="modal" data-target="#postcomment">
											<i class="far fa-comment"></i> 28
										</div>
									</div>
									<div class="feed-post__action-right">
										<div class="share">
											<i class="fas fa-share"></i>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="feed-post">
							<div class="feed-post__top d-flex align-items-center justify-content-between">
								<div class="feed-post__top-left d-flex align-items-center">
									<a href="#"><img src="assets/images/client7.jpg" class="feed-post__admin-image"></a>
									<div class="feed-post__admin-detail">
										<a href="#"><div class="feed-post__admin-name">John Smith <img src="assets/images/announcement-icon-blue.svg"></div></a>
										<div class="feed-post__date">12:00, 12th Nov 2020</div>
									</div>
								</div>
								<div class="feed-post__top-right">
									<div class="feed-post__option">
										<i class="far fa-bookmark"></i>
									</div>
									<div class="feed-post__report">
										<i class="fas fa-ellipsis-h"></i>
										<div class="feed-post__report-dropdown" data-toggle="modal" data-target="#reportpost">Report Post</div>
									</div>
								</div>
							</div>
							<div class="feed-post__bottom">
								<div class="feed-post__content">
									Loved the Match Today!!! Congratulations to the Homeland Team
									<div class="feed-post__tags"><strong>with</strong> with John Smith and 2 Others</div>
									<div class="feed-post__image-video">
										<div class="swiper-container">
											<div class="swiper-wrapper">
												<div class="swiper-slide">
													<div class="video-wrap">
														<video controls>
															<source src="https://www.w3schools.com/tags/movie.mp4" type="video/mp4">
															Your browser does not support the video tag.
														</video>
													</div>
												</div>
												<div class="swiper-slide">
													<div class="video-wrap">
														<video controls>
															<source src="http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4" type="video/mp4">
															Your browser does not support the video tag.
														</video>
													</div>
												</div>
												<div class="swiper-slide">
													<div class="video-wrap">
														<video controls>
															<source src="http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ElephantsDream.mp4" type="video/mp4">
															Your browser does not support the video tag.
														</video>
													</div>
												</div>
												<div class="swiper-slide">
													<div class="video-wrap">
														<video controls>
															<source src="http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerEscapes.mp4" type="video/mp4">
															Your browser does not support the video tag.
														</video>
													</div>
												</div>
											</div>
											<div class="swiper-pagination"></div>
											<div class="swiper-overlay"></div>
										</div>
									</div>
								</div>
								<div class="feed-post__action d-flex align-items-center justify-content-between">
									<div class="feed-post__action-left d-flex align-items-center">
										<div class="like">
											<i class="far fa-thumbs-up"></i> 120
										</div>
										<div class="heart">
											<i class="far fa-heart"></i> 8
										</div>
										<div class="comment" data-toggle="modal" data-target="#postcomment">
											<i class="far fa-comment"></i> 28
										</div>
									</div>
									<div class="feed-post__action-right">
										<div class="share">
											<i class="fas fa-share"></i>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="feed-post">
							<div class="feed-post__top d-flex align-items-center justify-content-between">
								<div class="feed-post__top-left d-flex align-items-center">
									<a href="#"><img src="assets/images/client7.jpg" class="feed-post__admin-image"></a>
									<div class="feed-post__admin-detail">
										<a href="#"><div class="feed-post__admin-name">John Smith <img src="assets/images/announcement-icon-blue.svg"></div></a>
										<div class="feed-post__date">12:00, 12th Nov 2020</div>
									</div>
								</div>
								<div class="feed-post__top-right">
									<div class="feed-post__option">
										<i class="far fa-bookmark"></i>
									</div>
									<div class="feed-post__report">
										<i class="fas fa-ellipsis-h"></i>
										<div class="feed-post__report-dropdown" data-toggle="modal" data-target="#reportpost">Report Post</div>
									</div>
								</div>
							</div>
							<div class="feed-post__bottom">
								<div class="feed-post__content">Game Night Today!!</div>
								<div class="feed-post__action d-flex align-items-center justify-content-between">
									<div class="feed-post__action-left d-flex align-items-center">
										<div class="like">
											<i class="far fa-thumbs-up"></i> 120
										</div>
										<div class="heart">
											<i class="far fa-heart"></i> 8
										</div>
										<div class="comment" data-toggle="modal" data-target="#postcomment">
											<i class="far fa-comment"></i> 28
										</div>
									</div>
									<div class="feed-post__action-right">
										<div class="share">
											<i class="fas fa-share"></i>
										</div>
									</div>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php include 'include/footer.php'; ?>