<?php include 'include/head.php'; ?>

<?php include 'include/header.php'; ?>

<div class="account">
	<div class="container">
		<div class="row">
			<div class="col-xl-3 col-md-4">
				<div class="account-sidebar-wrap">
					<div class="account-menu">
						Account Settings <i class="fas fa-bars"></i>
					</div>
					<div class="account-sidebar bg-white">
						<div class="close">×</div>
						<ul class="account-sidebar__links">
							<li>
								<a href="account-verify-mobile.php"><i class="fas fa-mobile-alt"></i>Verify Mobile Number</a>
							</li>
							<li>
								<a href="account-favourite-post.php" class="active"><i class="fas fa-heart"></i>Favorite Post</a>
							</li>
							<li>
								<a href="account-saved-post.php"><i class="fas fa-bookmark"></i>Saved Post</a>
							</li>
							<li>
								<a href="account-transaction-history.php"><i class="fas fa-exchange-alt"></i>Transaction History</a>
							</li>
							<li>
								<a href="account-change-password.php"><i class="fas fa-lock"></i>Change Password</a>
							</li>
							<li>
								<a href="account-terms-conditions.php"><i class="fas fa-clipboard-list"></i>Terms & Conditions</a>
							</li>
							<li>
								<a href="account-privacy-policy.php"><i class="fas fa-mobile-alt"></i>Privacy Policy</a>
							</li>
							<li>
								<a href="account-faq.php"><i class="fas fa-comments"></i>FAQs</a>
							</li>
							<li>
								<a href="account-feedback.php"><i class="fas fa-thumbs-up"></i>Feedback</a>
							</li>
							<li>
								<a href="account-delete.php"><i class="fas fa-trash"></i>Delete My Account</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-xl-9 col-md-8">
				<div class="white-title">
					<h3>Favorite Post</h3>
				</div>

				<div class="w-700">
					<div class="feed-post-listing">		
						<div class="feed-post">
							<div class="feed-post__top d-flex align-items-center justify-content-between">
								<div class="feed-post__top-left d-flex align-items-center">
									<a href="#"><img src="assets/images/client7.jpg" class="feed-post__admin-image"></a>
									<div class="feed-post__admin-detail">
										<a href="#"><div class="feed-post__admin-name">John Smith <img src="assets/images/announcement-icon-blue.svg"></div></a>
										<div class="feed-post__date">12:00, 12th Nov 2020</div>
									</div>
								</div>
								<div class="feed-post__top-right">
									<div class="feed-post__option">
										<i class="far fa-bookmark"></i>
									</div>
									<div class="feed-post__report">
										<i class="fas fa-ellipsis-h"></i>
										<div class="feed-post__report-dropdown" data-toggle="modal" data-target="#reportpost">Report Post</div>
									</div>
								</div>
							</div>
							<div class="feed-post__bottom">
								<div class="feed-post__content">Game Night Today!!</div>
								<div class="feed-post__action d-flex align-items-center justify-content-between">
									<div class="feed-post__action-left d-flex align-items-center">
										<div class="like">
											<i class="far fa-thumbs-up"></i> 120
										</div>
										<div class="heart">
											<i class="fas fa-heart"></i> 8
										</div>
										<div class="comment" data-toggle="modal" data-target="#postcomment">
											<i class="far fa-comment"></i> 28
										</div>
									</div>
									<div class="feed-post__action-right">
										<div class="share">
											<i class="fas fa-share"></i>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="feed-post">
							<div class="feed-post__top d-flex align-items-center justify-content-between">
								<div class="feed-post__top-left d-flex align-items-center">
									<a href="#"><img src="assets/images/client7.jpg" class="feed-post__admin-image"></a>
									<div class="feed-post__admin-detail">
										<a href="#"><div class="feed-post__admin-name">John Smith <img src="assets/images/announcement-icon-blue.svg"></div></a>
										<div class="feed-post__date">12:00, 12th Nov 2020</div>
									</div>
								</div>
								<div class="feed-post__top-right">
									<div class="feed-post__option">
										<i class="far fa-bookmark"></i>
									</div>
									<div class="feed-post__report">
										<i class="fas fa-ellipsis-h"></i>
										<div class="feed-post__report-dropdown" data-toggle="modal" data-target="#reportpost">Report Post</div>
									</div>
								</div>
							</div>
							<div class="feed-post__bottom">
								<div class="feed-post__content">
									Loved the Match Today!!! Congratulations to the Homeland Team
									<div class="feed-post__tags"><strong>with</strong> with John Smith and 2 Others</div>
									<div class="feed-post__image-video">
										<div class="swiper-container">
											<div class="swiper-wrapper">
												<div class="swiper-slide">
													<img src="assets/images/client7.jpg">
												</div>
												<div class="swiper-slide">
													<img src="assets/images/slider-image.jpg">
												</div>
												<div class="swiper-slide">
													<img src="assets/images/slider-image.jpg">
												</div>
												<div class="swiper-slide">
													<img src="assets/images/slider-image.jpg">
												</div>
												<div class="swiper-slide">
													<img src="assets/images/slider-image.jpg">
												</div>
											</div>
											<div class="swiper-pagination"></div>
											<div class="swiper-overlay"></div>
										</div>
									</div>
								</div>
								<div class="feed-post__action d-flex align-items-center justify-content-between">
									<div class="feed-post__action-left d-flex align-items-center">
										<div class="like">
											<i class="far fa-thumbs-up"></i> 120
										</div>
										<div class="heart">
											<i class="fas fa-heart"></i> 8
										</div>
										<div class="comment" data-toggle="modal" data-target="#postcomment">
											<i class="far fa-comment"></i> 28
										</div>
									</div>
									<div class="feed-post__action-right">
										<div class="share">
											<i class="fas fa-share"></i>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="feed-post">
							<div class="feed-post__top d-flex align-items-center justify-content-between">
								<div class="feed-post__top-left d-flex align-items-center">
									<a href="#"><img src="assets/images/client7.jpg" class="feed-post__admin-image"></a>
									<div class="feed-post__admin-detail">
										<a href="#"><div class="feed-post__admin-name">John Smith <img src="assets/images/announcement-icon-blue.svg"></div></a>
										<div class="feed-post__date">12:00, 12th Nov 2020</div>
									</div>
								</div>
								<div class="feed-post__top-right">
									<div class="feed-post__option">
										<i class="far fa-bookmark"></i>
									</div>
									<div class="feed-post__report">
										<i class="fas fa-ellipsis-h"></i>
										<div class="feed-post__report-dropdown" data-toggle="modal" data-target="#reportpost">Report Post</div>
									</div>
								</div>
							</div>
							<div class="feed-post__bottom">
								<div class="feed-post__content">
									Loved the Match Today!!! Congratulations to the Homeland Team
									<div class="feed-post__tags"><strong>with</strong> with John Smith and 2 Others</div>
									<div class="feed-post__image-video">
										<div class="swiper-container">
											<div class="swiper-wrapper">
												<div class="swiper-slide">
													<div class="video-wrap">
														<video controls>
															<source src="https://www.w3schools.com/tags/movie.mp4" type="video/mp4">
															Your browser does not support the video tag.
														</video>
													</div>
												</div>
												<div class="swiper-slide">
													<div class="video-wrap">
														<video controls>
															<source src="http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4" type="video/mp4">
															Your browser does not support the video tag.
														</video>
													</div>
												</div>
												<div class="swiper-slide">
													<div class="video-wrap">
														<video controls>
															<source src="http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ElephantsDream.mp4" type="video/mp4">
															Your browser does not support the video tag.
														</video>
													</div>
												</div>
												<div class="swiper-slide">
													<div class="video-wrap">
														<video controls>
															<source src="http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerEscapes.mp4" type="video/mp4">
															Your browser does not support the video tag.
														</video>
													</div>
												</div>
											</div>
											<div class="swiper-pagination"></div>
											<div class="swiper-overlay"></div>
										</div>
									</div>
								</div>
								<div class="feed-post__action d-flex align-items-center justify-content-between">
									<div class="feed-post__action-left d-flex align-items-center">
										<div class="like">
											<i class="far fa-thumbs-up"></i> 120
										</div>
										<div class="heart">
											<i class="fas fa-heart"></i> 8
										</div>
										<div class="comment" data-toggle="modal" data-target="#postcomment">
											<i class="far fa-comment"></i> 28
										</div>
									</div>
									<div class="feed-post__action-right">
										<div class="share">
											<i class="fas fa-share"></i>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="feed-post">
							<div class="feed-post__top d-flex align-items-center justify-content-between">
								<div class="feed-post__top-left d-flex align-items-center">
									<a href="#"><img src="assets/images/client7.jpg" class="feed-post__admin-image"></a>
									<div class="feed-post__admin-detail">
										<a href="#"><div class="feed-post__admin-name">John Smith <img src="assets/images/announcement-icon-blue.svg"></div></a>
										<div class="feed-post__date">12:00, 12th Nov 2020</div>
									</div>
								</div>
								<div class="feed-post__top-right">
									<div class="feed-post__option">
										<i class="far fa-bookmark"></i>
									</div>
									<div class="feed-post__report">
										<i class="fas fa-ellipsis-h"></i>
										<div class="feed-post__report-dropdown" data-toggle="modal" data-target="#reportpost">Report Post</div>
									</div>
								</div>
							</div>
							<div class="feed-post__bottom">
								<div class="feed-post__content">Game Night Today!!</div>
								<div class="feed-post__action d-flex align-items-center justify-content-between">
									<div class="feed-post__action-left d-flex align-items-center">
										<div class="like">
											<i class="far fa-thumbs-up"></i> 120
										</div>
										<div class="heart">
											<i class="fas fa-heart"></i> 8
										</div>
										<div class="comment" data-toggle="modal" data-target="#postcomment">
											<i class="far fa-comment"></i> 28
										</div>
									</div>
									<div class="feed-post__action-right">
										<div class="share">
											<i class="fas fa-share"></i>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>

<div class="modal fade reportpost" id="reportpost" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-body pb-0">
				<div class="modal-title">
					<h3>Select Report Post Issue</h3>
				</div>
				<div class="reportpost__type">
					<ul>
						<li><a href="#">Spam Post</a></li>
						<li><a href="#">Represents Violence</a></li>
						<li><a href="#">Obscene Content</a></li>
						<li><a href="#">Racist Post</a></li>
					</ul>
				</div>
			</div>
			<div class="btn-close" data-dismiss="modal"><img src="assets/images/close-black-icon.svg"></div>
		</div>
	</div>
</div>

<div class="modal fade postcomment" id="postcomment" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<div class="modal-title">
					<h3>Comments</h3>
				</div>
				<div class="postcomment__main">
					<div class="postcomment__list">
						
						<!-- Post Comment Repeater -->
						<div class="postcomment__item">
							<div class="postcomment__box">
								<div class="postcomment__top d-flex align-items-center justify-content-between">
									<div class="left">
										<a href="#" class="d-inline-flex align-items-center">
											<img src="assets/images/client14.jpg">
											William Rose
										</a>
									</div>
									<div class="right d-inline-flex align-items-center">
										<div class="replay">
											<a href="#">Reply <i class="fas fa-share"></i></a>
										</div>
										<div class="option">
											<img src="assets/images/dots-icon.svg">
										</div>
									</div>
								</div>
								<div class="postcomment__text">All the best boys</div>
							</div>
						</div>
						
						<div class="postcomment__item">
							<div class="postcomment__box">
								<div class="postcomment__top d-flex align-items-center justify-content-between">
									<div class="left">
										<a href="#" class="d-inline-flex align-items-center">
											<img src="assets/images/client11.jpg">
											William Jones
										</a>
									</div>
									<div class="right d-inline-flex align-items-center">
										<div class="replay">
											<a href="#">Reply <i class="fas fa-share"></i></a>
										</div>
										<div class="option">
											<img src="assets/images/dots-icon.svg">
										</div>
									</div>
								</div>
								<div class="postcomment__text">We all know whos gonna win...</div>
							</div>
							<!-- Post Comment Sub Main -->
							<div class="postcomment__level2">
								<!-- Post Comment Sub Repeater -->
								<div class="postcomment__box">
									<div class="postcomment__top d-flex align-items-center justify-content-between">
										<div class="left">
											<a href="#" class="d-inline-flex align-items-center">
												<img src="assets/images/client2.jpg">
												Steve Smith
											</a>
										</div>
										<div class="right d-inline-flex align-items-center">
											<div class="replay">
												<a href="#">Reply <i class="fas fa-share"></i></a>
											</div>
											<div class="option">
												<img src="assets/images/dots-icon.svg">
											</div>
										</div>
									</div>
									<div class="postcomment__text"><span class="tag">William Jones</span> Its too obvious bro...</div>
								</div>									
								<div class="postcomment__box">
									<div class="postcomment__top d-flex align-items-center justify-content-between">
										<div class="left">
											<a href="#" class="d-inline-flex align-items-center">
												<img src="assets/images/client11.jpg">
												William Jones
											</a>
										</div>
										<div class="right d-inline-flex align-items-center">
											<div class="replay">
												<a href="#">Reply <i class="fas fa-share"></i></a>
											</div>
											<div class="option">
												<img src="assets/images/dots-icon.svg">
											</div>
										</div>
									</div>
									<div class="postcomment__text"><span class="tag">Steve Smith</span> I know right! Game is on...</div>
								</div>
								<!-- Load More Replays -->
								<div class="loadmore-replays">
									<a href="#">Show more (25)</a>
								</div>
							</div>
						</div>

						<div class="postcomment__item">
							<div class="postcomment__box">
								<div class="postcomment__top d-flex align-items-center justify-content-between">
									<div class="left">
										<a href="#" class="d-inline-flex align-items-center">
											<img src="assets/images/client10.jpg">
											Micheal Martin
										</a>
									</div>
									<div class="right d-inline-flex align-items-center">
										<div class="replay">
											<a href="#">Reply <i class="fas fa-share"></i></a>
										</div>
										<div class="option">
											<img src="assets/images/dots-icon.svg">
										</div>
									</div>
								</div>
								<div class="postcomment__text">All the best boys</div>
							</div>
						</div>

						<!-- Load More Comments -->
						<div class="loadmore-comments">
							<a href="#"><img src="assets/images/down-blue-rounded-icon.svg"></a>
						</div>

					</div>
				</div>
				<div class="postcomment__write-main">
					<div class="postcomment__write-top">
						<div class="postcomment__replay-label d-flex align-items-center justify-content-between">
							<div class="left"><span>×</span> Replying to</div>
							<div class="right">
								<div class="postcomment__replay-user">
									<div class="postcomment__replay-box d-flex align-items-center">
										<img src="assets/images/client14.jpg">William Rose
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="postcomment__write-box">
						<div class="postcomment__textarea">
							<textarea class="textarea autogrow2" placeholder="Write Comment"></textarea>
							<button type="submit"><img src="assets/images/submit-rounded-icon.svg"></button>
						</div>
					</div>
				</div>
			</div>
			<div class="btn-close" data-dismiss="modal"><img src="assets/images/close-black-icon.svg"></div>
		</div>
	</div>
</div>

<?php include 'include/footer.php'; ?>