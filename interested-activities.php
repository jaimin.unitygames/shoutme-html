<?php include 'include/head-login-register.php';?>

<?php include 'include/header-login-register.php';?>

<section class="login-register">
	<div class="container">
		<div class="multistep-form">
			<div class="multistep-top d-flex flex-wrap justify-content-between">
				<div class="multistep-top__box multistep-top__box--complete">
					<a href="#">
						<div class="multistep-top__icon">
							<svg xmlns="http://www.w3.org/2000/svg" width="11.672" height="20" viewBox="0 0 11.672 20">
								<defs><style>.mbnumv{fill-rule:evenodd;}</style></defs><g transform="translate(-74 -8)"><g transform="translate(74 8)">
								<path class="mbnumv" d="M76.145,8H83.59a2.156,2.156,0,0,1,2.082,2.145v15.71A2.114,2.114,0,0,1,83.59,28H76.145A2.127,2.127,0,0,1,74,25.855V10.145A2.168,2.168,0,0,1,76.145,8Zm3.722,16.972a1.075,1.075,0,0,1,0,2.145,1.073,1.073,0,1,1,0-2.145ZM76.145,10.461H83.59a.622.622,0,0,1,.568.631V23.457a.565.565,0,0,1-.568.568H76.145a.583.583,0,0,1-.631-.568V11.091A.632.632,0,0,1,76.145,10.461Zm2.713-1.451H81a.284.284,0,0,1,0,.568H78.858A.287.287,0,1,1,78.858,9.009Z" transform="translate(-74 -8)"/></g></g>
							</svg>
						</div>
						<span>Mobile Number Verification</span>
					</a>
				</div>
				<div class="multistep-top__box multistep-top__box--complete">
					<a href="#">
						<div class="multistep-top__icon">
							<svg xmlns="http://www.w3.org/2000/svg" width="17.99" height="20" viewBox="0 0 17.99 20">
								<g class="a" transform="translate(17 9.501)"><g transform="translate(-17 -9.501)"><path class="b" d="M.485,20l-.192-.752a8.9,8.9,0,0,1,3.52-9.565,6.972,6.972,0,0,0,10.375,0A8.915,8.915,0,0,1,17.7,19.254L17.5,20ZM4,5a5,5,0,1,1,5,5A5,5,0,0,1,4,5Z" transform="translate(0)"/></g></g>
							</svg>
						</div>
						<span>About Yourself</span>
					</a>
				</div>
				<div class="multistep-top__box multistep-top__box--active">
					<a href="#">
						<div class="multistep-top__icon">
							<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20">
								<g class="a" transform="translate(-3 -2)"><g transform="translate(3 2)"><g transform="translate(0 0)"><path class="b" d="M3,203.474v-5.263a1.053,1.053,0,0,1,1.053-1.053H9.316v-1.053a2.105,2.105,0,0,1,4.211,0v1.053h5.263a1.053,1.053,0,0,1,1.053,1.053v5.263H20.9a2.1,2.1,0,0,1,2.1,2.1v0a2.106,2.106,0,0,1-2.105,2.105H19.842v5.263A1.053,1.053,0,0,1,18.789,214H13.526v-1.053a2.105,2.105,0,0,0-4.211,0V214H4.053A1.053,1.053,0,0,1,3,212.947v-5.263H4.053a2.105,2.105,0,0,0,0-4.211Z" transform="translate(-3 -194)"/></g></g></g>
							</svg>
						</div>
						<span>Interested Activities</span>
					</a>
				</div>
				<div class="multistep-top__box">
					<a href="#">
						<div class="multistep-top__icon">
							<svg xmlns="http://www.w3.org/2000/svg" width="17.471" height="20" viewBox="0 0 17.471 20">
								<g class="a" transform="translate(-8.5 -2.5)"><path class="b" d="M24.074,2.5H10.418A1.911,1.911,0,0,0,8.5,4.418V20.582A1.911,1.911,0,0,0,10.418,22.5H24.053a1.911,1.911,0,0,0,1.918-1.918V4.418A1.893,1.893,0,0,0,24.074,2.5ZM13.347,18.074h-.3a.969.969,0,1,1,0-1.939h.3a.969.969,0,0,1,0,1.939Zm0-4.594h-.3a.969.969,0,0,1,0-1.939h.3a.969.969,0,0,1,0,1.939Zm0-4.594h-.3a.967.967,0,0,1-.969-.969.954.954,0,0,1,.969-.969h.3a.967.967,0,0,1,.969.969A.954.954,0,0,1,13.347,8.886Zm8.093,9.189H16.593a.969.969,0,1,1,0-1.939h4.868a.967.967,0,0,1,.969.969A1.014,1.014,0,0,1,21.44,18.074Zm0-4.594H16.593a.969.969,0,1,1,0-1.939h4.868a.967.967,0,0,1,.969.969A1.014,1.014,0,0,1,21.44,13.48Zm0-4.594H16.593a.967.967,0,0,1-.969-.969.954.954,0,0,1,.969-.969h4.868a.967.967,0,0,1,.969.969A.985.985,0,0,1,21.44,8.886Z"/></g>
							</svg>
						</div>
						<span>Final Step</span>
					</a>
				</div>
			</div>
			<div class="multistep-bottom step3">
				<div class="interested-activity">
					<div class="row">
						<div class="interested-activity__col">
							<div class="interested-activity__box">
								<div class="interested-activity__game">
								<input type="checkbox" name="football" id="football" value="football">
									<div class="backgroundcolor"></div>
									<div class="interested-activity__game-icon">
										<svg xmlns="http://www.w3.org/2000/svg" width="35" height="35" viewBox="0 0 35 35">
											<path class="a" d="M17.5,35A17.5,17.5,0,1,1,35,17.5,17.5,17.5,0,0,1,17.5,35ZM8.066,20.046a1.526,1.526,0,0,0,.117,1.207L10.6,25.116a1.236,1.236,0,0,0,1.035.527L17.2,25.25a1.233,1.233,0,0,0,.948-.664L20.208,19.9a1.252,1.252,0,0,0-.142-1.154L16.54,14.162a1.3,1.3,0,0,0-1.084-.451l-4.551.594a1.248,1.248,0,0,0-.912.719ZM17.5,13.41l3.551,4.621a1.405,1.405,0,0,0,1.089.489l4.548-.311a1.167,1.167,0,0,0,.926-.675l1.726-4.366a1.4,1.4,0,0,0-.14-1.185L26.209,7.7a1.4,1.4,0,0,0-1.062-.54l-5.114.126a1.123,1.123,0,0,0-.93.644l-1.762,4.313A1.3,1.3,0,0,0,17.5,13.41ZM2.716,20.2a.756.756,0,0,0,.856.517l2.509-.491a1.351,1.351,0,0,0,.905-.759L9,14.227a1.962,1.962,0,0,0-.016-1.255L7.54,9.475a.931.931,0,0,0-.928-.538l-1.82.238a1.268,1.268,0,0,0-.921.714L1.6,15.553q-.041.335-.067.675Zm19.243,8.962L18.818,26.8a2.1,2.1,0,0,0-1.214-.357l-5.829.412a1.069,1.069,0,0,0-.879.691l-.565,1.781a1.25,1.25,0,0,0,.266,1.13l2.3,2.387a16.14,16.14,0,0,0,7.66.381l1.627-3.061A.817.817,0,0,0,21.959,29.161ZM24.6,29.1a3.649,3.649,0,0,0-.857,1.007l-1.417,2.667A16.054,16.054,0,0,0,31.009,26.1l-1.416-.189a1.89,1.89,0,0,0-1.211.319Zm6.693-15.791a.907.907,0,0,0-.916.538L28.55,18.463a2.685,2.685,0,0,0-.114,1.29L29.3,24a1.016,1.016,0,0,0,.806.751l.895.119.671.089a16.074,16.074,0,0,0,1.349-11.421ZM25.806,5.284l.355-1.256A15.939,15.939,0,0,0,17.5,1.486a16.11,16.11,0,0,0-2.849.253l-.466,1.284a.772.772,0,0,0,.375.933L18.282,5.8a3.244,3.244,0,0,0,1.281.283l5.382-.132A.974.974,0,0,0,25.806,5.284ZM4.63,7.97l.67-.088,2.01-.263a3.092,3.092,0,0,0,1.213-.486L11.9,4.652a2.619,2.619,0,0,0,.773-1.033l.565-1.559A16.034,16.034,0,0,0,4.63,7.97ZM7.5,30.142a8.14,8.14,0,0,0,2.557,1.729l-.668-.821a2.435,2.435,0,0,0-1.08-.7Z"/>
										</svg>
									</div>
									<span>Football</span>
								</div>
								<div class="interested-activity__level">
									<select class="form-control">
										<option>Select Level</option>
										<option>Beginner</option>
										<option>Intermediate</option>
										<option>Expert</option>
									</select>
								</div>
							</div>
						</div>
						<div class="interested-activity__col">
							<div class="interested-activity__box">
								<div class="interested-activity__game">
								<input type="checkbox" name="football" id="football" value="football">
									<div class="backgroundcolor"></div>
									<div class="interested-activity__game-icon">
										<svg xmlns="http://www.w3.org/2000/svg" width="35" height="35" viewBox="0 0 35 35">
											<g transform="translate(5.987 41.987)"><path class="a" d="M6.063,30.749,17.455,19.354l4.776,4.775A20.141,20.141,0,0,0,17.38,35,17.432,17.432,0,0,1,6.063,30.749Zm18.07-4.717,4.756,4.758a17.446,17.446,0,0,1-8.772,4.018A17.414,17.414,0,0,1,24.133,26.032Zm1.9-1.9A17.417,17.417,0,0,1,34.8,20.119a17.4,17.4,0,0,1-4.017,8.772ZM0,17.714a20.182,20.182,0,0,0,4.867-1.3A20,20,0,0,0,10.8,12.7l4.761,4.762L4.171,28.843A17.441,17.441,0,0,1,0,17.714Zm19.352-.256L30.746,6.064A17.427,17.427,0,0,1,35,17.38a20.119,20.119,0,0,0-10.869,4.852ZM35,17.5v0ZM12.666,10.769A20.064,20.064,0,0,0,15.978,5.3a20.1,20.1,0,0,0,1.349-5.3h-.292A17.461,17.461,0,0,1,28.843,4.174L17.455,15.558ZM4.211,6.109,8.9,10.794A17.428,17.428,0,0,1,.182,14.968,17.43,17.43,0,0,1,4.211,6.109Zm1.9-1.9A17.394,17.394,0,0,1,14.588.241,17.421,17.421,0,0,1,10.76,8.863Z" transform="translate(-5.987 -41.987)"/></g>
										</svg>
									</div>
									<span>Basketball</span>
								</div>
								<div class="interested-activity__level">
									<select class="form-control">
										<option>Select Level</option>
										<option>Beginner</option>
										<option>Intermediate</option>
										<option>Expert</option>
									</select>
								</div>
							</div>
						</div>
						<div class="interested-activity__col">
							<div class="interested-activity__box">
								<div class="interested-activity__game">
								<input type="checkbox" name="football" id="football" value="football">
									<div class="backgroundcolor"></div>
									<div class="interested-activity__game-icon">
										<svg xmlns="http://www.w3.org/2000/svg" width="35" height="35" viewBox="0 0 35 35">
											<g transform="translate(0 0)"><path class="a" d="M5.418,35a5.419,5.419,0,0,1,0-10.837H16.866l11.8-23.6A1.025,1.025,0,0,1,30.04.108l4.393,2.2h0a1.025,1.025,0,0,1,.459,1.376L19.515,34.433A1.025,1.025,0,0,1,18.6,35ZM2.051,13.107V11.68a8.124,8.124,0,0,0,1.256.846,13.423,13.423,0,0,0,6.359,1.46,13.413,13.413,0,0,0,6.366-1.464,8.122,8.122,0,0,0,1.25-.843v1.428c0,2.538-3.437,4.32-7.615,4.32S2.051,15.652,2.051,13.107Zm0-5.492c0-2.538,3.437-4.32,7.615-4.32s7.615,1.776,7.615,4.32-3.437,4.32-7.615,4.32S2.051,10.16,2.051,7.615Z" transform="translate(0 0)"/></g>
										</svg>
									</div>
									<span>Hockey</span>
								</div>
								<div class="interested-activity__level">
									<select class="form-control">
										<option>Select Level</option>
										<option>Beginner</option>
										<option>Intermediate</option>
										<option>Expert</option>
									</select>
								</div>
							</div>
						</div>
						<div class="interested-activity__col">
							<div class="interested-activity__box">
								<div class="interested-activity__game">
								<input type="checkbox" name="football" id="football" value="football">
									<div class="backgroundcolor"></div>
									<div class="interested-activity__game-icon">
										<svg xmlns="http://www.w3.org/2000/svg" width="35" height="35" viewBox="0 0 35 35">
											<g transform="translate(-5 -5)"><path class="a" d="M13.547,13.547A30.589,30.589,0,0,0,6.322,24.609a23.548,23.548,0,0,1,8.551,5.518,23.548,23.548,0,0,1,5.518,8.551,30.589,30.589,0,0,0,11.062-7.225,30.588,30.588,0,0,0,7.225-11.062,23.548,23.548,0,0,1-8.551-5.518,23.548,23.548,0,0,1-5.518-8.551A30.588,30.588,0,0,0,13.547,13.547Zm12.039,2.486,1.141,1.141,1.049-1.049a.778.778,0,1,1,1.1,1.1l-1.049,1.049,1.141,1.141a.778.778,0,1,1-1.1,1.1l-1.141-1.141-1.718,1.718,1.141,1.141a.778.778,0,1,1-1.1,1.1l-1.141-1.141-1.717,1.717,1.141,1.141a.778.778,0,0,1-1.1,1.1l-1.141-1.141-1.718,1.718,1.141,1.141a.778.778,0,0,1-1.1,1.1l-1.141-1.141-1.049,1.049a.778.778,0,1,1-1.1-1.1l1.049-1.049-1.141-1.141a.778.778,0,0,1,1.1-1.1l1.141,1.141,1.718-1.718-1.141-1.141a.778.778,0,1,1,1.1-1.1l1.141,1.141,1.717-1.717-1.141-1.141a.778.778,0,0,1,1.1-1.1l1.141,1.141,1.718-1.718-1.141-1.141a.778.778,0,0,1,1.1-1.1ZM5.448,27.6a20.479,20.479,0,0,1,7.224,4.725A20.478,20.478,0,0,1,17.4,39.552c-4.524.988-8.372.3-10.311-1.639S4.46,32.126,5.448,27.6ZM32.327,12.673A20.478,20.478,0,0,1,27.6,5.448c4.524-.988,8.371-.3,10.311,1.639S40.54,12.874,39.552,17.4a20.477,20.477,0,0,1-7.224-4.725Z" transform="translate(0)"/></g>
										</svg>
									</div>
									<span>Rugby</span>
								</div>
								<div class="interested-activity__level">
									<select class="form-control">
										<option>Select Level</option>
										<option>Beginner</option>
										<option>Intermediate</option>
										<option>Expert</option>
									</select>
								</div>
							</div>
						</div>
						<div class="interested-activity__col">
							<div class="interested-activity__box">
								<div class="interested-activity__game">
								<input type="checkbox" name="football" id="football" value="football">
									<div class="backgroundcolor"></div>
									<div class="interested-activity__game-icon">
										<svg xmlns="http://www.w3.org/2000/svg" width="35" height="35" viewBox="0 0 35 35">
											<g transform="translate(-0.5 -0.5)"><path class="a" d="M25.021,30.159A4.841,4.841,0,1,1,29.862,35,4.831,4.831,0,0,1,25.021,30.159ZM4.542,33.883l-3.425-3.5a3.866,3.866,0,0,1,0-5.436L17.649,8.415a3.957,3.957,0,0,1,5.511,0l.446.447L32.394,0,35,2.606l-8.862,8.787.447.447a3.957,3.957,0,0,1,0,5.51L10.053,33.883A3.825,3.825,0,0,1,7.3,35,3.824,3.824,0,0,1,4.542,33.883Z" transform="translate(0.5 0.5)"/></g>
										</svg>
									</div>
									<span>Cricket</span>
								</div>
								<div class="interested-activity__level">
									<select class="form-control">
										<option>Select Level</option>
										<option>Beginner</option>
										<option>Intermediate</option>
										<option>Expert</option>
									</select>
								</div>
							</div>
						</div>
						<div class="interested-activity__col">
							<div class="interested-activity__box">
								<div class="interested-activity__game">
								<input type="checkbox" name="football" id="football" value="football">
									<div class="backgroundcolor"></div>
									<div class="interested-activity__game-icon">
										<svg xmlns="http://www.w3.org/2000/svg" width="35" height="35" viewBox="0 0 35 35">
											<g transform="translate(-0.5 -0.5)"><path class="a" d="M30.847,34.288l-4.435-4.434,3.44-3.441c2.044,2.046,4.436,4.435,4.436,4.434a2.433,2.433,0,0,1-3.44,3.441Zm-7.009-7c-1.095-1.1-8.982-2.221-10.393-2.28l-.252-.015a14.167,14.167,0,0,1-8.959-4.2C-.944,15.617-1.442,7.691,3.123,3.126l0,0C7.692-1.442,15.616-.944,20.791,4.237a14.18,14.18,0,0,1,4.2,8.959s.011.183.014.252c.06,1.41,1.182,9.3,2.281,10.394.244.243,1,1,1,1l-3.443,3.445Zm-1.683-5.127a8.255,8.255,0,0,1-2.311,1.664l5.176,1.2-1.2-5.177A8.3,8.3,0,0,1,22.156,22.158Zm-8.137.435a9.115,9.115,0,0,0,3.5-.694l-1.406-1.4ZM8.864,21.268a11.34,11.34,0,0,0,2.544,1l-1.769-1.77ZM10.735,19.4l2.143,2.143L15.017,19.4l-2.14-2.14Zm6.48,0,1.733,1.734A7.464,7.464,0,0,0,21.1,19l-1.74-1.74ZM5.268,18.385c.208.243.425.485.656.717A12.544,12.544,0,0,0,7.5,20.433L8.538,19.4,6.4,17.256Zm8.708-2.228L16.118,18.3l2.14-2.142-2.141-2.14Zm-6.481,0L9.638,18.3l2.14-2.14L9.636,14.015Zm12.962,0,1.421,1.422a9.085,9.085,0,0,0,.716-3.559ZM2.943,14.23a11.762,11.762,0,0,0,1.384,2.9l.972-.971L3.157,14.015Zm1.313-1.314L6.4,15.058l2.142-2.142L6.4,10.774Zm12.961,0,2.139,2.141L21.5,12.915l-2.143-2.141Zm-6.481,0,2.142,2.141,2.14-2.141-2.141-2.142Zm3.24-3.24,2.142,2.14,2.139-2.139L16.118,7.534ZM2.435,11.1l.721.72L5.3,9.676,3.126,7.505A9.123,9.123,0,0,0,2.435,11.1Zm5.06-1.422,2.141,2.141,2.142-2.139L9.638,7.534Zm12.962,0,1.833,1.835a11.566,11.566,0,0,0-1.019-2.648ZM17.215,6.435l2.143,2.141L20.435,7.5A12.661,12.661,0,0,0,19.1,5.923c-.233-.231-.473-.448-.719-.657Zm-6.48,0,2.142,2.141,2.14-2.141L12.878,4.293ZM4.876,4.874a7.937,7.937,0,0,0-.979,1.2l2.5,2.5,2.14-2.139L6.032,3.93A8.005,8.005,0,0,0,4.876,4.874Zm9.1-1.678,2.142,2.142,1.009-1.012a11.657,11.657,0,0,0-2.9-1.383ZM7.449,3.147,9.638,5.336l2.139-2.141-.761-.762A9.082,9.082,0,0,0,7.449,3.147ZM27.935,8.322a6.123,6.123,0,0,0,0-7.558,4.543,4.543,0,0,1,5.044,0,6.121,6.121,0,0,0,0,7.557,4.542,4.542,0,0,1-5.045,0Zm5.037-3.778a4.833,4.833,0,0,1,.974-2.909,4.545,4.545,0,0,1,0,5.819A4.838,4.838,0,0,1,32.972,4.544Zm-7.06,0a4.522,4.522,0,0,1,1.055-2.909,4.831,4.831,0,0,1,0,5.819A4.522,4.522,0,0,1,25.912,4.544Z" transform="translate(0.5 0.5)"/></g>
										</svg>
									</div>
									<span>Tennis</span>
								</div>
								<div class="interested-activity__level">
									<select class="form-control">
										<option>Select Level</option>
										<option>Beginner</option>
										<option>Intermediate</option>
										<option>Expert</option>
									</select>
								</div>
							</div>
						</div>
						<div class="interested-activity__col">
							<div class="interested-activity__box">
								<div class="interested-activity__game">
								<input type="checkbox" name="football" id="football" value="football">
									<div class="backgroundcolor"></div>
									<div class="interested-activity__game-icon">
										<svg xmlns="http://www.w3.org/2000/svg" width="35" height="35.016" viewBox="0 0 35 35.016">
											<g transform="translate(8.933 36)"><path class="a" d="M2.057,35a2.516,2.516,0,0,1-1.314-.722h0c-.721-.721-.964-1.742-.521-2.185L7.7,24.612,10.4,27.318,2.928,34.794a.864.864,0,0,1-.624.222A1.535,1.535,0,0,1,2.057,35Zm21.9-6.332a3.863,3.863,0,1,1,3.863,3.863A3.863,3.863,0,0,1,23.954,28.663ZM10.79,25.719l8.452-3.3a9.082,9.082,0,0,0,1.724.167h.018a9.58,9.58,0,0,0,2.716-.4L11.892,26.821Zm-2.6-2.6-.016-.013L12.816,11.3a9.573,9.573,0,0,0-.4,2.735,9.045,9.045,0,0,0,.166,1.719L9.275,24.2Zm11.893-1.984L21.3,19.924l1.135,1.134a7.954,7.954,0,0,1-1.445.133A7.734,7.734,0,0,1,20.087,21.138Zm-3.73-1.585,2.288-2.288,1.747,1.747L18.6,20.806A6.988,6.988,0,0,1,16.357,19.554Zm5.855-.54,1.6-1.6,2.126,2.126a8.879,8.879,0,0,1-2.074,1.118Zm2.507-2.506,1.6-1.6,2.356,2.355-1.729,1.463ZM14.194,16.4l1.793-1.793,1.747,1.747-2.287,2.288A6.992,6.992,0,0,1,14.194,16.4Zm5.36-.046,1.6-1.6L22.9,16.507l-1.6,1.6ZM27.225,14l1.6-1.6,2.564,2.564-1.729,1.463Zm-5.164-.151,1.6-1.6L25.4,14l-1.6,1.6ZM16.9,13.7l1.6-1.6,1.747,1.747-1.6,1.6Zm-2.955-1.135L15.076,13.7l-1.215,1.215A7.859,7.859,0,0,1,13.942,12.564Zm15.789-1.07,1.6-1.6L34.033,12.6a4.745,4.745,0,0,1-.658.68l-1,.85Zm-5.164-.151,1.6-1.6,1.747,1.747-1.6,1.6ZM19.4,11.192,21,9.6l1.747,1.747-1.6,1.6Zm-5.064-.052a8.877,8.877,0,0,1,1.118-2.074l2.126,2.126-1.6,1.6Zm17.9-2.152,1.706-1.706.012.012A3.638,3.638,0,0,1,35,10.056a4.3,4.3,0,0,1-.3,1.392Zm-5.164-.151,1.6-1.6,1.747,1.747-1.6,1.6ZM21.91,8.686l1.6-1.6,1.747,1.747-1.6,1.6Zm-5.639-.627L17.734,6.33l2.355,2.355-1.6,1.6ZM29.58,6.33l1.706-1.706,1.747,1.747L31.327,8.077Zm-5.164-.15,1.6-1.6L27.759,6.33l-1.6,1.6Zm-5.848-.835,1.463-1.729L22.6,6.179,21,7.775Zm8.355-1.671,1.705-1.706,1.747,1.747L28.67,5.42ZM20.866,2.628l.85-1A4.729,4.729,0,0,1,22.4.966L25.1,3.673l-1.6,1.6ZM23.552.3A4.282,4.282,0,0,1,24.943,0c.058,0,.115,0,.171,0a3.616,3.616,0,0,1,2.591,1.044l.012.012L26.012,2.762Z" transform="translate(-8.933 -36)"/></g>
										</svg>
									</div>
									<span>Squash</span>
								</div>
								<div class="interested-activity__level">
									<select class="form-control">
										<option>Select Level</option>
										<option>Beginner</option>
										<option>Intermediate</option>
										<option>Expert</option>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<button type="submit" class="btn-custom btn-black">Next <img src="assets/images/arrow.svg"></button>
					</div>
				</div>
			</div>
		</div>
	</div>

</section>

<?php include 'include/footer-login-register.php';?>