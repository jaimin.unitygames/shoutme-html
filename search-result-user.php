<?php include 'include/head.php';?>

<?php include 'include/header.php';?>

<div class="searchpage">
	<div class="container w-1000">
		<div class="searchpage__top">
			<div class="row align-items-center justify-content-between">
				<div class="searchpage__back">
					<a href="home.php"><img src="assets/images/back-btn-white.svg"></a>
				</div>
				<div class="searchpage__search-box">
					<div class="form-group">
						<input type="text" name="" class="form-control" placeholder="Search">
						<button type="submit"><img src="assets/images/search-btn-icon-blue.svg"></button>
					</div>
				</div>
				<div class="searchpage__filter-wrap">
					<div class="searchpage__filter">
						<a href="search-result-user.php" class="filter-btn active">Users</a>
						<a href="search-result-post.php" class="filter-btn">Posts</a>
					</div>
				</div>
			</div>
		</div>
		<div class="searchpage__result-main">
			<div class="searchpage__users">
				<div class="row">
					<div class="col-md-6">
						<div class="searchpage__users-box">
							<div class="searchpage__users-top d-flex align-items-center justify-content-between">
								<div class="left">
									<a href="#" class="d-inline-flex align-items-center">
										<img src="assets/images/client7.jpg" alt="">
										John Smith <span>(21)</span>
									</a>
								</div>
								<div class="right d-flex align-items-center">
									<a href="#"><img src="assets/images/search-announce-icon-blue.svg" alt=""></a>
									<a href="#"><img src="assets/images/add-user-icon.svg" alt=""></a>
									<a href="#"><img src="assets/images/message-icon.svg" alt=""></a>
								</div>
							</div>
							<div class="searchpage__users-content">
								It’s hard to beat a person who never gives up
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="searchpage__users-box">
							<div class="searchpage__users-top d-flex align-items-center justify-content-between">
								<div class="left">
									<a href="#" class="d-inline-flex align-items-center">
										<img src="assets/images/client7.jpg" alt="">
										John Smith <span>(21)</span>
									</a>
								</div>
								<div class="right d-flex align-items-center">
									<a href="#"><img src="assets/images/search-announce-icon-blue.svg" alt=""></a>
									<a href="#"><img src="assets/images/add-user-icon.svg" alt=""></a>
									<a href="#"><img src="assets/images/message-icon.svg" alt=""></a>
								</div>
							</div>
							<div class="searchpage__users-content">
								It’s hard to beat a person who never gives up
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="searchpage__users-box">
							<div class="searchpage__users-top d-flex align-items-center justify-content-between">
								<div class="left">
									<a href="#" class="d-inline-flex align-items-center">
										<img src="assets/images/client7.jpg" alt="">
										John Smith <span>(21)</span>
									</a>
								</div>
								<div class="right d-flex align-items-center">
									<a href="#"><img src="assets/images/search-announce-icon-blue.svg" alt=""></a>
									<a href="#"><img src="assets/images/add-user-icon.svg" alt=""></a>
									<a href="#"><img src="assets/images/message-icon.svg" alt=""></a>
								</div>
							</div>
							<div class="searchpage__users-content">
								It’s hard to beat a person who never gives up
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="searchpage__users-box">
							<div class="searchpage__users-top d-flex align-items-center justify-content-between">
								<div class="left">
									<a href="#" class="d-inline-flex align-items-center">
										<img src="assets/images/client7.jpg" alt="">
										John Smith <span>(21)</span>
									</a>
								</div>
								<div class="right d-flex align-items-center">
									<a href="#"><img src="assets/images/search-announce-icon-blue.svg" alt=""></a>
									<a href="#"><img src="assets/images/add-user-icon.svg" alt=""></a>
									<a href="#"><img src="assets/images/message-icon.svg" alt=""></a>
								</div>
							</div>
							<div class="searchpage__users-content">
								It’s hard to beat a person who never gives up
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="searchpage__users-box">
							<div class="searchpage__users-top d-flex align-items-center justify-content-between">
								<div class="left">
									<a href="#" class="d-inline-flex align-items-center">
										<img src="assets/images/client7.jpg" alt="">
										John Smith <span>(21)</span>
									</a>
								</div>
								<div class="right d-flex align-items-center">
									<a href="#"><img src="assets/images/search-announce-icon-blue.svg" alt=""></a>
									<a href="#"><img src="assets/images/add-user-icon.svg" alt=""></a>
									<a href="#"><img src="assets/images/message-icon.svg" alt=""></a>
								</div>
							</div>
							<div class="searchpage__users-content">
								It’s hard to beat a person who never gives up
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="searchpage__users-box">
							<div class="searchpage__users-top d-flex align-items-center justify-content-between">
								<div class="left">
									<a href="#" class="d-inline-flex align-items-center">
										<img src="assets/images/client7.jpg" alt="">
										John Smith <span>(21)</span>
									</a>
								</div>
								<div class="right d-flex align-items-center">
									<a href="#"><img src="assets/images/search-announce-icon-blue.svg" alt=""></a>
									<a href="#"><img src="assets/images/add-user-icon.svg" alt=""></a>
									<a href="#"><img src="assets/images/message-icon.svg" alt=""></a>
								</div>
							</div>
							<div class="searchpage__users-content">
								It’s hard to beat a person who never gives up
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="searchpage__users-box">
							<div class="searchpage__users-top d-flex align-items-center justify-content-between">
								<div class="left">
									<a href="#" class="d-inline-flex align-items-center">
										<img src="assets/images/client7.jpg" alt="">
										John Smith <span>(21)</span>
									</a>
								</div>
								<div class="right d-flex align-items-center">
									<a href="#"><img src="assets/images/search-announce-icon-blue.svg" alt=""></a>
									<a href="#"><img src="assets/images/add-user-icon.svg" alt=""></a>
									<a href="#"><img src="assets/images/message-icon.svg" alt=""></a>
								</div>
							</div>
							<div class="searchpage__users-content">
								It’s hard to beat a person who never gives up
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="searchpage__users-box">
							<div class="searchpage__users-top d-flex align-items-center justify-content-between">
								<div class="left">
									<a href="#" class="d-inline-flex align-items-center">
										<img src="assets/images/client7.jpg" alt="">
										John Smith <span>(21)</span>
									</a>
								</div>
								<div class="right d-flex align-items-center">
									<a href="#"><img src="assets/images/search-announce-icon-blue.svg" alt=""></a>
									<a href="#"><img src="assets/images/add-user-icon.svg" alt=""></a>
									<a href="#"><img src="assets/images/message-icon.svg" alt=""></a>
								</div>
							</div>
							<div class="searchpage__users-content">
								It’s hard to beat a person who never gives up
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php include 'include/footer.php';?>