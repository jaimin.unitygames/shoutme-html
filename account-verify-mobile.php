<?php include 'include/head.php'; ?>

<?php include 'include/header.php'; ?>

<div class="account">
	<div class="container">
		<div class="row">
			<div class="col-xl-3 col-md-4">
				<div class="account-sidebar-wrap">
					<div class="account-menu">
						Account Settings <i class="fas fa-bars"></i>
					</div>
					<div class="account-sidebar bg-white">
						<div class="close">×</div>
						<ul class="account-sidebar__links">
							<li>
								<a href="account-verify-mobile.php" class="active"><i class="fas fa-mobile-alt"></i>Verify Mobile Number</a>
							</li>
							<li>
								<a href="account-favourite-post.php"><i class="fas fa-heart"></i>Favorite Post</a>
							</li>
							<li>
								<a href="account-saved-post.php"><i class="fas fa-bookmark"></i>Saved Post</a>
							</li>
							<li>
								<a href="account-transaction-history.php"><i class="fas fa-exchange-alt"></i>Transaction History</a>
							</li>
							<li>
								<a href="account-change-password.php"><i class="fas fa-lock"></i>Change Password</a>
							</li>
							<li>
								<a href="account-terms-conditions.php"><i class="fas fa-clipboard-list"></i>Terms & Conditions</a>
							</li>
							<li>
								<a href="account-privacy-policy.php"><i class="fas fa-mobile-alt"></i>Privacy Policy</a>
							</li>
							<li>
								<a href="account-faq.php"><i class="fas fa-comments"></i>FAQs</a>
							</li>
							<li>
								<a href="account-feedback.php"><i class="fas fa-thumbs-up"></i>Feedback</a>
							</li>
							<li>
								<a href="account-delete.php"><i class="fas fa-trash"></i>Delete My Account</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-xl-9 col-md-8">
				<div class="account-settings">
					<div class="white-title">
						<h3>Verify Mobile Number</h3>
					</div>
					<div class="w-335">
						<div class="verify-number"><!-- Step 1 -->
							<div class="form-group">
								<label>Please enter your mobile number</label>
								<div class="input-group d-flex">
									<select name="" id="" class="form-control blue mr-2" style="width: 40px;">
										<option value="uk">United Kingdom</option>
										<option value="india">India</option>
									</select>
									<input type="text" class="form-control" placeholder="000 000 0000" value="000 000 0000">
								</div>
							</div>
							<div class="form-group">
								<button type="submit" class="btn-custom btn-blue">Submit</button>
							</div>
						</div>

						<div class="verify-otp"><!-- Step 2 -->
							<div class="form-group">
								<label>Please Enter One Time Password we sent on +44 000 000 0000</label>
								<div class="input-group d-flex justify-content-between">
									<input type="text" class="form-control" max="1">
									<input type="text" class="form-control" max="1">
									<input type="text" class="form-control" max="1">
									<input type="text" class="form-control" max="1">
									<input type="text" class="form-control" max="1">
									<input type="text" class="form-control" max="1">
								</div>
							</div>
							<div class="form-group orange-link text-center">
								<a href="#">Resend OTP</a>
							</div>
							<div class="form-group">
								<button type="submit" class="btn-custom btn-blue">Submit</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php include 'include/footer.php'; ?>