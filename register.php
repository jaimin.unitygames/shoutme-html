<?php include 'include/head-login-register.php';?>

<?php include 'include/header-login-register.php';?>

<section class="login-register">
	<div class="container">
		<div class="wid-335">
			<h2>REGISTER</h2>
			<form>
				<div class="form-group">
					<label for="fullname">Full Name</label>
					<div class="input-group">
						<div class="icon">
							<img src="assets/images/birthdate-icon.svg">
						</div>
						<input type="text" id="fullname" class="form-control" placeholder="John Smith">
					</div>
					<!-- <div class="error">This is invalid</div> -->
				</div>
				<div class="form-group">
					<label for="email">Email</label>
					<div class="input-group">
						<div class="icon">
							<img src="assets/images/email-icon.svg">
						</div>
						<input type="email" id="email" class="form-control" placeholder="john@mail.com">
					</div>
					<!-- <div class="error">This is invalid</div> -->
				</div>
				<div class="form-group">
					<label for="password">Password</label>
					<div class="input-group">
						<div class="icon">
							<img src="assets/images/password-icon.svg">
						</div>
						<input type="password" id="password" class="form-control" placeholder="•••••••••">
					</div>
					<!-- <div class="error">This is invalid</div> -->
				</div>
				<div class="form-group">
					<label for="password">Gender</label>
					<div class="input-group d-flex flex-wrap radio-custom">
						<div>
							<input type="radio" id="male" name="gender" value="male" checked="true">
							<label for="male">Male</label>
						</div>
						<div>
							<input type="radio" id="female" name="gender" value="female">
							<label for="female">Female</label>
						</div>
					</div>
					<!-- <div class="error">This is invalid</div> -->
				</div>
				<div class="form-group">
					<label for="birthdate">Birthdate</label>
					<div class="input-group">
						<div class="icon">
							<img src="assets/images/birthdate-icon.svg">
						</div>
						<input type="text" id="dob" class="form-control datepicker" placeholder="dd - mm - yyyy" readonly>
					</div>
					<!-- <div class="error">This is invalid</div> -->
				</div>
				<div class="form-group">
					<button type="submit" class="btn-custom btn-black">Register <img src="assets/images/arrow.svg"></button>
				</div>
			</form>
			<div class="login-register__with">
				<span>Or Register with</span>
				<div class="login-register__icon">
					<a href="#"><img src="assets/images/facebook-icon.svg"></a>
					<a href="#"><img src="assets/images/google-icon.svg"></a>
				</div>
			</div>
		</div>
	</div>
	<div class="login-register__now">
		<div class="container">
			<div class="wid-335">
				<span>Already a User?</span>
				<a href="login.php" class="btn-custom btn-blue">Login Now <img src="assets/images/arrow.svg"></a>
			</div>
		</div>
	</div>
</section>

<?php include 'include/footer-login-register.php';?>