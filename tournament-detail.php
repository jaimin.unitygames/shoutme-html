<?php include 'include/head.php'; ?>

<?php include 'include/header.php'; ?>

<div class="activity-detail">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="ml-md-0 mx-auto activity-detail__left mw-370">
					<!-- Map For Mobile -->
					<div class="activity-detail__map d-md-none">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2484.0072458382283!2d-0.10628488402851216!3d51.4947345194499!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4876059dbe55c279%3A0xce04e2436b2b0201!2sFootball%20Ground!5e0!3m2!1sen!2sin!4v1624952164615!5m2!1sen!2sin" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
					</div>
					<div class="activity-detail__title">
						<h3>The London Cup</h3>
					</div>
					<div class="activity-detail__name">Football</div>
					<div class="activity-detail__vs">5 vs 5</div>
					<div class="activity-detail__payment-chat">
						<div class="activity-detail__payment">
							<img src="assets/images/money-icon.svg">
							£10
						</div>
					</div>
					<div class="activity-detail__time d-flex align-items-center">
						<img src="assets/images/yellow-timer-icon.svg">
						12:00, 12th Nov 2020 (2 Hours)
					</div>
					<div class="activity-detail__location d-flex align-items-center">
						<img src="assets/images/yellow-location-icon.svg">
						Public Ground, London
					</div>
					<div class="activity-detail__info-box">
						<h4>Description</h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
					</div>
					<div class="activity-detail__action-btn mb-20 pr-0">
						<a href="#" class="btn-custom btn-black-light">Number of teams that would participate <span>3</span></a>
					</div>
					<div class="activity-detail__info-box">
						<h4>Participating Criteria</h4>
						<table>
							<tr>
								<td align="left">Age</td>
								<td align="right">21 - 25 <img src="assets/images/red-cross-icon.svg" class="icon"></td>
							</tr>
							<tr>
								<td align="left">Gender</td>
								<td align="right">Male <img src="assets/images/green-tick-icon.svg" class="icon"></td>
							</tr>
						</table>
					</div>
					<!-- Schedule Button for Desktop -->
					<div class="activity-detail__action-btn d-none d-md-block pr-0 mb-20">
						<a href="#" class="btn-custom white-border-btn text-left icon-right">Schedule <img src="assets/images/arrow.svg"></a>
					</div>
					<!-- Points Table Button for Desktop -->
					<div class="activity-detail__action-btn d-none d-md-block pr-0 mb-20">
						<a href="#" class="btn-custom white-border-btn text-left icon-right">Points Table <img src="assets/images/arrow.svg"></a>
					</div>
					<!-- Pay and Join Button for Desktop -->
					<div class="activity-detail__action-btn d-none d-md-block pr-0">
						<a href="#" class="btn-custom btn-blue-gradient">Pay & Join Tournament</a>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="mr-md-0 mx-auto activity-detail__right mw-470">
					<!-- Map For Desktop -->
					<div class="activity-detail__map d-none d-md-block">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2484.0072458382283!2d-0.10628488402851216!3d51.4947345194499!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4876059dbe55c279%3A0xce04e2436b2b0201!2sFootball%20Ground!5e0!3m2!1sen!2sin!4v1624952164615!5m2!1sen!2sin" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
					</div>
					<div class="activity-detail__info-box">
						<h4>Tournament Rules</h4>
						<ul class="list">
							<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit</li>
							<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit</li>
						</ul>
					</div>
					<div class="activity-detail__info-box text-center bluebg">
						<h4>Winnings</h4>
						<div class="orange-text">
							<p>iPhone X to winning team</p>
							<p>Goodies to Finalists</p>
						</div>
					</div>
					<div class="activity-detail__info-box organiser-table">
						<h4>Tournament Organiser Details</h4>
						<table>
							<tr>
								<td>Organiser</td>
								<td align="right"><div class="value">William Stan Sports</div></td>
							</tr>
							<tr>
								<td>Contact Number</td>
								<td align="right"><div class="value">+44 000 000 0000</div></td>
							</tr>
						</table>
					</div>
					<!-- Schedule Button for Mobile -->
					<div class="activity-detail__action-btn d-md-none pr-0 mb-20">
						<a href="#" class="btn-custom white-border-btn text-left icon-right">Schedule <img src="assets/images/arrow.svg"></a>
					</div>
					<!-- Points Table Button for Mobile -->
					<div class="activity-detail__action-btn d-md-none pr-0 mb-20">
						<a href="#" class="btn-custom white-border-btn text-left icon-right">Points Table <img src="assets/images/arrow.svg"></a>
					</div>
					<!-- Pay and Join Button for Mobile -->
					<div class="activity-detail__action-btn d-md-none pr-0">
						<a href="#" class="btn-custom btn-blue-gradient">Pay & Join Tournament</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Participant Popup -->
<div class="modal fade participantpopup" id="participant" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<div class="modal-title">
					<h3>Participant List</h3>
				</div>
				<div class="participant-lock d-flex align-items-center justify-content-center">
					<div>
						<img src="assets/images/lock-icon.svg">
						<p>You won't be able to see participants until you participate in the Activity</p>
					</div>
				</div>
			</div>
			<div class="btn-close" data-dismiss="modal"><img src="assets/images/close-black-icon.svg"></div>
		</div>
	</div>
</div>

<!-- Formation Popup -->
<div class="modal fade formationpopup" id="formation" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<div class="modal-title">
					<h3>Team 1</h3>
				</div>
				<div class="formation-ground">
					<img src="assets/images/ground-popup-image.jpg">
				</div>
			</div>
			<div class="btn-close" data-dismiss="modal"><img src="assets/images/close-black-icon.svg"></div>
		</div>
	</div>
</div>

<!-- Request Popup -->
<div class="modal fade requestpopup" id="request" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<div class="modal-title">
					<h3>Requests</h3>
				</div>
				<div class="users contentscroll">
					<div class="row">
						<div class="col-md-6">
							<div class="users-box">
								<div class="users-top d-flex align-items-center justify-content-between">
									<div class="left">
										<a href="#" class="d-inline-flex align-items-center">
											<img src="assets/images/client7.jpg" alt="">
											John Smith <span>(21)</span>
										</a>
									</div>
									<div class="right d-flex align-items-center">
										<a href="#"><img src="assets/images/search-announce-icon-blue.svg" alt=""></a>
										<a href="#"><img src="assets/images/add-user-icon.svg" alt=""></a>
										<a href="#"><img src="assets/images/message-icon.svg" alt=""></a>
									</div>
								</div>
								<div class="users-content">
									It’s hard to beat a person who never gives up
								</div>
								<div class="buttons d-flex align-items-center justify-content-between">
									<a href="#" class="btn-custom">Accept</a>
									<a href="#" class="btn-custom btn-black-light">Reject</a>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="users-box">
								<div class="users-top d-flex align-items-center justify-content-between">
									<div class="left">
										<a href="#" class="d-inline-flex align-items-center">
											<img src="assets/images/client7.jpg" alt="">
											John Smith <span>(21)</span>
										</a>
									</div>
									<div class="right d-flex align-items-center">
										<a href="#"><img src="assets/images/search-announce-icon-blue.svg" alt=""></a>
										<a href="#"><img src="assets/images/add-user-icon.svg" alt=""></a>
										<a href="#"><img src="assets/images/message-icon.svg" alt=""></a>
									</div>
								</div>
								<div class="users-content">
									It’s hard to beat a person who never gives up
								</div>
								<div class="buttons d-flex align-items-center justify-content-between">
									<a href="#" class="btn-custom">Accept</a>
									<a href="#" class="btn-custom btn-black-light">Reject</a>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="users-box">
								<div class="users-top d-flex align-items-center justify-content-between">
									<div class="left">
										<a href="#" class="d-inline-flex align-items-center">
											<img src="assets/images/client7.jpg" alt="">
											John Smith <span>(21)</span>
										</a>
									</div>
									<div class="right d-flex align-items-center">
										<a href="#"><img src="assets/images/search-announce-icon-blue.svg" alt=""></a>
										<a href="#"><img src="assets/images/add-user-icon.svg" alt=""></a>
										<a href="#"><img src="assets/images/message-icon.svg" alt=""></a>
									</div>
								</div>
								<div class="users-content">
									It’s hard to beat a person who never gives up
								</div>
								<div class="buttons d-flex align-items-center justify-content-between">
									<a href="#" class="btn-custom">Accept</a>
									<a href="#" class="btn-custom btn-black-light">Reject</a>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="users-box">
								<div class="users-top d-flex align-items-center justify-content-between">
									<div class="left">
										<a href="#" class="d-inline-flex align-items-center">
											<img src="assets/images/client7.jpg" alt="">
											John Smith <span>(21)</span>
										</a>
									</div>
									<div class="right d-flex align-items-center">
										<a href="#"><img src="assets/images/search-announce-icon-blue.svg" alt=""></a>
										<a href="#"><img src="assets/images/add-user-icon.svg" alt=""></a>
										<a href="#"><img src="assets/images/message-icon.svg" alt=""></a>
									</div>
								</div>
								<div class="users-content">
									It’s hard to beat a person who never gives up
								</div>
								<div class="buttons d-flex align-items-center justify-content-between">
									<a href="#" class="btn-custom">Accept</a>
									<a href="#" class="btn-custom btn-black-light">Reject</a>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="users-box">
								<div class="users-top d-flex align-items-center justify-content-between">
									<div class="left">
										<a href="#" class="d-inline-flex align-items-center">
											<img src="assets/images/client7.jpg" alt="">
											John Smith <span>(21)</span>
										</a>
									</div>
									<div class="right d-flex align-items-center">
										<a href="#"><img src="assets/images/search-announce-icon-blue.svg" alt=""></a>
										<a href="#"><img src="assets/images/add-user-icon.svg" alt=""></a>
										<a href="#"><img src="assets/images/message-icon.svg" alt=""></a>
									</div>
								</div>
								<div class="users-content">
									It’s hard to beat a person who never gives up
								</div>
								<div class="buttons d-flex align-items-center justify-content-between">
									<a href="#" class="btn-custom">Accept</a>
									<a href="#" class="btn-custom btn-black-light">Reject</a>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="users-box">
								<div class="users-top d-flex align-items-center justify-content-between">
									<div class="left">
										<a href="#" class="d-inline-flex align-items-center">
											<img src="assets/images/client7.jpg" alt="">
											John Smith <span>(21)</span>
										</a>
									</div>
									<div class="right d-flex align-items-center">
										<a href="#"><img src="assets/images/search-announce-icon-blue.svg" alt=""></a>
										<a href="#"><img src="assets/images/add-user-icon.svg" alt=""></a>
										<a href="#"><img src="assets/images/message-icon.svg" alt=""></a>
									</div>
								</div>
								<div class="users-content">
									It’s hard to beat a person who never gives up
								</div>
								<div class="buttons d-flex align-items-center justify-content-between">
									<a href="#" class="btn-custom">Accept</a>
									<a href="#" class="btn-custom btn-black-light">Reject</a>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="users-box">
								<div class="users-top d-flex align-items-center justify-content-between">
									<div class="left">
										<a href="#" class="d-inline-flex align-items-center">
											<img src="assets/images/client7.jpg" alt="">
											John Smith <span>(21)</span>
										</a>
									</div>
									<div class="right d-flex align-items-center">
										<a href="#"><img src="assets/images/search-announce-icon-blue.svg" alt=""></a>
										<a href="#"><img src="assets/images/add-user-icon.svg" alt=""></a>
										<a href="#"><img src="assets/images/message-icon.svg" alt=""></a>
									</div>
								</div>
								<div class="users-content">
									It’s hard to beat a person who never gives up
								</div>
								<div class="buttons d-flex align-items-center justify-content-between">
									<a href="#" class="btn-custom">Accept</a>
									<a href="#" class="btn-custom btn-black-light">Reject</a>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="users-box">
								<div class="users-top d-flex align-items-center justify-content-between">
									<div class="left">
										<a href="#" class="d-inline-flex align-items-center">
											<img src="assets/images/client7.jpg" alt="">
											John Smith <span>(21)</span>
										</a>
									</div>
									<div class="right d-flex align-items-center">
										<a href="#"><img src="assets/images/search-announce-icon-blue.svg" alt=""></a>
										<a href="#"><img src="assets/images/add-user-icon.svg" alt=""></a>
										<a href="#"><img src="assets/images/message-icon.svg" alt=""></a>
									</div>
								</div>
								<div class="users-content">
									It’s hard to beat a person who never gives up
								</div>
								<div class="buttons d-flex align-items-center justify-content-between">
									<a href="#" class="btn-custom">Accept</a>
									<a href="#" class="btn-custom btn-black-light">Reject</a>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="users-box">
								<div class="users-top d-flex align-items-center justify-content-between">
									<div class="left">
										<a href="#" class="d-inline-flex align-items-center">
											<img src="assets/images/client7.jpg" alt="">
											John Smith <span>(21)</span>
										</a>
									</div>
									<div class="right d-flex align-items-center">
										<a href="#"><img src="assets/images/search-announce-icon-blue.svg" alt=""></a>
										<a href="#"><img src="assets/images/add-user-icon.svg" alt=""></a>
										<a href="#"><img src="assets/images/message-icon.svg" alt=""></a>
									</div>
								</div>
								<div class="users-content">
									It’s hard to beat a person who never gives up
								</div>
								<div class="buttons d-flex align-items-center justify-content-between">
									<a href="#" class="btn-custom">Accept</a>
									<a href="#" class="btn-custom btn-black-light">Reject</a>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="users-box">
								<div class="users-top d-flex align-items-center justify-content-between">
									<div class="left">
										<a href="#" class="d-inline-flex align-items-center">
											<img src="assets/images/client7.jpg" alt="">
											John Smith <span>(21)</span>
										</a>
									</div>
									<div class="right d-flex align-items-center">
										<a href="#"><img src="assets/images/search-announce-icon-blue.svg" alt=""></a>
										<a href="#"><img src="assets/images/add-user-icon.svg" alt=""></a>
										<a href="#"><img src="assets/images/message-icon.svg" alt=""></a>
									</div>
								</div>
								<div class="users-content">
									It’s hard to beat a person who never gives up
								</div>
								<div class="buttons d-flex align-items-center justify-content-between">
									<a href="#" class="btn-custom">Accept</a>
									<a href="#" class="btn-custom btn-black-light">Reject</a>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="users-box">
								<div class="users-top d-flex align-items-center justify-content-between">
									<div class="left">
										<a href="#" class="d-inline-flex align-items-center">
											<img src="assets/images/client7.jpg" alt="">
											John Smith <span>(21)</span>
										</a>
									</div>
									<div class="right d-flex align-items-center">
										<a href="#"><img src="assets/images/search-announce-icon-blue.svg" alt=""></a>
										<a href="#"><img src="assets/images/add-user-icon.svg" alt=""></a>
										<a href="#"><img src="assets/images/message-icon.svg" alt=""></a>
									</div>
								</div>
								<div class="users-content">
									It’s hard to beat a person who never gives up
								</div>
								<div class="buttons d-flex align-items-center justify-content-between">
									<a href="#" class="btn-custom">Accept</a>
									<a href="#" class="btn-custom btn-black-light">Reject</a>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="users-box">
								<div class="users-top d-flex align-items-center justify-content-between">
									<div class="left">
										<a href="#" class="d-inline-flex align-items-center">
											<img src="assets/images/client7.jpg" alt="">
											John Smith <span>(21)</span>
										</a>
									</div>
									<div class="right d-flex align-items-center">
										<a href="#"><img src="assets/images/search-announce-icon-blue.svg" alt=""></a>
										<a href="#"><img src="assets/images/add-user-icon.svg" alt=""></a>
										<a href="#"><img src="assets/images/message-icon.svg" alt=""></a>
									</div>
								</div>
								<div class="users-content">
									It’s hard to beat a person who never gives up
								</div>
								<div class="buttons d-flex align-items-center justify-content-between">
									<a href="#" class="btn-custom">Accept</a>
									<a href="#" class="btn-custom btn-black-light">Reject</a>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="users-box">
								<div class="users-top d-flex align-items-center justify-content-between">
									<div class="left">
										<a href="#" class="d-inline-flex align-items-center">
											<img src="assets/images/client7.jpg" alt="">
											John Smith <span>(21)</span>
										</a>
									</div>
									<div class="right d-flex align-items-center">
										<a href="#"><img src="assets/images/search-announce-icon-blue.svg" alt=""></a>
										<a href="#"><img src="assets/images/add-user-icon.svg" alt=""></a>
										<a href="#"><img src="assets/images/message-icon.svg" alt=""></a>
									</div>
								</div>
								<div class="users-content">
									It’s hard to beat a person who never gives up
								</div>
								<div class="buttons d-flex align-items-center justify-content-between">
									<a href="#" class="btn-custom">Accept</a>
									<a href="#" class="btn-custom btn-black-light">Reject</a>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="users-box">
								<div class="users-top d-flex align-items-center justify-content-between">
									<div class="left">
										<a href="#" class="d-inline-flex align-items-center">
											<img src="assets/images/client7.jpg" alt="">
											John Smith <span>(21)</span>
										</a>
									</div>
									<div class="right d-flex align-items-center">
										<a href="#"><img src="assets/images/search-announce-icon-blue.svg" alt=""></a>
										<a href="#"><img src="assets/images/add-user-icon.svg" alt=""></a>
										<a href="#"><img src="assets/images/message-icon.svg" alt=""></a>
									</div>
								</div>
								<div class="users-content">
									It’s hard to beat a person who never gives up
								</div>
								<div class="buttons d-flex align-items-center justify-content-between">
									<a href="#" class="btn-custom">Accept</a>
									<a href="#" class="btn-custom btn-black-light">Reject</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="btn-close" data-dismiss="modal"><img src="assets/images/close-black-icon.svg"></div>
		</div>
	</div>
</div>

<?php include 'include/footer.php'; ?>