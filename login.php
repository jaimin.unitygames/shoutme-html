<?php include 'include/head-login-register.php';?>

<?php include 'include/header-login-register.php';?>

<section class="login-register">
	<div class="container">
		<div class="wid-335">
			<h2>Login</h2>
			<form>
				<div class="form-group">
					<label for="email">Email</label>
					<div class="input-group">
						<div class="icon">
							<img src="assets/images/email-icon.svg">
						</div>
						<input type="email" id="email" class="form-control" placeholder="john@mail.com">
					</div>
					<!-- <div class="error">This is invalid</div> -->
				</div>
				<div class="form-group">
					<label for="password">Password</label>
					<div class="input-group">
						<div class="icon">
							<img src="assets/images/password-icon.svg">
						</div>
						<input type="password" id="password" class="form-control" placeholder="•••••••••">
					</div>
					<!-- <div class="error">This is invalid</div> -->
				</div>
				<div class="form-group text-right">
					<a href="#" class="white-link">Forgot Password</a>
				</div>
				<div class="form-group">
					<button type="submit" class="btn-custom btn-black">Sign In <img src="assets/images/arrow.svg"></button>
				</div>
			</form>
			<div class="login-register__with">
				<span>You can Login with</span>
				<div class="login-register__icon">
					<a href="#"><img src="assets/images/facebook-icon.svg"></a>
					<a href="#"><img src="assets/images/google-icon.svg"></a>
				</div>
			</div>
		</div>
	</div>
	<div class="login-register__now">
		<div class="container">
			<div class="wid-335">
				<span>New Here?</span>
				<a href="register.php" class="btn-custom btn-blue">Register Now <img src="assets/images/arrow.svg"></a>
			</div>
		</div>
	</div>
</section>

<?php include 'include/footer-login-register.php';?>