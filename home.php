<?php include 'include/head.php'; ?>

<?php include 'include/header.php'; ?>

<div class="hm-feed">
	<div class="container">
		<div class="row">
			
			<div class="hm-feed__col hm-feed__col--1">
				<div class="hm-feed_left-wrap">
					<div class="hm-feed__search">
						<a href="search-result-user.php">
							<div class="form-group">
								<input type="text" name="" class="form-control" placeholder="Search Shout Me" readonly>
								<button type="submit"><img src="assets/images/search-btn-icon.svg"></button>
							</div>
						</a>
					</div>
					<div class="hm-feed__online-user">
						<div class="hm-feed__online-text">Online Now</div>
						<div class="hm-feed__create-chat"><a href="#" class="btn-custom btn-blue">Create new Conversation</a></div>
						<ul class="hm-feed__online-users">
							<li><a href="#"><img src="assets/images/client7.jpg"><span>John Smith</span></a></li>
							<li><a href="#"><img src="assets/images/client7.jpg"><span>John Smith</span></a></li>
							<li><a href="#"><img src="assets/images/client7.jpg"><span>John Smith</span></a></li>
							<li><a href="#"><img src="assets/images/client7.jpg"><span>John Smith</span></a></li>
							<li><a href="#"><img src="assets/images/client7.jpg"><span>John Smith</span></a></li>
							<li><a href="#"><img src="assets/images/client7.jpg"><span>John Smith</span></a></li>
							<li><a href="#"><img src="assets/images/client7.jpg"><span>John Smith</span></a></li>
							<li><a href="#"><img src="assets/images/client7.jpg"><span>John Smith</span></a></li>
							<li><a href="#"><img src="assets/images/client7.jpg"><span>John Smith</span></a></li>
							<li><a href="#"><img src="assets/images/client7.jpg"><span>John Smith</span></a></li>
							<li><a href="#"><img src="assets/images/client7.jpg"><span>John Smith</span></a></li>
							<li><a href="#"><img src="assets/images/client7.jpg"><span>John Smith</span></a></li>
						</ul>
					</div>
				</div>
			</div>
			
			<div class="hm-feed__col hm-feed__col--2">
				
				<div class="post-create">
					<a href="#" data-toggle="modal" data-target="#createpost" data-backdrop="static" data-keyboard="false">
						<div class="post-create__box">
							<div class="post-create__text">Post Something...</div>
							<div class="post-create__options d-flex align-items-center">
								<img src="assets/images/post-image-icon.svg">
								<img src="assets/images/post-video-icon.svg">
							</div>
						</div>
					</a>
				</div>

				<div class="modal fade createpost" id="createpost" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-body">
								<div class="createpost__select-type">
									<h3>Select Post Type</h3>
									<div class="createpost__select-type-inner d-flex align-items-center justify-content-between">
										<div class="createpost__type-text">
											<img src="assets/images/post-text-icon.svg">
											Text
										</div>
										<div class="createpost__type-image">
											<img src="assets/images/post-image-icon.svg">
											Image
										</div>
										<div class="createpost__type-video">
											<img src="assets/images/post-video-icon.svg">
											Video
										</div>
									</div>
								</div>
								<div class="createpost__after-selet">
									<div class="createpost__top d-flex align-items-center">
										<img src="assets/images/client7.jpg">
										<span>John Smith</span>
										<button type="submit" class="btn-custom btn-blue">Post</button>
									</div>
									<div class="createpost__content">
										<div class="createpost__title">An interesting title</div>
										<div class="createpost__textarea">
										<textarea class="textarea autogrow" placeholder="Your Text..."></textarea>
										</div>
										<div class="createpost__preview-area d-flex flex-wrap add_more_images">
											<div class="createpost__preview-addmore-item">
												<div class="createpost__preview-addmore-box">
													<img src="assets/images/slider-image.jpg">
													<span class="remove"><img src="assets/images/close-black-icon.svg"></span>
												</div>
											</div>
											<div class="createpost__preview-addmore-item">
												<div class="createpost__preview-addmore-box">
													<img src="assets/images/slider-image.jpg">
													<span class="remove"><img src="assets/images/close-black-icon.svg"></span>
												</div>
											</div>
											<div class="createpost__preview-addmore-item">
												<div class="createpost__preview-addmore-box">
													<img src="assets/images/slider-image.jpg">
													<span class="remove"><img src="assets/images/close-black-icon.svg"></span>
												</div>
											</div>
											<div class="createpost__preview-addmore-item">
												<div class="createpost__preview-addmore-box">
													<img src="assets/images/slider-image.jpg">
													<span class="remove"><img src="assets/images/close-black-icon.svg"></span>
												</div>
											</div>
											<div class="createpost__preview-addmore-item">
												<div class="createpost__preview-addmore-box">
													<img src="assets/images/slider-image.jpg">
													<span class="remove"><img src="assets/images/close-black-icon.svg"></span>
												</div>
											</div>
											<div class="createpost__preview-addmore-item">
												<div class="createpost__preview-addmore-box">
													<video>
														<source src="https://www.w3schools.com/tags/movie.mp4" type="video/mp4">
														Your browser does not support the video tag.
													</video>
													<span class="remove"><img src="assets/images/close-black-icon.svg"></span>
												</div>
											</div>
											<div class="createpost__preview-addmore-item">
												<div class="createpost__preview-addmore-box">
													<img src="assets/images/slider-image.jpg">
													<span class="remove"><img src="assets/images/close-black-icon.svg"></span>
												</div>
											</div>
											<div class="createpost__preview-addmore-item">
												<div class="createpost__preview-addmore-box">
													<label for="createpost-preview-addmore"><img src="assets/images/createpost-addmore-icon.svg" title="Select Images"></label>
													<input type="file" name="" multiple class="form-control" id="createpost-preview-addmore">
												</div>
											</div>
										</div>
										<div class="createpost__type d-flex align-items-center">
											<div class="image">
												<label for="createpost-images"><img src="assets/images/post-image-icon.svg" title="Select Images"></label>
												<input type="file" name="" multiple class="form-control" id="createpost-images">
											</div>
											<div class="video">
												<label for="createpost-videos"><img src="assets/images/post-video-icon.svg" title="Select Videos"></label>
												<input type="file" name="" multiple class="form-control" id="createpost-videos">
											</div>
										</div>
									</div>
									<div class="createpost__tagpeople">
										<div class="createpost__search-box d-flex flex-wrap">
											<div class="createpost__search">
												<div class="form-group">
													<input type="text" name="" class="form-control" placeholder="Search People to Tag">
													<button type="submit"><img src="assets/images/search-blue-icon.svg"></button>
													<div class="createpost__search-result">
														<div class="createpost__search-user"><img src="assets/images/client7.jpg">John Smith</div>
														<div class="createpost__search-user"><img src="assets/images/client7.jpg">John Smith</div>
														<div class="createpost__search-user"><img src="assets/images/client7.jpg">John Smith</div>
														<div class="createpost__search-user"><img src="assets/images/client7.jpg">John Smith</div>
														<div class="createpost__search-user"><img src="assets/images/client7.jpg">John Smith</div>
														<div class="createpost__search-user"><img src="assets/images/client7.jpg">John Smith</div>
														<div class="createpost__search-user"><img src="assets/images/client7.jpg">John Smith</div>
														<div class="createpost__search-user"><img src="assets/images/client7.jpg">John Smith</div>
														<div class="createpost__search-user"><img src="assets/images/client7.jpg">John Smith</div>
														<div class="createpost__search-user"><img src="assets/images/client7.jpg">John Smith</div>
													</div>
												</div>
											</div>
											<div class="createpost__taglist d-flex flex-wrap">
												<div class="createpost__taguser">
													<img src="assets/images/client7.jpg">
													<span class="remove"><img src="assets/images/close-black-icon.svg"></span>
												</div>
												<div class="createpost__taguser">
													<img src="assets/images/client7.jpg">
													<span class="remove"><img src="assets/images/close-black-icon.svg"></span>
												</div>
												<div class="createpost__taguser">
													<img src="assets/images/client7.jpg">
													<span class="remove"><img src="assets/images/close-black-icon.svg"></span>
												</div>
												<div class="createpost__taguser">
													<img src="assets/images/client7.jpg">
													<span class="remove"><img src="assets/images/close-black-icon.svg"></span>
												</div>
												<div class="createpost__taguser">
													<img src="assets/images/client7.jpg">
													<span class="remove"><img src="assets/images/close-black-icon.svg"></span>
												</div>
												<div class="createpost__taguser">
													<img src="assets/images/client7.jpg">
													<span class="remove"><img src="assets/images/close-black-icon.svg"></span>
												</div>
												<div class="createpost__taguser">
													<img src="assets/images/client7.jpg">
													<span class="remove"><img src="assets/images/close-black-icon.svg"></span>
												</div>
												<div class="createpost__taguser">
													<img src="assets/images/client7.jpg">
													<span class="remove"><img src="assets/images/close-black-icon.svg"></span>
												</div>
												<div class="createpost__taguser">
													<img src="assets/images/client7.jpg">
													<span class="remove"><img src="assets/images/close-black-icon.svg"></span>
												</div>
												<div class="createpost__taguser">
													<img src="assets/images/client7.jpg">
													<span class="remove"><img src="assets/images/close-black-icon.svg"></span>
												</div>
												<div class="createpost__taguser">
													<img src="assets/images/client7.jpg">
													<span class="remove"><img src="assets/images/close-black-icon.svg"></span>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="loader">
									<img src="assets/images/loader.svg">
								</div>
							</div>
							<div class="btn-close" data-dismiss="modal"><img src="assets/images/close-black-icon.svg"></div>
						</div>
					</div>
				</div>
				
				<div class="feed-filter">
					<select class="form-control multiselect" multiple="multiple" style="display: none;">
						<option value="following" selected>Following</option>
						<option value="trending">Trending</option>
						<option value="nearby">Nearby</option>
					</select>
				</div>
				
				<div class="feed-post-listing">

					<div class="post-loader">
						<img src="assets/images/white-border-loader.svg">
					</div>

					<!-- <div class="white-text-msg text-center py-5 px-2">
						<div class="text">No Content here. Please check back later</div>
					</div> -->
					
					<div class="feed-post">
						<div class="feed-post__top d-flex align-items-center justify-content-between">
							<div class="feed-post__top-left d-flex align-items-center">
								<a href="#"><img src="assets/images/client7.jpg" class="feed-post__admin-image"></a>
								<div class="feed-post__admin-detail">
									<a href="#"><div class="feed-post__admin-name">John Smith <img src="assets/images/announcement-icon-blue.svg"></div></a>
									<div class="feed-post__date">12:00, 12th Nov 2020</div>
								</div>
							</div>
							<div class="feed-post__top-right">
								<div class="feed-post__option">
									<i class="far fa-bookmark"></i>
								</div>
								<div class="feed-post__report">
									<i class="fas fa-ellipsis-h"></i>
									<div class="feed-post__report-dropdown" data-toggle="modal" data-target="#reportpost">Report Post</div>
								</div>
							</div>
						</div>
						<div class="feed-post__bottom">
							<div class="feed-post__content">Game Night Today!!</div>
							<div class="feed-post__action d-flex align-items-center justify-content-between">
								<div class="feed-post__action-left d-flex align-items-center">
									<div class="like">
										<div class="icon">
											<svg class="before" xmlns="http://www.w3.org/2000/svg" width="21.015" height="20.195" viewBox="0 0 21.015 20.195">
												<defs><style>.likebeforea{fill:none;stroke:#1461af;}</style></defs><g transform="translate(0.5 -9.923)"><g transform="translate(0 16.559)"><path class="likebeforea" d="M3.265,167.256H.653a.654.654,0,0,0-.653.653v11.753a.653.653,0,0,0,.653.653H3.265a.653.653,0,0,0,.653-.653V167.909A.654.654,0,0,0,3.265,167.256Z" transform="translate(0 -167.256)"/></g><g transform="translate(5.224 10.433)"><g transform="translate(0 0)"><path class="likebeforea" d="M146.872,16.559c-.105-.018-5.029,0-5.029,0l.688-1.876c.475-1.3.167-3.274-1.149-3.978a2.257,2.257,0,0,0-1.511-.223,1.089,1.089,0,0,0-.665.5,2.712,2.712,0,0,0-.209.926,4,4,0,0,1-1.108,2.117c-1.014.989-4.163,3.841-4.163,3.841V28.312h10.883a1.977,1.977,0,0,0,1.707-2.92,1.977,1.977,0,0,0,.653-2.612,1.977,1.977,0,0,0,.653-2.612A1.992,1.992,0,0,0,146.872,16.559Z" transform="translate(-133.726 -10.433)"/></g></g></g>
											</svg>
											<svg class="after" xmlns="http://www.w3.org/2000/svg" width="20" height="19.185" viewBox="0 0 20 19.185">
												<defs><style>.likeaftera{fill:#1461af;}</style></defs><g transform="translate(0 -10.433)"><g transform="translate(0 16.559)"><path class="likeaftera" d="M3.265,167.256H.653a.654.654,0,0,0-.653.653v11.753a.653.653,0,0,0,.653.653H3.265a.653.653,0,0,0,.653-.653V167.909A.654.654,0,0,0,3.265,167.256Z" transform="translate(0 -167.256)"/></g><g transform="translate(5.224 10.433)"><g transform="translate(0 0)"><path class="likeaftera" d="M146.872,16.559c-.105-.018-5.029,0-5.029,0l.688-1.876c.475-1.3.167-3.274-1.149-3.978a2.257,2.257,0,0,0-1.511-.223,1.089,1.089,0,0,0-.665.5,2.712,2.712,0,0,0-.209.926,4,4,0,0,1-1.108,2.117c-1.014.989-4.163,3.841-4.163,3.841V28.312h10.883a1.977,1.977,0,0,0,1.707-2.92,1.977,1.977,0,0,0,.653-2.612,1.977,1.977,0,0,0,.653-2.612A1.992,1.992,0,0,0,146.872,16.559Z" transform="translate(-133.726 -10.433)"/></g></g></g>
											</svg>
										</div> 120
									</div>
									<div class="heart">
										<div class="icon">
											<svg class="before" xmlns="http://www.w3.org/2000/svg" width="22" height="20.814" viewBox="0 0 22 20.814">
												<defs><style>.heartbeforea{fill:none;}.heartbeforeb,.heartbeforec{stroke:none;}.heartbeforec{fill:#ffb14b;}</style></defs><g transform="translate(82 22.6)"><g transform="translate(-82 -43)"><g class="heartbeforea"><path class="heartbeforeb" d="M11,39.8,9.6,38.4C4.4,33.8,1,30.7,1,26.9a5.422,5.422,0,0,1,5.5-5.5A6.01,6.01,0,0,1,11,23.5a6.006,6.006,0,0,1,4.5-2.1A5.422,5.422,0,0,1,21,26.9c0,3.8-3.4,6.9-8.6,11.5Z"/><path class="heartbeforec" d="M 11.00000095367432 39.79999923706055 L 12.40000057220459 38.39999771118164 C 17.60000038146973 33.79999923706055 21 30.69999885559082 21 26.89999771118164 C 21 23.79999923706055 18.60000038146973 21.39999771118164 15.50000095367432 21.39999771118164 C 13.80000114440918 21.39999771118164 12.09804058074951 22.19999885559082 11.00000095367432 23.49999809265137 C 9.90000057220459 22.19999885559082 8.200000762939453 21.39999771118164 6.500000953674316 21.39999771118164 C 3.400001049041748 21.39999771118164 1.000000953674316 23.79999923706055 1.000000953674316 26.89999771118164 C 1.000000953674316 30.69999885559082 4.400001049041748 33.79999923706055 9.600001335144043 38.39999771118164 L 11.00000095367432 39.79999923706055 M 11.00000095367432 41.21420669555664 L 8.914455413818359 39.1286735534668 C 6.150452613830566 36.68337631225586 4.077080726623535 34.80950164794922 2.577461004257202 32.98497009277344 C 0.8190109729766846 30.84550857543945 9.5367431640625e-07 28.91196823120117 9.5367431640625e-07 26.89999771118164 C 9.5367431640625e-07 23.2551383972168 2.855140924453735 20.39999771118164 6.500000953674316 20.39999771118164 C 8.13609790802002 20.39999771118164 9.753412246704102 21.01666069030762 10.9995641708374 22.08572006225586 C 12.24466991424561 21.01666641235352 13.86224555969238 20.39999771118164 15.50000095367432 20.39999771118164 C 19.14486122131348 20.39999771118164 22 23.2551383972168 22 26.89999771118164 C 22 28.91196823120117 21.18099021911621 30.84550857543945 19.42254066467285 32.98497009277344 C 17.92292022705078 34.80950164794922 15.84954929351807 36.68337631225586 13.08554649353027 39.1286735534668 L 11.00000095367432 41.21420669555664 Z"/></g></g></g>
											</svg>
											<svg class="after" xmlns="http://www.w3.org/2000/svg" width="22" height="20.814" viewBox="0 0 22 20.814">
												<defs><style>.heartaftera{fill:#ffb14b;}.heartafterb,.heartafterc{stroke:none;}.heartafterc{fill:#ffb14b;}</style></defs><g transform="translate(82 22.6)"><g transform="translate(-82 -43)"><g class="heartaftera"><path class="heartafterb" d="M11,39.8,9.6,38.4C4.4,33.8,1,30.7,1,26.9a5.422,5.422,0,0,1,5.5-5.5A6.01,6.01,0,0,1,11,23.5a6.006,6.006,0,0,1,4.5-2.1A5.422,5.422,0,0,1,21,26.9c0,3.8-3.4,6.9-8.6,11.5Z"/><path class="heartafterc" d="M 11.00000095367432 39.79999923706055 L 12.40000057220459 38.39999771118164 C 17.60000038146973 33.79999923706055 21 30.69999885559082 21 26.89999771118164 C 21 23.79999923706055 18.60000038146973 21.39999771118164 15.50000095367432 21.39999771118164 C 13.80000114440918 21.39999771118164 12.09804058074951 22.19999885559082 11.00000095367432 23.49999809265137 C 9.90000057220459 22.19999885559082 8.200000762939453 21.39999771118164 6.500000953674316 21.39999771118164 C 3.400001049041748 21.39999771118164 1.000000953674316 23.79999923706055 1.000000953674316 26.89999771118164 C 1.000000953674316 30.69999885559082 4.400001049041748 33.79999923706055 9.600001335144043 38.39999771118164 L 11.00000095367432 39.79999923706055 M 11.00000095367432 41.21420669555664 L 8.914455413818359 39.1286735534668 C 6.150452613830566 36.68337631225586 4.077080726623535 34.80950164794922 2.577461004257202 32.98497009277344 C 0.8190109729766846 30.84550857543945 9.5367431640625e-07 28.91196823120117 9.5367431640625e-07 26.89999771118164 C 9.5367431640625e-07 23.2551383972168 2.855140924453735 20.39999771118164 6.500000953674316 20.39999771118164 C 8.13609790802002 20.39999771118164 9.753412246704102 21.01666069030762 10.9995641708374 22.08572006225586 C 12.24466991424561 21.01666641235352 13.86224555969238 20.39999771118164 15.50000095367432 20.39999771118164 C 19.14486122131348 20.39999771118164 22 23.2551383972168 22 26.89999771118164 C 22 28.91196823120117 21.18099021911621 30.84550857543945 19.42254066467285 32.98497009277344 C 17.92292022705078 34.80950164794922 15.84954929351807 36.68337631225586 13.08554649353027 39.1286735534668 L 11.00000095367432 41.21420669555664 Z"/></g></g></g>
											</svg>
										</div> 8
									</div>
									<div class="comment" data-toggle="modal" data-target="#postcomment">
										<div class="icon">
											<svg class="before" xmlns="http://www.w3.org/2000/svg" width="22" height="21.522" viewBox="0 0 22 21.522">
												<defs><style>.beforecommenta{fill:none;}.beforecommentb,.beforecommentc{stroke:none;}.beforecommentc{fill:#a8bfd5;}</style></defs><g transform="translate(1 1)"><g class="beforecommenta"><path class="beforecommentb" d="M10,0C4.458,0,0,3.929,0,8.766a8.47,8.47,0,0,0,3.708,6.851l.156,3.277c.029.611.45.81.938.446l2.7-2.01a10.115,10.115,0,0,0,2.5.252c5.542,0,10-3.929,10-8.766S15.542,0,10,0Z"/><path class="beforecommentc" d="M 10 -3.814697265625e-06 C 4.458330154418945 -3.814697265625e-06 0 3.929465293884277 0 8.765745162963867 C 0 11.53655529022217 1.458330154418945 14.00499534606934 3.708330154418945 15.61711502075195 L 3.864099502563477 18.89400482177734 C 3.89312744140625 19.50488471984863 4.31382942199707 19.70411491394043 4.802430152893066 19.33999633789062 L 7.5 17.33000564575195 C 8.291669845581055 17.53144454956055 9.125 17.58188629150391 10 17.58188629150391 C 15.54166984558105 17.58188629150391 20 13.65244483947754 20 8.816125869750977 C 20 3.979846000671387 15.54166984558105 -3.814697265625e-06 10 -3.814697265625e-06 M 10 -1.000003814697266 C 16.06542015075684 -1.000003814697266 21 3.403495788574219 21 8.816125869750977 C 21 11.4495153427124 19.84151077270508 13.91700553894043 17.73793029785156 15.76406574249268 C 15.66847038269043 17.5811653137207 12.92041969299316 18.58188629150391 10 18.58188629150391 C 9.361814498901367 18.58188629150391 8.555877685546875 18.55931854248047 7.731721878051758 18.40442276000977 L 5.399920463562012 20.14187622070312 C 5.066249847412109 20.39053535461426 4.700409889221191 20.52199554443359 4.342020034790039 20.52199554443359 C 3.666929244995117 20.52199554443359 2.916830062866211 20.02739524841309 2.865230560302734 18.94147491455078 L 2.731781005859375 16.13415908813477 C 0.3536052703857422 14.26455116271973 -1 11.60455799102783 -1 8.765745162963867 C -1 6.132375717163086 0.1584892272949219 3.664886474609375 2.262069702148438 1.817825317382812 C 4.331540107727051 0.00072479248046875 7.07958984375 -1.000003814697266 10 -1.000003814697266 Z"/></g></g>
											</svg>
											<svg class="after" xmlns="http://www.w3.org/2000/svg" width="22" height="21.522" viewBox="0 0 22 21.522">
												<defs><style>.aftercommenta{fill:#a8bfd5;}.aftercommentb,.aftercommentc{stroke:none;}.aftercommentc{fill:#a8bfd5;}</style></defs><g transform="translate(1 1)"><g class="aftercommenta"><path class="aftercommentb" d="M10,0C4.458,0,0,3.929,0,8.766a8.47,8.47,0,0,0,3.708,6.851l.156,3.277c.029.611.45.81.938.446l2.7-2.01a10.115,10.115,0,0,0,2.5.252c5.542,0,10-3.929,10-8.766S15.542,0,10,0Z"/><path class="aftercommentc" d="M 10 -3.814697265625e-06 C 4.458330154418945 -3.814697265625e-06 0 3.929465293884277 0 8.765745162963867 C 0 11.53655529022217 1.458330154418945 14.00499534606934 3.708330154418945 15.61711502075195 L 3.864099502563477 18.89400482177734 C 3.89312744140625 19.50488471984863 4.31382942199707 19.70411491394043 4.802430152893066 19.33999633789062 L 7.5 17.33000564575195 C 8.291669845581055 17.53144454956055 9.125 17.58188629150391 10 17.58188629150391 C 15.54166984558105 17.58188629150391 20 13.65244483947754 20 8.816125869750977 C 20 3.979846000671387 15.54166984558105 -3.814697265625e-06 10 -3.814697265625e-06 M 10 -1.000003814697266 C 16.06542015075684 -1.000003814697266 21 3.403495788574219 21 8.816125869750977 C 21 11.4495153427124 19.84151077270508 13.91700553894043 17.73793029785156 15.76406574249268 C 15.66847038269043 17.5811653137207 12.92041969299316 18.58188629150391 10 18.58188629150391 C 9.361814498901367 18.58188629150391 8.555877685546875 18.55931854248047 7.731721878051758 18.40442276000977 L 5.399920463562012 20.14187622070312 C 5.066249847412109 20.39053535461426 4.700409889221191 20.52199554443359 4.342020034790039 20.52199554443359 C 3.666929244995117 20.52199554443359 2.916830062866211 20.02739524841309 2.865230560302734 18.94147491455078 L 2.731781005859375 16.13415908813477 C 0.3536052703857422 14.26455116271973 -1 11.60455799102783 -1 8.765745162963867 C -1 6.132375717163086 0.1584892272949219 3.664886474609375 2.262069702148438 1.817825317382812 C 4.331540107727051 0.00072479248046875 7.07958984375 -1.000003814697266 10 -1.000003814697266 Z"/></g></g>
											</svg>
										</div> 28
									</div>
								</div>
								<div class="feed-post__action-right">
									<div class="share">
										<i class="fas fa-share"></i>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="feed-post">
						<div class="feed-post__top d-flex align-items-center justify-content-between">
							<div class="feed-post__top-left d-flex align-items-center">
								<a href="#"><img src="assets/images/client7.jpg" class="feed-post__admin-image"></a>
								<div class="feed-post__admin-detail">
									<a href="#"><div class="feed-post__admin-name">John Smith <img src="assets/images/announcement-icon-blue.svg"></div></a>
									<div class="feed-post__date">12:00, 12th Nov 2020</div>
								</div>
							</div>
							<div class="feed-post__top-right">
								<div class="feed-post__option">
									<i class="far fa-bookmark"></i>
								</div>
								<div class="feed-post__report">
									<i class="fas fa-ellipsis-h"></i>
									<div class="feed-post__report-dropdown" data-toggle="modal" data-target="#reportpost">Report Post</div>
								</div>
							</div>
						</div>
						<div class="feed-post__bottom">
							<div class="feed-post__content">
								Loved the Match Today!!! Congratulations to the Homeland Team
								<div class="feed-post__tags"><strong>with</strong> with John Smith and 2 Others</div>
								<div class="feed-post__image-video">
									<div class="swiper-container">
										<div class="swiper-wrapper">
											<div class="swiper-slide">
												<img src="assets/images/client7.jpg">
											</div>
										</div>
										<div class="swiper-pagination"></div>
										<div class="swiper-overlay"></div>
									</div>
								</div>
							</div>
							<div class="feed-post__action d-flex align-items-center justify-content-between">
								<div class="feed-post__action-left d-flex align-items-center">
									<div class="like">
										<i class="far fa-thumbs-up"></i> 120
									</div>
									<div class="heart">
										<i class="far fa-heart"></i> 8
									</div>
									<div class="comment" data-toggle="modal" data-target="#postcomment">
										<i class="far fa-comment"></i> 28
									</div>
								</div>
								<div class="feed-post__action-right">
									<div class="share">
										<i class="fas fa-share"></i>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="feed-post">
						<div class="feed-post__top d-flex align-items-center justify-content-between">
							<div class="feed-post__top-left d-flex align-items-center">
								<a href="#"><img src="assets/images/client7.jpg" class="feed-post__admin-image"></a>
								<div class="feed-post__admin-detail">
									<a href="#"><div class="feed-post__admin-name">John Smith <img src="assets/images/announcement-icon-blue.svg"></div></a>
									<div class="feed-post__date">12:00, 12th Nov 2020</div>
								</div>
							</div>
							<div class="feed-post__top-right">
								<div class="feed-post__option">
									<i class="far fa-bookmark"></i>
								</div>
								<div class="feed-post__report">
									<i class="fas fa-ellipsis-h"></i>
									<div class="feed-post__report-dropdown" data-toggle="modal" data-target="#reportpost">Report Post</div>
								</div>
							</div>
						</div>
						<div class="feed-post__bottom">
							<div class="feed-post__content">
								Loved the Match Today!!! Congratulations to the Homeland Team
								<div class="feed-post__tags"><strong>with</strong> with John Smith and 2 Others</div>
								<div class="feed-post__image-video">
									<div class="video-wrap">
										<video controls>
											<source src="https://www.w3schools.com/tags/movie.mp4" type="video/mp4">
											Your browser does not support the video tag.
										</video>
									</div>
								</div>
							</div>
							<div class="feed-post__action d-flex align-items-center justify-content-between">
								<div class="feed-post__action-left d-flex align-items-center">
									<div class="like">
										<i class="far fa-thumbs-up"></i> 120
									</div>
									<div class="heart">
										<i class="far fa-heart"></i> 8
									</div>
									<div class="comment" data-toggle="modal" data-target="#postcomment">
										<i class="far fa-comment"></i> 28
									</div>
								</div>
								<div class="feed-post__action-right">
									<div class="share">
										<i class="fas fa-share"></i>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="feed-post">
						<div class="feed-post__top d-flex align-items-center justify-content-between">
							<div class="feed-post__top-left d-flex align-items-center">
								<a href="#"><img src="assets/images/client7.jpg" class="feed-post__admin-image"></a>
								<div class="feed-post__admin-detail">
									<a href="#"><div class="feed-post__admin-name">John Smith <img src="assets/images/announcement-icon-blue.svg"></div></a>
									<div class="feed-post__date">12:00, 12th Nov 2020</div>
								</div>
							</div>
							<div class="feed-post__top-right">
								<div class="feed-post__option">
									<i class="far fa-bookmark"></i>
								</div>
								<div class="feed-post__report">
									<i class="fas fa-ellipsis-h"></i>
									<div class="feed-post__report-dropdown" data-toggle="modal" data-target="#reportpost">Report Post</div>
								</div>
							</div>
						</div>
						<div class="feed-post__bottom">
							<div class="feed-post__content">
								Loved the Match Today!!! Congratulations to the Homeland Team
								<div class="feed-post__tags"><strong>with</strong> with John Smith and 2 Others</div>
								<div class="feed-post__image-video">
									<div class="swiper-container">
										<div class="swiper-wrapper">
											<div class="swiper-slide">
												<img src="assets/images/client7.jpg">
											</div>
											<div class="swiper-slide">
												<img src="assets/images/client9.jpg">
											</div>
											<div class="swiper-slide">
												<img src="assets/images/client11.jpg">
											</div>
											<div class="swiper-slide">
												<img src="assets/images/client13.jpg">
											</div>
											<div class="swiper-slide">
												<img src="assets/images/client5.jpg">
											</div>
										</div>
										<div class="swiper-pagination"></div>
										<div class="swiper-overlay"></div>
									</div>
								</div>
							</div>
							<div class="feed-post__action d-flex align-items-center justify-content-between">
								<div class="feed-post__action-left d-flex align-items-center">
									<div class="like">
										<i class="far fa-thumbs-up"></i> 120
									</div>
									<div class="heart">
										<i class="far fa-heart"></i> 8
									</div>
									<div class="comment" data-toggle="modal" data-target="#postcomment">
										<i class="far fa-comment"></i> 28
									</div>
								</div>
								<div class="feed-post__action-right">
									<div class="share">
										<i class="fas fa-share"></i>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="feed-post">
						<div class="feed-post__top d-flex align-items-center justify-content-between">
							<div class="feed-post__top-left d-flex align-items-center">
								<a href="#"><img src="assets/images/client7.jpg" class="feed-post__admin-image"></a>
								<div class="feed-post__admin-detail">
									<a href="#"><div class="feed-post__admin-name">John Smith <img src="assets/images/announcement-icon-blue.svg"></div></a>
									<div class="feed-post__date">12:00, 12th Nov 2020</div>
								</div>
							</div>
							<div class="feed-post__top-right">
								<div class="feed-post__option">
									<i class="far fa-bookmark"></i>
								</div>
								<div class="feed-post__report">
									<i class="fas fa-ellipsis-h"></i>
									<div class="feed-post__report-dropdown" data-toggle="modal" data-target="#reportpost">Report Post</div>
								</div>
							</div>
						</div>
						<div class="feed-post__bottom">
							<div class="feed-post__content">
								Loved the Match Today!!! Congratulations to the Homeland Team
								<div class="feed-post__tags"><strong>with</strong> with John Smith and 2 Others</div>
								<div class="feed-post__image-video">
									<div class="video-wrap">
										<video controls>
											<source src="https://www.w3schools.com/tags/movie.mp4" type="video/mp4">
											Your browser does not support the video tag.
										</video>
									</div>
								</div>
							</div>
							<div class="feed-post__action d-flex align-items-center justify-content-between">
								<div class="feed-post__action-left d-flex align-items-center">
									<div class="like">
										<i class="far fa-thumbs-up"></i> 120
									</div>
									<div class="heart">
										<i class="far fa-heart"></i> 8
									</div>
									<div class="comment" data-toggle="modal" data-target="#postcomment">
										<i class="far fa-comment"></i> 28
									</div>
								</div>
								<div class="feed-post__action-right">
									<div class="share">
										<i class="fas fa-share"></i>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="feed-post">
						<div class="feed-post__top d-flex align-items-center justify-content-between">
							<div class="feed-post__top-left d-flex align-items-center">
								<a href="#"><img src="assets/images/client7.jpg" class="feed-post__admin-image"></a>
								<div class="feed-post__admin-detail">
									<a href="#"><div class="feed-post__admin-name">John Smith <img src="assets/images/announcement-icon-blue.svg"></div></a>
									<div class="feed-post__date">12:00, 12th Nov 2020</div>
								</div>
							</div>
							<div class="feed-post__top-right">
								<div class="feed-post__option">
									<i class="far fa-bookmark"></i>
								</div>
								<div class="feed-post__report">
									<i class="fas fa-ellipsis-h"></i>
									<div class="feed-post__report-dropdown" data-toggle="modal" data-target="#reportpost">Report Post</div>
								</div>
							</div>
						</div>
						<div class="feed-post__bottom">
							<div class="feed-post__content">Game Night Today!!</div>
							<div class="feed-post__action d-flex align-items-center justify-content-between">
								<div class="feed-post__action-left d-flex align-items-center">
									<div class="like">
										<i class="far fa-thumbs-up"></i> 120
									</div>
									<div class="heart">
										<i class="far fa-heart"></i> 8
									</div>
									<div class="comment" data-toggle="modal" data-target="#postcomment">
										<i class="far fa-comment"></i> 28
									</div>
								</div>
								<div class="feed-post__action-right">
									<div class="share">
										<i class="fas fa-share"></i>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>

			</div>
			
		</div>
	</div>
</div>

<div class="modal fade reportpost" id="reportpost" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-body pb-0">
				<div class="modal-title">
					<h3>Select Report Post Issue</h3>
				</div>
				<div class="reportpost__type">
					<ul>
						<li><a href="#">Spam Post</a></li>
						<li><a href="#">Represents Violence</a></li>
						<li><a href="#">Obscene Content</a></li>
						<li><a href="#">Racist Post</a></li>
					</ul>
				</div>
			</div>
			<div class="btn-close" data-dismiss="modal"><img src="assets/images/close-black-icon.svg"></div>
		</div>
	</div>
</div>

<div class="modal fade postcomment" id="postcomment" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<div class="modal-title">
					<h3>Comments</h3>
				</div>
				<div class="postcomment__main">
					<div class="postcomment__list">
						
						<!-- Post Comment Repeater -->
						<div class="postcomment__item">
							<div class="postcomment__box">
								<div class="postcomment__top d-flex align-items-center justify-content-between">
									<div class="left">
										<a href="#" class="d-inline-flex align-items-center">
											<img src="assets/images/client14.jpg">
											William Rose
										</a>
									</div>
									<div class="right d-inline-flex align-items-center">
										<div class="replay">
											<a href="#">Reply <i class="fas fa-share"></i></a>
										</div>
										<div class="option">
											<img src="assets/images/dots-icon.svg">
											<div class="feed-post__comment-dropdown" data-toggle="modal" data-target="#reportcomment">Report Comment</div>
										</div>
									</div>
								</div>
								<div class="postcomment__text">All the best boys</div>
							</div>
						</div>
						
						<div class="postcomment__item">
							<div class="postcomment__box">
								<div class="postcomment__top d-flex align-items-center justify-content-between">
									<div class="left">
										<a href="#" class="d-inline-flex align-items-center">
											<img src="assets/images/client11.jpg">
											William Jones
										</a>
									</div>
									<div class="right d-inline-flex align-items-center">
										<div class="replay">
											<a href="#">Reply <i class="fas fa-share"></i></a>
										</div>
										<div class="option">
											<img src="assets/images/dots-icon.svg">
										</div>
									</div>
								</div>
								<div class="postcomment__text">We all know whos gonna win...</div>
							</div>
							<!-- Post Comment Sub Main -->
							<div class="postcomment__level2">
								<!-- Post Comment Sub Repeater -->
								<div class="postcomment__box">
									<div class="postcomment__top d-flex align-items-center justify-content-between">
										<div class="left">
											<a href="#" class="d-inline-flex align-items-center">
												<img src="assets/images/client2.jpg">
												Steve Smith
											</a>
										</div>
										<div class="right d-inline-flex align-items-center">
											<div class="replay">
												<a href="#">Reply <i class="fas fa-share"></i></a>
											</div>
											<div class="option">
												<img src="assets/images/dots-icon.svg">
											</div>
										</div>
									</div>
									<div class="postcomment__text"><span class="tag">William Jones</span> Its too obvious bro...</div>
								</div>
								<div class="postcomment__box">
									<div class="postcomment__top d-flex align-items-center justify-content-between">
										<div class="left">
											<a href="#" class="d-inline-flex align-items-center">
												<img src="assets/images/client11.jpg">
												William Jones
											</a>
										</div>
										<div class="right d-inline-flex align-items-center">
											<div class="replay">
												<a href="#">Reply <i class="fas fa-share"></i></a>
											</div>
											<div class="option">
												<img src="assets/images/dots-icon.svg">
											</div>
										</div>
									</div>
									<div class="postcomment__text"><span class="tag">Steve Smith</span> I know right! Game is on...</div>
								</div>
								<!-- Load More Replays -->
								<div class="loadmore-replays">
									<a href="#">Show more (25)</a>
								</div>
							</div>
						</div>

						<div class="postcomment__item">
							<div class="postcomment__box">
								<div class="postcomment__top d-flex align-items-center justify-content-between">
									<div class="left">
										<a href="#" class="d-inline-flex align-items-center">
											<img src="assets/images/client11.jpg">
											William Jones
										</a>
									</div>
									<div class="right d-inline-flex align-items-center">
										<div class="replay">
											<a href="#">Reply <i class="fas fa-share"></i></a>
										</div>
										<div class="option">
											<img src="assets/images/dots-icon.svg">
										</div>
									</div>
								</div>
								<div class="postcomment__text">We all know whos gonna win...</div>
							</div>
							<!-- Post Comment Sub Main -->
							<div class="postcomment__level2">
								<!-- Post Comment Sub Repeater -->
								<div class="postcomment__box">
									<div class="postcomment__top d-flex align-items-center justify-content-between">
										<div class="left">
											<a href="#" class="d-inline-flex align-items-center">
												<img src="assets/images/client2.jpg">
												Steve Smith
											</a>
										</div>
										<div class="right d-inline-flex align-items-center">
											<div class="replay">
												<a href="#">Reply <i class="fas fa-share"></i></a>
											</div>
											<div class="option">
												<img src="assets/images/dots-icon.svg">
											</div>
										</div>
									</div>
									<div class="postcomment__text"><span class="tag">William Jones</span> Its too obvious bro...</div>
								</div>									
								<div class="postcomment__box">
									<div class="postcomment__top d-flex align-items-center justify-content-between">
										<div class="left">
											<a href="#" class="d-inline-flex align-items-center">
												<img src="assets/images/client11.jpg">
												William Jones
											</a>
										</div>
										<div class="right d-inline-flex align-items-center">
											<div class="replay">
												<a href="#">Reply <i class="fas fa-share"></i></a>
											</div>
											<div class="option">
												<img src="assets/images/dots-icon.svg">
											</div>
										</div>
									</div>
									<div class="postcomment__text"><span class="tag">Steve Smith</span> I know right! Game is on...</div>
								</div>
								<!-- Load More Replays -->
								<div class="loadmore-replays">
									<a href="#">Show more (25)</a>
								</div>
							</div>
						</div>

						<div class="postcomment__item">
							<div class="postcomment__box">
								<div class="postcomment__top d-flex align-items-center justify-content-between">
									<div class="left">
										<a href="#" class="d-inline-flex align-items-center">
											<img src="assets/images/client10.jpg">
											Micheal Martin
										</a>
									</div>
									<div class="right d-inline-flex align-items-center">
										<div class="replay">
											<a href="#">Reply <i class="fas fa-share"></i></a>
										</div>
										<div class="option">
											<img src="assets/images/dots-icon.svg">
										</div>
									</div>
								</div>
								<div class="postcomment__text">All the best boys</div>
							</div>
						</div>

						<!-- Load More Comments -->
						<div class="loadmore-comments">
							<a href="#"><img src="assets/images/down-blue-rounded-icon.svg"></a>
						</div>

					</div>
				</div>

				<div class="postcomment__write-main">
					
					<div class="postcomment__write-top">
						<div class="postcomment__replay-label d-flex align-items-center justify-content-between">
							<div class="left"><span>×</span> Replying to</div>
							<div class="right">
								<div class="postcomment__replay-user">
									<div class="postcomment__replay-box d-flex align-items-center">
										<img src="assets/images/client14.jpg">William Rose
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="postcomment__write-box">
						<div class="postcomment__textarea">
							<textarea class="textarea autogrow2" placeholder="Write Comment"></textarea>
							<button type="submit"><img src="assets/images/submit-rounded-icon.svg"></button>
						</div>
					</div>

				</div>

			</div>
			<div class="btn-close" data-dismiss="modal"><img src="assets/images/close-black-icon.svg"></div>
		</div>
	</div>
</div>

<div class="modal fade reportpost" id="reportcomment" data-backdrop="static" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-body pb-0">
				<div class="modal-title">
					<h3>Select Report Comment Issue</h3>
				</div>
				<div class="reportpost__type">
					<ul>
						<li><a href="#">Spam Post</a></li>
						<li><a href="#">Represents Violence</a></li>
						<li><a href="#">Obscene Content</a></li>
						<li><a href="#">Racist Post</a></li>
					</ul>
				</div>
			</div>
			<div class="btn-close" data-dismiss="modal"><img src="assets/images/close-black-icon.svg"></div>
		</div>
	</div>
</div>

<?php include 'include/footer.php'; ?>