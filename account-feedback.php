<?php include 'include/head.php'; ?>

<?php include 'include/header.php'; ?>

<div class="account">
	<div class="container">
		<div class="row">
			<div class="col-xl-3 col-md-4">
				<div class="account-sidebar-wrap">
					<div class="account-menu">
						Account Settings <i class="fas fa-bars"></i>
					</div>
					<div class="account-sidebar bg-white">
						<div class="close">×</div>
						<ul class="account-sidebar__links">
							<li>
								<a href="account-verify-mobile.php"><i class="fas fa-mobile-alt"></i>Verify Mobile Number</a>
							</li>
							<li>
								<a href="account-favourite-post.php"><i class="fas fa-heart"></i>Favorite Post</a>
							</li>
							<li>
								<a href="account-saved-post.php"><i class="fas fa-bookmark"></i>Saved Post</a>
							</li>
							<li>
								<a href="account-transaction-history.php"><i class="fas fa-exchange-alt"></i>Transaction History</a>
							</li>
							<li>
								<a href="account-change-password.php"><i class="fas fa-lock"></i>Change Password</a>
							</li>
							<li>
								<a href="account-terms-conditions.php"><i class="fas fa-clipboard-list"></i>Terms & Conditions</a>
							</li>
							<li>
								<a href="account-privacy-policy.php"><i class="fas fa-mobile-alt"></i>Privacy Policy</a>
							</li>
							<li>
								<a href="account-faq.php"><i class="fas fa-comments"></i>FAQs</a>
							</li>
							<li>
								<a href="account-feedback.php" class="active"><i class="fas fa-thumbs-up"></i>Feedback</a>
							</li>
							<li>
								<a href="account-delete.php"><i class="fas fa-trash"></i>Delete My Account</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-xl-9 col-md-8">
				<div class="account-settings">
					<div class="white-title">
						<h3>Feedback</h3>
					</div>
					<div class="w-370">
						<div class="form-group">
							<label>Select Topic</label>
							<select class="form-control blue">
								<option>Topic 1</option>
								<option>Topic 2</option>
								<option>Topic 3</option>
							</select>
							<div class="error">This is error message</div>
						</div>
						<div class="form-group">
							<label>Write Message</label>
							<textarea rows="5" cols="30" class="form-control" placeholder="Write Here..."></textarea>
						</div>
						<div class="form-group">
							<button type="submit" class="btn-custom btn-blue">Submit</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php include 'include/footer.php'; ?>