<?php include 'include/head.php';?>

<?php include 'include/header.php';?>

<div class="searchpage">
	<div class="container w-1000">
		<div class="searchpage__top">
			<div class="row align-items-center justify-content-between">
				<div class="searchpage__back">
					<a href="home.php"><img src="assets/images/back-btn-white.svg"></a>
				</div>
				<div class="searchpage__search-box">
					<div class="form-group">
						<input type="text" name="" class="form-control" placeholder="Search">
						<button type="submit"><img src="assets/images/search-btn-icon-blue.svg"></button>
					</div>
				</div>
				<div class="searchpage__filter-wrap">
					<div class="searchpage__filter">
						<a href="search-result-user.php" class="filter-btn">Users</a>
						<a href="search-result-post.php" class="filter-btn active">Posts</a>
					</div>
				</div>
			</div>
		</div>
		<div class="searchpage__result-main">
			<div class="searchpage__posts">
				<div class="feed-post-listing">
					
					<div class="feed-post">
						<div class="feed-post__top d-flex align-items-center justify-content-between">
							<div class="feed-post__top-left d-flex align-items-center">
								<a href="#"><img src="assets/images/client7.jpg" class="feed-post__admin-image"></a>
								<div class="feed-post__admin-detail">
									<a href="#"><div class="feed-post__admin-name">John Smith <img src="assets/images/announcement-icon-blue.svg"></div></a>
									<div class="feed-post__date">12:00, 12th Nov 2020</div>
								</div>
							</div>
							<div class="feed-post__top-right">
								<div class="feed-post__option">
									<i class="far fa-bookmark"></i>
								</div>
								<div class="feed-post__report">
									<i class="fas fa-ellipsis-h"></i>
									<div class="feed-post__report-dropdown" data-toggle="modal" data-target="#reportpost">Report Post</div>
								</div>
							</div>
						</div>
						<div class="feed-post__bottom">
							<div class="feed-post__content">Game Night Today!!</div>
							<div class="feed-post__action d-flex align-items-center justify-content-between">
								<div class="feed-post__action-left d-flex align-items-center">
									<div class="like">
										<i class="far fa-thumbs-up"></i> 120
									</div>
									<div class="heart">
										<i class="far fa-heart"></i> 8
									</div>
									<div class="comment">
										<i class="far fa-comment"></i> 28
									</div>
								</div>
								<div class="feed-post__action-right">
									<div class="share">
										<i class="fas fa-share"></i>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="feed-post">
						<div class="feed-post__top d-flex align-items-center justify-content-between">
							<div class="feed-post__top-left d-flex align-items-center">
								<a href="#"><img src="assets/images/client7.jpg" class="feed-post__admin-image"></a>
								<div class="feed-post__admin-detail">
									<a href="#"><div class="feed-post__admin-name">John Smith <img src="assets/images/announcement-icon-blue.svg"></div></a>
									<div class="feed-post__date">12:00, 12th Nov 2020</div>
								</div>
							</div>
							<div class="feed-post__top-right">
								<div class="feed-post__option">
									<i class="far fa-bookmark"></i>
								</div>
								<div class="feed-post__report">
									<i class="fas fa-ellipsis-h"></i>
									<div class="feed-post__report-dropdown" data-toggle="modal" data-target="#reportpost">Report Post</div>
								</div>
							</div>
						</div>
						<div class="feed-post__bottom">
							<div class="feed-post__content">
								Loved the Match Today!!! Congratulations to the Homeland Team
								<div class="feed-post__tags"><strong>with</strong> with John Smith and 2 Others</div>
								<div class="feed-post__image-video">
									<div class="swiper-container">
										<div class="swiper-wrapper">
											<div class="swiper-slide">
												<img src="assets/images/slider-image.jpg">
											</div>
											<div class="swiper-slide">
												<img src="assets/images/slider-image.jpg">
											</div>
											<div class="swiper-slide">
												<img src="assets/images/slider-image.jpg">
											</div>
											<div class="swiper-slide">
												<img src="assets/images/slider-image.jpg">
											</div>
											<div class="swiper-slide">
												<img src="assets/images/slider-image.jpg">
											</div>
										</div>
										<div class="swiper-pagination"></div>
										<div class="swiper-overlay"></div>
									</div>
								</div>
							</div>
							<div class="feed-post__action d-flex align-items-center justify-content-between">
								<div class="feed-post__action-left d-flex align-items-center">
									<div class="like">
										<i class="far fa-thumbs-up"></i> 120
									</div>
									<div class="heart">
										<i class="far fa-heart"></i> 8
									</div>
									<div class="comment">
										<i class="far fa-comment"></i> 28
									</div>
								</div>
								<div class="feed-post__action-right">
									<div class="share">
										<i class="fas fa-share"></i>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="feed-post">
						<div class="feed-post__top d-flex align-items-center justify-content-between">
							<div class="feed-post__top-left d-flex align-items-center">
								<a href="#"><img src="assets/images/client7.jpg" class="feed-post__admin-image"></a>
								<div class="feed-post__admin-detail">
									<a href="#"><div class="feed-post__admin-name">John Smith <img src="assets/images/announcement-icon-blue.svg"></div></a>
									<div class="feed-post__date">12:00, 12th Nov 2020</div>
								</div>
							</div>
							<div class="feed-post__top-right">
								<div class="feed-post__option">
									<i class="far fa-bookmark"></i>
								</div>
								<div class="feed-post__report">
									<i class="fas fa-ellipsis-h"></i>
									<div class="feed-post__report-dropdown" data-toggle="modal" data-target="#reportpost">Report Post</div>
								</div>
							</div>
						</div>
						<div class="feed-post__bottom">
							<div class="feed-post__content">
								Loved the Match Today!!! Congratulations to the Homeland Team
								<div class="feed-post__tags"><strong>with</strong> with John Smith and 2 Others</div>
								<div class="feed-post__image-video">
									<div class="swiper-container">
										<div class="swiper-wrapper">
											<div class="swiper-slide">
												<div class="video-wrap">
													<video controls>
														<source src="https://www.w3schools.com/tags/movie.mp4" type="video/mp4">
														Your browser does not support the video tag.
													</video>
												</div>
											</div>
											<div class="swiper-slide">
												<div class="video-wrap">
													<video controls>
														<source src="http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4" type="video/mp4">
														Your browser does not support the video tag.
													</video>
												</div>
											</div>
											<div class="swiper-slide">
												<div class="video-wrap">
													<video controls>
														<source src="http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ElephantsDream.mp4" type="video/mp4">
														Your browser does not support the video tag.
													</video>
												</div>
											</div>
											<div class="swiper-slide">
												<div class="video-wrap">
													<video controls>
														<source src="http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerEscapes.mp4" type="video/mp4">
														Your browser does not support the video tag.
													</video>
												</div>
											</div>
										</div>
										<div class="swiper-pagination"></div>
										<div class="swiper-overlay"></div>
									</div>
								</div>
							</div>
							<div class="feed-post__action d-flex align-items-center justify-content-between">
								<div class="feed-post__action-left d-flex align-items-center">
									<div class="like">
										<i class="far fa-thumbs-up"></i> 120
									</div>
									<div class="heart">
										<i class="far fa-heart"></i> 8
									</div>
									<div class="comment">
										<i class="far fa-comment"></i> 28
									</div>
								</div>
								<div class="feed-post__action-right">
									<div class="share">
										<i class="fas fa-share"></i>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="feed-post">
						<div class="feed-post__top d-flex align-items-center justify-content-between">
							<div class="feed-post__top-left d-flex align-items-center">
								<a href="#"><img src="assets/images/client7.jpg" class="feed-post__admin-image"></a>
								<div class="feed-post__admin-detail">
									<a href="#"><div class="feed-post__admin-name">John Smith <img src="assets/images/announcement-icon-blue.svg"></div></a>
									<div class="feed-post__date">12:00, 12th Nov 2020</div>
								</div>
							</div>
							<div class="feed-post__top-right">
								<div class="feed-post__option">
									<i class="far fa-bookmark"></i>
								</div>
								<div class="feed-post__report">
									<i class="fas fa-ellipsis-h"></i>
									<div class="feed-post__report-dropdown" data-toggle="modal" data-target="#reportpost">Report Post</div>
								</div>
							</div>
						</div>
						<div class="feed-post__bottom">
							<div class="feed-post__content">Game Night Today!!</div>
							<div class="feed-post__action d-flex align-items-center justify-content-between">
								<div class="feed-post__action-left d-flex align-items-center">
									<div class="like">
										<i class="far fa-thumbs-up"></i> 120
									</div>
									<div class="heart">
										<i class="far fa-heart"></i> 8
									</div>
									<div class="comment">
										<i class="far fa-comment"></i> 28
									</div>
								</div>
								<div class="feed-post__action-right">
									<div class="share">
										<i class="fas fa-share"></i>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade reportpost" id="reportpost" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-body pb-0">
				<div class="modal-title">
					<h3>Select Report Post Issue</h3>
				</div>
				<div class="reportpost__type">
					<ul>
						<li><a href="#">Spam Post</a></li>
						<li><a href="#">Represents Violence</a></li>
						<li><a href="#">Obscene Content</a></li>
						<li><a href="#">Racist Post</a></li>
					</ul>
				</div>
			</div>
			<div class="btn-close" data-dismiss="modal"><img src="assets/images/close-black-icon.svg"></div>
		</div>
	</div>
</div>

<?php include 'include/footer.php';?>