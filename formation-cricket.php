<?php include 'include/head.php'; ?>

<?php include 'include/header.php'; ?>

<div class="formation-detail">
	<div class="container">
		<div class="row">
			<div class="col-lg-4">
				<div class="backbtn">
					<a href="activity-detail.php" class="btn-custom white-border-btn d-inline-block"><img src="assets/images/arrow-reverse.svg"> Back to Activity</a>
				</div>
				<div class="team-players">
					<div class="team-players__wrap">
						<div class="team-players__box">
							<h3>Team</h3>
							<div class="expand-icon"><i class="fa-plus fas"></i></div>
							<div class="team-players__individual">
								<div class="accordion">
									<div class="card">
										<div class="card-header" id="TeamOne">
											<a class="btn btn-link btn-block text-left" role="button" data-toggle="collapse" href="#TeamcollapseOne" aria-expanded="true" aria-controls="TeamcollapseOne"></a>
											<span>Team 1</span> <i data-toggle="modal" data-target="#editteamname" class="fas fa-pencil-alt"></i>
										</div>
										<div id="TeamcollapseOne" class="collapse multi-collapse show" aria-labelledby="TeamOne">
											<div class="card-body">
												<ul class="team-players__list">
													<li><a href="#"><img src="assets/images/client7.jpg">John Smith <span class="num">(21)</span></a> <div class="icon"><i class="fas fa-minus"></i></div></li>
													<li><a href="#"><img src="assets/images/client7.jpg">John Smith <span class="num">(21)</span></a> <div class="icon"><i class="fas fa-minus"></i></div></li>
													<li><a href="#"><img src="assets/images/client7.jpg">John Smith <span class="num">(21)</span></a> <div class="icon"><i class="fas fa-minus"></i></div></li>
													<li><a href="#"><img src="assets/images/client7.jpg">John Smith <span class="num">(21)</span></a> <div class="icon"><i class="fas fa-minus"></i></div></li>
													<li><a href="#"><img src="assets/images/client7.jpg">John Smith <span class="num">(21)</span></a> <div class="icon"><i class="fas fa-minus"></i></div></li>
												</ul>
											</div>
										</div>
									</div>
									<div class="card">
										<div class="card-header" id="TeamTwo">
											<a class="btn btn-link btn-block text-left collapsed" role="button" data-toggle="collapse" href="#TeamcollapseTwo" aria-expanded="false" aria-controls="TeamcollapseTwo"></a>
											<span>Team 2</span> <i data-toggle="modal" data-target="#editteamname" class="fas fa-pencil-alt"></i>
										</div>
										<div id="TeamcollapseTwo" class="collapse multi-collapse" aria-labelledby="TeamTwo">
											<div class="card-body">
												<ul class="team-players__list">
													<li><a href="#"><img src="assets/images/client7.jpg">John Smith <span class="num">(21)</span></a> <div class="icon"><i class="fas fa-minus"></i></div></li>
													<li><a href="#"><img src="assets/images/client7.jpg">John Smith <span class="num">(21)</span></a> <div class="icon"><i class="fas fa-minus"></i></div></li>
													<li><a href="#"><img src="assets/images/client7.jpg">John Smith <span class="num">(21)</span></a> <div class="icon"><i class="fas fa-minus"></i></div></li>
													<li><a href="#"><img src="assets/images/client7.jpg">John Smith <span class="num">(21)</span></a> <div class="icon"><i class="fas fa-minus"></i></div></li>
													<li><a href="#"><img src="assets/images/client7.jpg">John Smith <span class="num">(21)</span></a> <div class="icon"><i class="fas fa-minus"></i></div></li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-8">
				<div class="formation-detail__right">
					<div class="formation-detail__select-team">
						<div class="white-title">
							<h3>Team</h3>
						</div>
						<div class="form-group">
							<label>Select Team</label>
							<select name="" id="" class="form-control blue">
								<option value="team1">Team 1</option>
								<option value="team2">Team 2</option>
							</select>
						</div>
					</div>
					<div class="formation-detail__ground">
						
						<div id="groundarea">
							<img src="assets/images/cricket-ground.png">
							<div class="player player__item player__item--eleven">
								<div class="player player__item--shirt-icon">
									<img src="assets/images/client14.jpg">
								</div>
								<div class="player player__item--label">
									Suárez
								</div>
							</div>
							<div class="player player__item player__item--ten">
								<div class="player player__item--shirt-icon">
									<img src="assets/images/client13.jpg">
								</div>
								<div class="player player__item--label">
									Neymar
								</div>
							</div>
							<div class="player player__item player__item--nine">
								<div class="player player__item--shirt-icon">
									<img src="assets/images/client11.jpg">
								</div>
								<div class="player player__item--label">
									Messi
								</div>
							</div>
							<div class="player player__item player__item--eight">
								<div class="player player__item--shirt-icon">
									<img src="assets/images/client10.jpg">
								</div>
								<div class="player player__item--label">
									Iniesta
								</div>
							</div>
							<div class="player player__item teamopp player__item--seven">
								<div class="player player__item--shirt-icon">
									<img src="assets/images/client9.jpg">
								</div>
								<div class="player player__item--label">
									Busquets
								</div>
							</div>
							<div class="player player__item player__item--six">
								<div class="player player__item--shirt-icon">
									<img src="assets/images/client7.jpg">
								</div>
								<div class="player player__item--label">
									Jordi Alba
								</div>
							</div>
							<div class="player player__item teamopp player__item--five">
								<div class="player player__item--shirt-icon">
									<img src="assets/images/client5.jpg">
								</div>
								<div class="player player__item--label">
									Rakitic
								</div>
							</div>
							<div class="player player__item player__item--four">
								<div class="player player__item--shirt-icon">
									<img src="assets/images/client4.jpg">
								</div>
								<div class="player player__item--label">
									Mascherano
								</div>
							</div>
							<div class="player player__item teamopp player__item--three">
								<div class="player player__item--shirt-icon">
									<img src="assets/images/client2.jpg">
								</div>
								<div class="player player__item--label">
									Piqué
								</div>
							</div>
							<div class="player player__item player__item--two">
								<div class="player player__item--shirt-icon">
									<img src="assets/images/client1.jpg">
								</div>
								<div class="player player__item--label">
									Daniel Alves
								</div>
							</div>
							<div class="player player__item teamopp player__item--gk">
								<div class="player player__item--shirt-icon">
									<img src="assets/images/client14.jpg">
								</div>
								<div class="player player__item--label">
									Ter Stegen
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade editteamname" id="editteamname" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<div class="modal-title">
					<h3>Update Team Name</h3>
				</div>
				<div class="editteamname-form">
					<div class="form-group">
						<input type="text" class="form-control" placeholder="Enter Team Name">
					</div>
					<div class="form-group text-center mb-0">
						<button type="submit" class="btn-custom">Submit</button>
					</div>
				</div>
			</div>
			<div class="btn-close" data-dismiss="modal"><img src="assets/images/close-black-icon.svg"></div>
		</div>
	</div>
</div>

<?php include 'include/footer.php'; ?>