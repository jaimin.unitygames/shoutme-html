<?php include 'include/head-login-register.php';?>

<?php include 'include/header-login-register.php';?>

<section class="login-register">
	<div class="container">
		<h1>Pages Link</h1>
		<ol>
			<li><a target="_blank" href="index.php">Landing Page</a></li>
			<li><a target="_blank" href="login.php">Login</a></li>
			<li><a target="_blank" href="register.php">Register</a></li>
			<li><a target="_blank" href="mobile-number-verification.php">Mobile Number Verification</a></li>
			<li><a target="_blank" href="mobile-number-verification-otp.php">Mobile Number Verification OTP</a></li>
			<li><a target="_blank" href="about-your-self.php">About Yourself</a></li>
			<li><a target="_blank" href="interested-activities.php">Interested Activities</a></li>
			<li><a target="_blank" href="final-step.php">Final Step</a></li>
			<li><a target="_blank" href="home.php">Home</a></li>
			<li><a target="_blank" href="search-result-user.php">Search Users</a></li>
			<li><a target="_blank" href="search-result-post.php">Search Posts</a></li>
			<li><a target="_blank" href="explore-nearby-activities.php">Explore Nearby Activities</a></li>
			<li><a target="_blank" href="activity-detail.php">Activity Detail</a></li>
			<li><a target="_blank" href="activity-payment-complete.php">Activity Payment Complete (21_Confirmation)</a></li>
			<li><a target="_blank" href="invite-participant.php">Invite Participants (27_Invite)</a></li>
			<li><a target="_blank" href="my-activity-hub.php">My Activity Hub</a></li>
			<li><a target="_blank" href="create-activity.php">Create New Activity</a></li>
			<li><a target="_blank" href="invite-participant-create-activity.php">Invite Participants (31_Invite)</a></li>
			<li><a target="_blank" href="formation.php">Formation</a></li>
			<li><a target="_blank" href="player-detail.php">Player Profile</a></li>
			<li><a target="_blank" href="tournament-list.php">Tournament List</a></li>
			<li><a target="_blank" href="tournament-detail.php">Tournament Detail</a></li>
			<li><a target="_blank" href="tournament-schedule.php">Tournament Schedule</a></li>
			<li><a target="_blank" href="tournament-point-table.php">Tournament Point Table</a></li>
			<li><a target="_blank" href="notification.php">All Notifications</a></li>
			<li><a target="_blank" href="my-profile.php">My Profile</a></li>
			<li><a target="_blank" href="account-verify-mobile.php">Verify Mobile Number</a></li>
			<li><a target="_blank" href="account-favourite-post.php">Favorite Post</a></li>
			<li><a target="_blank" href="account-saved-post.php">Saved Post</a></li>
			<li><a target="_blank" href="account-transaction-history.php">Transaction History</a></li>
			<li><a target="_blank" href="account-change-password.php">Change Password</a></li>
			<li><a target="_blank" href="account-terms-conditions.php">Terms & Conditions</a></li>
			<li><a target="_blank" href="account-privacy-policy.php">Privacy Policy</a></li>
			<li><a target="_blank" href="account-faq.php">FAQs</a></li>
			<li><a target="_blank" href="account-feedback.php">Feedback</a></li>
			<li><a target="_blank" href="account-delete.php">Delete My Account</a></li>
		</ol>
	</div>
</section>

<style type="text/css">
	h1 {
		color: #ffffff;
		font-size: 30px;
		margin-bottom: 30px;
	}
	ol {
		list-style-type: decimal;
		padding-left: 30px;
	}
	ol li {
		color: #ffffff;
		font-size: 18px;
		display: list-item;
		margin-bottom: 10px;
	}
	ol li a {
		color: #ffffff;
		text-decoration: none;
	}
	ol li a:hover,
	ol li a:focus {
		color: #ffffff;
	}
</style>

<?php include 'include/footer-login-register.php'; ?>