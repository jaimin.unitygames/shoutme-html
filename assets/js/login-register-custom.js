$(document).ready(function() {

	// /****** Datepicker ******/
	// if($(".datepicker").length){
	// 	$(".datepicker").datetimepicker({ 
	// 		format: 'DD-MM-YYYY'
	// 	});
	// }

	/****** Datepicker ******/
	if($(".datepicker").length){
		$(".datepicker").datepicker({ 
			dateFormat: 'dd-mm-yy',
			changeMonth: true,
			changeYear: true,
			yearRange: "-50:+00",
			maxDate: new Date()
		});
	}

	/****** Timepicker ******/
	if($(".timepicker").length){
		$('.timepicker').datetimepicker({
			format: 'LT'
		});
	}

});