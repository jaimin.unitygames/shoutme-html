$(document).ready(function() {

	/****** Mobile Menu ******/
	$('.header__mobile-icon').on('click', function(e){
		e.preventDefault();
		$('body').addClass('overflow-hidden');
		$('.header__mobile-menu').addClass('open');
		$('.header__mobile-menu-bg').addClass('active');
		return false;
	});

	$('.header__mobile-menu .close').on('click', function(e){
		e.preventDefault();
		$('body').removeClass('overflow-hidden');
		$('.header__mobile-menu').removeClass("open");
		$('.header__mobile-menu-bg').removeClass('active');
		return false;
	});

	/****** Datepicker ******/
	if($(".datepicker").length){
		$(".datepicker").datetimepicker({ 
			format: 'DD-MM-YYYY'
		});
	}

	/****** Timepicker ******/
	if($(".timepicker").length){
		$('.timepicker').datetimepicker({
			format: 'LT'
		});
	}

	/****** Multiselect ******/
	if($(".multiselect").length){
		$('.multiselect').multiselect({
			numberDisplayed: 0,
			allSelectedText: false
		});
	}

	/****** Feed Post Like/Heart ******/
	if($(".feed-post__action-left").length){
		$('.feed-post__action-left .like, .feed-post__action-left .heart').click(function() {
			$(this).toggleClass('active');
		});

		$('.feed-post__option .fa-bookmark').click(function() {
			$(this).toggleClass('fas fa-bookmark');
			$(this).toggleClass('far fa-bookmark');
		});
	}

	/****** Create Post - Search Result Box ******/
	function checkForInput(element) {

		const $label = $(element).parent('.form-group');

		if ($(element).val().length > 0) {
			$label.addClass('input-has-value');
		} else {
			$label.removeClass('input-has-value');
		}
	}

	if($(".createpost__search .form-control, .invite-participant__search-box .form-control").length){
		$('.createpost__search .form-control, .invite-participant__search-box .form-control').on('change keyup', function() {
			checkForInput(this);
		});

		var hide = true;

		$('body').on("click", function () {
			if (hide) $('.createpost__search, .invite-participant__search-box').removeClass('active');			
			hide = true;
		});

		$('body').on('click', '.createpost__search, .invite-participant__search-box', function () {

			var self = $(this);

			self.addClass('active');
			hide = false;
		});

	}

	/****** Create Post - Search Result Box ******/
	if($(".textarea.autogrow2").length){
		$('.textarea.autogrow2').autosize();
	}
	if($(".textarea.autogrow").length){
		$('#createpost').on('shown.bs.modal', function () {
			$('.textarea.autogrow').autosize();
		})
	}

	/****** Feed Post - Swiper Slider Disable for Swipe for single ******/
	if($(".feed-post .swiper-container").length){
		$('.feed-post').each(function() {
			if($(this).find('.swiper-container .swiper-slide').length == 1) {
				$(this).find('.swiper-wrapper').addClass('disabled');
				$(this).find('.swiper-pagination').addClass('disabled');
			}
		});
	}

	/****** Feed Post - Swiper Slider ******/
	if($(".feed-post__image-video").length){
		var mySwiper = (function() {

			// Variables
			var $swiperContainer = $(".feed-post__image-video");

			function init($this) {

				// Swiper elements
				var $el = $this.find('.swiper-container');

				var $swiper = new Swiper($el, {
					pagination: {
						el: '.swiper-pagination',
						dynamicBullets: true,
						clickable: true
					}
				});
				// $swiper.on('slideChange', function () {
				// 	console.log('slide changed');
				// 	$('.feed-post__image-video video').trigger('pause');
				// });
			}

			// Events
			if ($swiperContainer.length) {
				$swiperContainer.each(function(i, Slider) {
					init($(Slider));
				})
			}

		})();
	}


	/****** Activity Nearby Filter Click ******/
	if($(".activity-filter-icon").length){
		$('.activity-filter-icon').on('click', function(e){
			$('.filter-sidebar').addClass('open');
			return false;
		});
		$('.filter-sidebar .close').on('click', function(e){
			$(this).parent('.filter-sidebar').removeClass('open');
			return false;
		});
	}

	/****** Search Page Input Focus ******/
	if($(".searchpage__search-box").length){
		$(".searchpage__search-box .form-control").focus();
	}

	/****** Comment replay tag close Click ******/
	if($(".postcomment__replay-label").length){
		$('.postcomment__replay-label .left span').on('click', function(e){
			$(this).parents('.postcomment__write-top').hide();
		});
	}

	/****** Team Formation Payer Update ******/
	if($(".team-players__list").length){
		$('.team-players__list li .icon').click(function() {
			$(this).toggleClass('remove');
			$(this).find('i').toggleClass('fas fa-plus');
			$(this).find('i').toggleClass('fas fa-minus');
		});
	}

	/****** Team Formation Mobile Player Open ******/
	if($(".team-players__box .expand-icon").length){
		$('.team-players__box h3').click(function() {
			$('.team-players__individual').toggleClass('open');
		});
	}

	/****** Create Activity Next Step Click Event ******/
	if($(".create-activity__form .next").length){
		$('.create-activity__form .next a').click(function() {
			$(this).parents('.form-step1').hide();
			$(this).parents('.form-step1').next('.form-step2').show();
		});
	}
	if($(".create-activity__form .back").length){
		$('.create-activity__form .back').click(function() {
			$(this).parents('.form-step2').hide();
			$(this).parents('.create-activity__form').find('.form-step1').show();
		});
	}

	/****** Account Sidebar Mobile ******/
	if($(".account-menu").length){
		$('.account-menu').click(function() {
			$(this).next('.account-sidebar').addClass('open');
		});
		$('.account-sidebar .close').click(function() {
			$(this).parents('.account-sidebar').removeClass('open');
		});
	}

	$('.account-sidebar-wrap').scrollToFixed({
		marginTop: $('.header').outerHeight(true) + 10,
		limit: $('footer').offset().top
	});

	/****** Report Comment ******/
	if($("#reportcomment").length){
		$('#reportcomment').on('shown.bs.modal', function (e) {
			$("#postcomment").addClass("disabled");
		});
		$('#reportcomment').on('hidden.bs.modal', function () {
			$("#postcomment").removeClass("disabled");
		});
	}

});


$(document).ready(function(){
	$(".player__item").draggable({
		containment: "#groundarea",
		cursor: "grabbing"
	});
	$(".player__item" ).on( "dragstop", function( event, ui ) {
		var playerId = $(this).data('playerId');
		var leftPercent = ( 100 * parseFloat(ui.position.left / parseFloat($('#groundarea').width())) );
		var topPercent = ( 100 * parseFloat(ui.position.top / parseFloat($('#groundarea').height())) );
		$.ajax({
			type: "POST",
			url: "./update_position.php",
			cache: false,
			data: {'playerId': playerId, 'leftPercent': leftPercent, 'topPercent': topPercent, 'left': ui.position.left, 'top': ui.position.top},
			dataType: 'json',
			success: function (result) {
				console.log(result);
			},
			error: function (xhr, ajaxOptions, thrownError) {
				console.log(xhr);
			}
		});
	});
});