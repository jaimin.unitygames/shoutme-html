$(document).ready(function() {

	/****** Testimonial ******/
	if($(".testimonial").length){
		$('.testimonial__images a').on('click', function (event) {
			event.preventDefault();
			$('.testimonial__image-box--active').removeClass('testimonial__image-box--active');
			$(this).parent().addClass('testimonial__image-box--active');
			$('.testimonial__content-block').hide();
			$($(this).attr('href')).show();
		});
		$('.testimonial__image-row--1  .testimonial__image-box:first a').trigger('click');
	}

	/****** Header Mobile Button ******/
	$(".header__mobile-menu").click(function(){
		$(".header__buttons").slideToggle();
	});

});