<?php include 'include/head.php'; ?>

<?php include 'include/header.php'; ?>

<div class="vc-table">
	<div class="vc-table-cell">
		<div class="container w-700">
			<div class="payment-success text-center">
				<div class="payment-success__icon">
					<img src="assets/images/check-round-blue-icon.svg" alt="">
				</div>
				<div class="payment-success__content">
					<h2>Congratulations</h2>
					<div class="text">Your Payment for the Football has been successfully recieved</div>
					<div class="buttons d-flex align-items-center justify-content-between">
						<a href="#" class="btn-custom btn-blue-gradient">Go To Activity Details</a>
						<a href="#" class="btn-custom white-border-btn noicon">Explore Other Activities</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php include 'include/footer.php'; ?>