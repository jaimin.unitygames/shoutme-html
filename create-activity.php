<?php include 'include/head.php'; ?>

<?php include 'include/header.php'; ?>

<div class="my-activity create-activity">
	<div class="container">
		<div class="white-title">
			<h3>Create New Activity</h3>
		</div>
	</div>
	<div class="container w-800">
		<div class="create-activity__form">
			<div class="form-step1">
				<div class="create-activity__map">
					<div class="form-group">
						<input type="text" class="form-control" placeholder="Search Location">
						<button type="submit"><img src="assets/images/search-btn-icon-darkblue.svg"></button>
					</div>
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2484.0072458382283!2d-0.10628488402851216!3d51.4947345194499!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4876059dbe55c279%3A0xce04e2436b2b0201!2sFootball%20Ground!5e0!3m2!1sen!2sin!4v1624952164615!5m2!1sen!2sin" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="">Select Activity</label>
							<select name="" id="" class="form-control">
								<option value="Football">Football</option>
								<option value="Basketball">Basketball</option>
								<option value="Hockey">Hockey</option>
								<option value="Rugby">Rugby</option>
								<option value="Cricket">Cricket</option>
								<option value="Tennis">Tennis</option>
								<option value="Squash">Squash</option>
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="">Date</label>
							<input type="text" class="form-control datepicker" placeholder="dd/mm/yyyy">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="">Start Time</label>
							<input type="text" class="timepicker form-control" placeholder="hh:mm">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="">Duration</label>
							<input type="text" class="form-control" placeholder="Hours">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group select-age">
							<label for="">Select Age</label>
							<div class="input-group d-flex flex-wrap align-items-center justify-content-between">
								<input type="text" class="form-control" placeholder="From">
								<span>to</span>
								<input type="text" class="form-control" placeholder="To">
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="">Gender</label>
							<div class="input-group d-flex flex-wrap radio-custom">
								<div>
									<input type="radio" id="any" name="gender" value="any" checked="true">
									<label for="any">Any</label>
								</div>
								<div>
									<input type="radio" id="male" name="gender" value="male">
									<label for="male">Male</label>
								</div>
								<div>
									<input type="radio" id="female" name="gender" value="female">
									<label for="female">Female</label>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="">Amount to be Paid for Sports Pitch</label>
							<div class="input-group mb-3">
								<div class="input-group-prepend">
									<span class="input-group-text">£</span>
								</div>
								<input type="text" class="form-control" aria-label="Amount (to the nearest Pound)">
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="">Type of Game</label>
							<select name="" id="" class="form-control">
								<option value="5vs5">5 vs 5</option>
								<option value="7vs7">7 vs 7</option>
								<option value="10vs10">10 vs 10</option>
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="">Comments (Optional)</label>
							<textarea name="" id="" cols="30" rows="5" class="form-control" placeholder="Please specify details regarding the game"></textarea>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group autojoin">
							<label for="autojoin" class="custom-checkbox d-flex align-items-center justify-content-between">Auto Join
								<input type="checkbox" id="autojoin" name="autojoin" value="true">
								<span class="checkmark"></span>
							</label>
						</div>
					</div>
					<div class="col-md-12 text-center">
						<div class="form-group next">
							<a href="javascript:;" class="btn-custom btn-blue-gradient d-inline-block w-auto">Next</a>
						</div>
					</div>
				</div>
			</div>
			<div class="form-step2">
				<div class="invite-participant__box text-center">
					<h3>Invite Participants to your Activity</h3>
					<div class="invite-participant__search-box">
						<div class="form-group">
							<input type="text" name="" class="form-control" placeholder="Search Participant Name">
							<button type="submit"><img src="assets/images/search-btn-icon-darkblue.svg"></button>
						</div>
					</div>
					<div class="invite-participant__map">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2484.0072458382283!2d-0.10628488402851216!3d51.4947345194499!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4876059dbe55c279%3A0xce04e2436b2b0201!2sFootball%20Ground!5e0!3m2!1sen!2sin!4v1624952164615!5m2!1sen!2sin" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
					</div>
					<div class="submit">
						<div class="d-flex align-items-center justify-content-center">
							<a href="javascript:;" class="back btn-custom white-border-btn">Back</a>
							<button type="submit" class="btn-custom btn-blue-gradient">Finish</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php include 'include/footer.php'; ?>