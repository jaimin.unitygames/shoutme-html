<?php include 'include/head.php'; ?>

<?php include 'include/header.php'; ?>

<div class="py-2 py-md-3 notification">
	<div class="container w-600">
		<div class="white-title mb-4 text-center">
			<h2>All Notifications</h2>
		</div>
		<div class="notification__list">
			<div class="notification-box">
				<a href="#">
					<div class="top"><span>John Smith</span> has invited you to <span>Football Activity</span></div>
					<div class="time">12:00, 12th Nov 2020</div>
				</a>
			</div>
			<div class="notification-box">
				<a href="#">
					<div class="top"><span>John Smith</span> has invited you to <span>Football Activity</span></div>
					<div class="time">12:00, 12th Nov 2020</div>
				</a>
			</div>
			<div class="notification-box">
				<a href="#">
					<div class="top"><span>John Smith</span> has invited you to <span>Football Activity</span></div>
					<div class="time">12:00, 12th Nov 2020</div>
				</a>
			</div>
			<div class="notification-box">
				<a href="#">
					<div class="top"><span>John Smith</span> has invited you to <span>Football Activity</span></div>
					<div class="time">12:00, 12th Nov 2020</div>
				</a>
			</div>
			<div class="notification-box">
				<a href="#">
					<div class="top"><span>John Smith</span> has invited you to <span>Football Activity</span></div>
					<div class="time">12:00, 12th Nov 2020</div>
				</a>
			</div>
			<div class="notification-box">
				<a href="#">
					<div class="top"><span>John Smith</span> has invited you to <span>Football Activity</span></div>
					<div class="time">12:00, 12th Nov 2020</div>
				</a>
			</div>
		</div>
	</div>
</div>

<?php include 'include/footer.php'; ?>