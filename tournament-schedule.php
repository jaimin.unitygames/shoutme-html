<?php include 'include/head.php'; ?>

<?php include 'include/header.php'; ?>

<div class="my-activity tournament">
	<div class="container">
		<div class="tournament-schedule">
			<div class="tournament-schedule__top">
				<div class="backbtn">
					<a href="tournament-detail.php" class="btn-custom white-border-btn d-inline-block"><img src="assets/images/arrow-reverse.svg"> Back to Tournament</a>
				</div>
				<div class="white-title">
					<h2>Tournament Schedule</h2>
				</div>
			</div>
			<div class="tournament-schedule__list">
				<div class="row justify-content-center">
					<div class="col-lg-4 col-sm-6 d-flex">
						<div class="tournament-schedule__box d-flex flex-column justify-content-between bg-white w-100">
							<h3>Group 1</h3>
							<div class="tournament-schedule__teams d-flex align-items-center justify-content-between">
								<div class="tournament-schedule__team-box">
									<div class="name">Team Name</div>
									<span class="num">5</span>
								</div>
								<div class="vs">vs</div>
								<div class="tournament-schedule__team-box">
									<div class="name">Team Name</div>
									<span class="num">5</span>
								</div>
							</div>
							<div class="datetime">02-11-2020 10:00 am</div>
						</div>
					</div>
					<div class="col-lg-4 col-sm-6 d-flex">
						<div class="tournament-schedule__box bg-white d-flex flex-column justify-content-between w-100">
							<h3>Group 2</h3>
							<div class="tournament-schedule__teams d-flex align-items-center justify-content-between">
								<div class="tournament-schedule__team-box">
									<div class="name">Team Name</div>
								</div>
								<div class="vs">vs</div>
								<div class="tournament-schedule__team-box">
									<div class="name">Team Name</div>
								</div>
							</div>
							<div class="datetime">02-11-2020 10:00 am</div>
						</div>
					</div>
					<div class="col-lg-4 col-sm-6 d-flex">
						<div class="tournament-schedule__box d-flex flex-column justify-content-between bg-white w-100">
							<h3>Group 3</h3>
							<div class="tournament-schedule__teams d-flex align-items-center justify-content-between">
								<div class="tournament-schedule__team-box">
									<div class="name">Team Name</div>
								</div>
								<div class="vs">vs</div>
								<div class="tournament-schedule__team-box">
									<div class="name">Team Name</div>
								</div>
							</div>
							<div class="datetime">02-11-2020 10:00 am</div>
						</div>
					</div>
					<div class="col-lg-4 col-sm-6 d-flex">
						<div class="tournament-schedule__box d-flex flex-column justify-content-between bg-white w-100">
							<h3>Group 1</h3>
							<div class="tournament-schedule__teams d-flex align-items-center justify-content-between">
								<div class="tournament-schedule__team-box">
									<div class="name">Team Name</div>
									<span class="num">5</span>
								</div>
								<div class="vs">vs</div>
								<div class="tournament-schedule__team-box">
									<div class="name">Team Name</div>
									<span class="num">5</span>
								</div>
							</div>
							<div class="datetime">02-11-2020 10:00 am</div>
						</div>
					</div>
					<div class="col-lg-4 col-sm-6 d-flex">
						<div class="tournament-schedule__box d-flex flex-column justify-content-between bg-white w-100">
							<h3>Group 3</h3>
							<div class="tournament-schedule__teams d-flex align-items-center justify-content-between">
								<div class="tournament-schedule__team-box">
									<div class="name">Team Name</div>
								</div>
								<div class="vs">vs</div>
								<div class="tournament-schedule__team-box">
									<div class="name">Team Name</div>
								</div>
							</div>
							<div class="datetime">02-11-2020 10:00 am</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php include 'include/footer.php'; ?>