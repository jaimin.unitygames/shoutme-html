<header class="header">
	<div class="container">
		<div class="header__inner d-flex align-items-center justify-content-between">
			<div class="header__logo">
				<a href="home.php">SHOUT ME</a>
			</div>
			<div class="header__right d-flex align-items-center">
				<div class="header__menu">
					<ul class="d-flex">
						<li>
							<a href="home.php" class="active">
								<svg xmlns="http://www.w3.org/2000/svg" width="25" height="23.606" viewBox="0 0 25 23.606">
									<g transform="translate(-0.073 0.25)"><path class="a" d="M12.914-.111,24.894,9.832a.534.534,0,0,1-.371.926H21.745V22.8a.553.553,0,0,1-.556.556H15.384a.553.553,0,0,1-.556-.556V15.822a.5.5,0,0,0-.494-.556h-3.52a.553.553,0,0,0-.556.556V22.8a.5.5,0,0,1-.494.556H3.959A.553.553,0,0,1,3.4,22.8V10.758H.562a.531.531,0,0,1-.309-.926L12.234-.111A.485.485,0,0,1,12.914-.111Z" transform="translate(0 0)"/></g>
								</svg>
								<span>Home</span>
							</a>
						</li>
						<li>
							<a href="explore-nearby-activities.php">
								<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 25 25">
									<g transform="translate(0 -0.022)"><path class="a" d="M3.859,25A3.764,3.764,0,0,1,0,21.017V5.518A3.86,3.86,0,0,1,4.5,1.712,3.922,3.922,0,0,1,7.618,4.785h5.107c-.019.209-.03.421-.03.635a6.823,6.823,0,0,0,.051.83H7.716v8.486L13.61,8.841a6.916,6.916,0,0,0,.871,1.2l-1.725,1.725,10.5,10.5a2.378,2.378,0,0,0,.281-1.124V11.6l.41-.86A6.866,6.866,0,0,0,25,9.662v11.48A3.862,3.862,0,0,1,21.143,25Zm6.5-1.465h10.78a2.376,2.376,0,0,0,1.06-.248l-5.8-5.8Zm-8.534-3.66a2.372,2.372,0,0,0,2.03,3.66H8.292l7.08-7.08L11.72,12.8l-4,4v4.335a.732.732,0,1,1-1.465,0,2.393,2.393,0,0,0-4.422-1.267Zm17.09-4.765L16.389,9.8a5.419,5.419,0,1,1,6.384,0l-2.531,5.31a.733.733,0,0,1-1.322,0ZM17.286,5.42a2.295,2.295,0,1,0,2.295-2.295A2.3,2.3,0,0,0,17.286,5.42Z" transform="translate(0 0.022)"/></g>
								</svg>
								<span>Explore Nearby Activities</span>
							</a>
						</li>
						<li>
							<a href="my-activity-hub.php">
								<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 25 25">
									<g transform="translate(-65 -228)"><path class="a" d="M768.214,255H757.5V242.5h4.464v-.893a1.786,1.786,0,1,1,3.571,0v.893h2.679v2.791a4.054,4.054,0,0,0-.417.136,3.577,3.577,0,0,0-2.244,2.96,3.634,3.634,0,0,0,.038.994A3.574,3.574,0,0,0,767.637,252a3.906,3.906,0,0,0,.409.156c.084.025.084.025.169.048Zm14.286,0H770v-4.464h-.893a1.786,1.786,0,1,1,0-3.571H770V242.5h4.464v.893a1.786,1.786,0,0,0,3.571,0V242.5H782.5Zm-22.209-14.286H757.5V230H770v4.464h.893a1.786,1.786,0,0,1,0,3.571H770v2.679h-2.791a3.794,3.794,0,0,0-.362-.887,3.6,3.6,0,0,0-1.952-1.6,3.688,3.688,0,0,0-1.054-.187c-.091,0-.182,0-.273,0a3.693,3.693,0,0,0-.963.184,3.6,3.6,0,0,0-1.815,1.385,3.773,3.773,0,0,0-.394.771c-.029.083-.029.082-.056.166S760.314,240.629,760.291,240.714Z" transform="translate(-692.5 -2)"/></g>
								</svg>
								<span class="badge">4</span>
								<span>My Activity Hub</span>
							</a>
						</li>
						<li>
							<a href="tournament-list.php">
								<svg xmlns="http://www.w3.org/2000/svg" width="20" height="25" viewBox="0 0 20 25"><defs><style>.tournamentsicon{fill-rule:evenodd;}</style></defs><path class="tournamentsicon" d="M675.167,75.833h-1.25a4.166,4.166,0,0,1-4.167-4.167h-.833A2.917,2.917,0,0,1,666,68.754V63.333a.833.833,0,0,1,.833-.833h2.917V60.413a.414.414,0,0,1,.416-.413h11.668a.416.416,0,0,1,.416.413V62.5h2.917a.833.833,0,0,1,.833.833v5.421a2.917,2.917,0,0,1-2.917,2.913h-.833a4.168,4.168,0,0,1-4.167,4.167h-1.25V80H678.5a2.5,2.5,0,0,1,2.5,2.5V85H671V82.5a2.5,2.5,0,0,1,2.5-2.5h1.669Zm7.083-11.667V70h.833a1.25,1.25,0,0,0,1.25-1.246V64.167Zm-12.5,0V70h-.833a1.25,1.25,0,0,1-1.25-1.246V64.167Z" transform="translate(-666 -60)"/></svg>
								<span>Tournaments</span>
							</a>
						</li>
					</ul>
				</div>
				<div class="header__actions-wrap d-flex align-items-center">
					<div class="header__action header__action--message">
						<a href="#">
							<img src="assets/images/messages-icon.svg">
							<span>8</span>
						</a>
					</div>
					<div class="header__action header__action--notification dropdown">
						<a href="#" role="button" class="dropdown-toggle" id="notification-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<img src="assets/images/notification-icon.svg">
							<span>2</span>
						</a>
						<div class="header__notification dropdown-menu" aria-labelledby="notification-dropdown">
							<h3>Notifications</h3>
							<div class="header__notification-list">
								<div class="header__notification-box">
									<a href="#">
										<div class="top"><span>John Smith</span> has invited you to <span>Football Activity</span></div>
										<div class="time">12:00, 12th Nov 2020</div>
									</a>
								</div>
								<div class="header__notification-box">
									<a href="#">
										<div class="top"><span>John Smith</span> has invited you to <span>Football Activity</span></div>
										<div class="time">12:00, 12th Nov 2020</div>
									</a>
								</div>
								<div class="header__notification-box">
									<a href="#">
										<div class="top"><span>John Smith</span> has invited you to <span>Football Activity</span></div>
										<div class="time">12:00, 12th Nov 2020</div>
									</a>
								</div>
							</div>
							<div class="see-all">
								<a href="#" class="btn-custom btn-black">See All Notifications</a>
							</div>
						</div>
					</div>
					<div class="header__action header__action--search">
						<a href="#">
							<img src="assets/images/search-btn-icon.svg">
						</a>
					</div>
					<div class="header__action header__action--user dropdown">
						<a href="#" role="button" class="dropdown-toggle" id="profile-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="Wolfeschlegelsteinhausenbergerdorff">
							<img src="assets/images/client7.jpg">
							<div class="name">Wolfeschlegelsteinhausenbergerdorff</div>
						</a>
						<div class="header__profile dropdown-menu" aria-labelledby="profile-dropdown">
							<div class="header__profile-options">
								<div class="header__profile-option-box">
									<a href="my-profile.php"><i class="fas fa-user"></i>My Profile</a>
								</div>
								<div class="header__profile-option-box">
									<a href="#"><i class="fas fa-cog"></i>Account Settings</a>
								</div>
								<div class="header__profile-option-box">
									<a href="login.php"><i class="fas fa-sign-out-alt"></i>Log Out</a>
								</div>
							</div>
						</div>
					</div>
					<div class="header__action header__action--settings">
						<a href="#">
							<svg xmlns="http://www.w3.org/2000/svg" width="19.984" height="20" viewBox="0 0 19.984 20">
								<g transform="translate(0.003 0.003)"><path class="a" d="M19.731,7.74a.8.8,0,0,0-.1-.184,1.817,1.817,0,0,0-1.505-.7h-.1a.935.935,0,0,1-.8-.492.026.026,0,0,1-.028-.028c-.106-.184-.2-.336-.308-.52a.026.026,0,0,1-.02-.024.967.967,0,0,1-.028-.959.12.12,0,0,0,.05-.078,2,2,0,0,0,.152-1.709.466.466,0,0,0-.1-.184,12.869,12.869,0,0,0-1.071-.908c-.05-.024-.106-.05-.18-.078a1.883,1.883,0,0,0-1.635.442.142.142,0,0,0-.074.05.838.838,0,0,1-.562.2.947.947,0,0,1-.36-.078,4.122,4.122,0,0,0-.586-.2.941.941,0,0,1-.612-.726.5.5,0,0,0-.028-.1,1.837,1.837,0,0,0-.971-1.4.36.36,0,0,0-.178-.048A11.582,11.582,0,0,0,9.3.018a.52.52,0,0,0-.18.05,1.9,1.9,0,0,0-.971,1.423.09.09,0,0,0-.028.078.911.911,0,0,1-.612.726c-.176.05-.382.128-.586.2a1.883,1.883,0,0,1-.36.05.913.913,0,0,1-.562-.2c-.024-.028-.05-.028-.074-.054A1.869,1.869,0,0,0,4.3,1.849a.262.262,0,0,0-.18.078,7.128,7.128,0,0,0-1.073.906.718.718,0,0,0-.1.184,1.929,1.929,0,0,0,.152,1.709.534.534,0,0,1,.05.078.971.971,0,0,1-.028.959.026.026,0,0,0-.028.028,4.084,4.084,0,0,0-.292.52.026.026,0,0,0-.028.028.884.884,0,0,1-.8.492h-.1a1.863,1.863,0,0,0-1.505.7.466.466,0,0,0-.1.184A10.034,10.034,0,0,0,0,9.167a.54.54,0,0,0,.028.2A1.9,1.9,0,0,0,1.2,10.54a.6.6,0,0,1,.1.028.882.882,0,0,1,.586.726v.028a4.362,4.362,0,0,0,.1.6v.028a1,1,0,0,1-.308.906.142.142,0,0,0-.074.05,1.919,1.919,0,0,0-.714,1.479.668.668,0,0,0,.05.2,11.24,11.24,0,0,0,.766,1.323.24.24,0,0,0,.156.156,1.849,1.849,0,0,0,1.6.1.118.118,0,0,0,.1-.028.922.922,0,0,1,.308-.05.935.935,0,0,1,.328.058,1.175,1.175,0,0,1,.308.176l.46.4.028.028a.953.953,0,0,1,.332.878.184.184,0,0,0-.028.1,1.945,1.945,0,0,0,.36,1.579.5.5,0,0,0,.18.128,7.484,7.484,0,0,0,1.459.542.334.334,0,0,0,.23,0A1.861,1.861,0,0,0,8.8,19.018a.534.534,0,0,0,.05-.078.842.842,0,0,1,.82-.464h.64a.915.915,0,0,1,.82.464.534.534,0,0,1,.05.078,1.857,1.857,0,0,0,1.275.959h.1c.028,0,.074,0,.042-.054a13.792,13.792,0,0,0,1.455-.546.472.472,0,0,0,.18-.128,1.779,1.779,0,0,0,.418-1.163,1.9,1.9,0,0,0-.036-.364.324.324,0,0,0-.028-.156.923.923,0,0,1,.36-.854.028.028,0,0,1,.04-.04,3.65,3.65,0,0,0,.46-.4h.028a.9.9,0,0,1,.612-.234.839.839,0,0,1,.222.028.4.4,0,0,1,.086.028.6.6,0,0,1,.1.028,2.109,2.109,0,0,0,.336.094,1.827,1.827,0,0,0,.356.036,1.867,1.867,0,0,0,.921-.234l.156-.156a7.436,7.436,0,0,0,.766-1.323.462.462,0,0,0,.05-.2,1.963,1.963,0,0,0-.714-1.479c-.024-.024-.05-.05-.074-.078a.88.88,0,0,1-.308-.8v-.03a4.468,4.468,0,0,0,.1-.6v-.022a.842.842,0,0,1,.588-.74.118.118,0,0,0,.11-.036,1.875,1.875,0,0,0,1.165-1.163.54.54,0,0,0,.028-.2A9.007,9.007,0,0,0,19.731,7.74ZM9.992,14.212A4.078,4.078,0,1,1,12.884,13,4.1,4.1,0,0,1,9.992,14.212Z" transform="translate(0 0)"/></g>
							</svg>
						</a>
					</div>
					<div class="header__action header__action--logout">
						<a href="#" data-toggle="modal" data-target="#logot-confirmation-popup" data-backdrop="static" data-keyboard="false">
							<svg xmlns="http://www.w3.org/2000/svg" width="20" height="17.634" viewBox="0 0 20 17.634">
								<g transform="translate(198.5 142.86)"><g transform="translate(-198.5 -142.86)"><path class="a" d="M7.642,17.613.716,16.223A.9.9,0,0,1,0,15.339V2.307a.885.885,0,0,1,.716-.884L7.642.013A.941.941,0,0,1,8.379.2.888.888,0,0,1,8.716.9V1.4h3.6a.89.89,0,0,1,.884.884V5.7a.884.884,0,0,1-1.769,0V3.192H8.716V14.413h2.737V11.907a.89.89,0,0,1,.884-.884.864.864,0,0,1,.863.905v3.411a.89.89,0,0,1-.884.884h-3.6v.505a.888.888,0,0,1-.337.695.83.83,0,0,1-.568.211A.525.525,0,0,1,7.642,17.613Zm-2.568-8.8a.947.947,0,1,0,.947-.947A.943.943,0,0,0,5.074,8.813Zm10.8,3.2a.877.877,0,0,1,0-1.263l1.074-1.074h-5.81a.884.884,0,0,1,0-1.768h5.81L15.874,6.834a.893.893,0,0,1,1.263-1.263l2.611,2.611a.916.916,0,0,1,0,1.263l-2.611,2.568a.916.916,0,0,1-1.263,0Z"/></g></g>
							</svg>
						</a>
					</div>
				</div>
				<div class="header__mobile-icon">
					<img src="assets/images/mobile-menu-icon.svg">
				</div>
				<div class="header__mobile-menu-bg"></div>
				<div class="header__mobile-menu">
					<div class="close">×</div>
					<div class="table">
						<div class="table-cell">
							<ul>
								<li><a href="home.php" class="active">
									<svg xmlns="http://www.w3.org/2000/svg" width="25" height="23.606" viewBox="0 0 25 23.606">
										<g transform="translate(-0.073 0.25)"><path class="a" d="M12.914-.111,24.894,9.832a.534.534,0,0,1-.371.926H21.745V22.8a.553.553,0,0,1-.556.556H15.384a.553.553,0,0,1-.556-.556V15.822a.5.5,0,0,0-.494-.556h-3.52a.553.553,0,0,0-.556.556V22.8a.5.5,0,0,1-.494.556H3.959A.553.553,0,0,1,3.4,22.8V10.758H.562a.531.531,0,0,1-.309-.926L12.234-.111A.485.485,0,0,1,12.914-.111Z" transform="translate(0 0)"></path></g>
									</svg>
									Home
								</a></li>
								<li><a href="explore-nearby-activities.php">
									<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 25 25">
										<g transform="translate(0 -0.022)"><path class="a" d="M3.859,25A3.764,3.764,0,0,1,0,21.017V5.518A3.86,3.86,0,0,1,4.5,1.712,3.922,3.922,0,0,1,7.618,4.785h5.107c-.019.209-.03.421-.03.635a6.823,6.823,0,0,0,.051.83H7.716v8.486L13.61,8.841a6.916,6.916,0,0,0,.871,1.2l-1.725,1.725,10.5,10.5a2.378,2.378,0,0,0,.281-1.124V11.6l.41-.86A6.866,6.866,0,0,0,25,9.662v11.48A3.862,3.862,0,0,1,21.143,25Zm6.5-1.465h10.78a2.376,2.376,0,0,0,1.06-.248l-5.8-5.8Zm-8.534-3.66a2.372,2.372,0,0,0,2.03,3.66H8.292l7.08-7.08L11.72,12.8l-4,4v4.335a.732.732,0,1,1-1.465,0,2.393,2.393,0,0,0-4.422-1.267Zm17.09-4.765L16.389,9.8a5.419,5.419,0,1,1,6.384,0l-2.531,5.31a.733.733,0,0,1-1.322,0ZM17.286,5.42a2.295,2.295,0,1,0,2.295-2.295A2.3,2.3,0,0,0,17.286,5.42Z" transform="translate(0 0.022)"></path></g>
									</svg>
									Explore Nearby Activities
								</a></li>
								<li><a href="my-activity-hub.php">
									<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 25 25">
										<g transform="translate(-65 -228)"><path class="a" d="M768.214,255H757.5V242.5h4.464v-.893a1.786,1.786,0,1,1,3.571,0v.893h2.679v2.791a4.054,4.054,0,0,0-.417.136,3.577,3.577,0,0,0-2.244,2.96,3.634,3.634,0,0,0,.038.994A3.574,3.574,0,0,0,767.637,252a3.906,3.906,0,0,0,.409.156c.084.025.084.025.169.048Zm14.286,0H770v-4.464h-.893a1.786,1.786,0,1,1,0-3.571H770V242.5h4.464v.893a1.786,1.786,0,0,0,3.571,0V242.5H782.5Zm-22.209-14.286H757.5V230H770v4.464h.893a1.786,1.786,0,0,1,0,3.571H770v2.679h-2.791a3.794,3.794,0,0,0-.362-.887,3.6,3.6,0,0,0-1.952-1.6,3.688,3.688,0,0,0-1.054-.187c-.091,0-.182,0-.273,0a3.693,3.693,0,0,0-.963.184,3.6,3.6,0,0,0-1.815,1.385,3.773,3.773,0,0,0-.394.771c-.029.083-.029.082-.056.166S760.314,240.629,760.291,240.714Z" transform="translate(-692.5 -2)"></path></g>
									</svg>
									My Activity Hub
									<span class="badge">4</span>
								</a></li>
								<li><a href="tournament-list.php">
									<svg xmlns="http://www.w3.org/2000/svg" width="20" height="25" viewBox="0 0 20 25">
										<defs><style>.tournamentsicon{fill-rule:evenodd;}</style></defs><path class="tournamentsicon" d="M675.167,75.833h-1.25a4.166,4.166,0,0,1-4.167-4.167h-.833A2.917,2.917,0,0,1,666,68.754V63.333a.833.833,0,0,1,.833-.833h2.917V60.413a.414.414,0,0,1,.416-.413h11.668a.416.416,0,0,1,.416.413V62.5h2.917a.833.833,0,0,1,.833.833v5.421a2.917,2.917,0,0,1-2.917,2.913h-.833a4.168,4.168,0,0,1-4.167,4.167h-1.25V80H678.5a2.5,2.5,0,0,1,2.5,2.5V85H671V82.5a2.5,2.5,0,0,1,2.5-2.5h1.669Zm7.083-11.667V70h.833a1.25,1.25,0,0,0,1.25-1.246V64.167Zm-12.5,0V70h-.833a1.25,1.25,0,0,1-1.25-1.246V64.167Z" transform="translate(-666 -60)"></path>
									</svg>
									Tournaments
								</a></li>
								<li><a href="#">
									<svg xmlns="http://www.w3.org/2000/svg" width="20" height="18.32" viewBox="0 0 20 18.32">
										<g transform="translate(-2.354 -5)"><g transform="translate(2.354 5)"><path class="a" d="M7.957,9.4a4.4,4.4,0,1,1,4.4,4.4,4.4,4.4,0,0,1-4.4-4.4ZM22.334,22.418a10.259,10.259,0,0,0-19.961,0,.734.734,0,0,0,.711.9h18.54a.734.734,0,0,0,.711-.9Z" transform="translate(-2.354 -5)"/></g></g>
									</svg>
									My Profile
								</a></li>
								<li><a href="#">
									<svg xmlns="http://www.w3.org/2000/svg" width="19.984" height="20" viewBox="0 0 19.984 20">
										<g transform="translate(0.003 0.003)"><path class="a" d="M19.731,7.74a.8.8,0,0,0-.1-.184,1.817,1.817,0,0,0-1.505-.7h-.1a.935.935,0,0,1-.8-.492.026.026,0,0,1-.028-.028c-.106-.184-.2-.336-.308-.52a.026.026,0,0,1-.02-.024.967.967,0,0,1-.028-.959.12.12,0,0,0,.05-.078,2,2,0,0,0,.152-1.709.466.466,0,0,0-.1-.184,12.869,12.869,0,0,0-1.071-.908c-.05-.024-.106-.05-.18-.078a1.883,1.883,0,0,0-1.635.442.142.142,0,0,0-.074.05.838.838,0,0,1-.562.2.947.947,0,0,1-.36-.078,4.122,4.122,0,0,0-.586-.2.941.941,0,0,1-.612-.726.5.5,0,0,0-.028-.1,1.837,1.837,0,0,0-.971-1.4.36.36,0,0,0-.178-.048A11.582,11.582,0,0,0,9.3.018a.52.52,0,0,0-.18.05,1.9,1.9,0,0,0-.971,1.423.09.09,0,0,0-.028.078.911.911,0,0,1-.612.726c-.176.05-.382.128-.586.2a1.883,1.883,0,0,1-.36.05.913.913,0,0,1-.562-.2c-.024-.028-.05-.028-.074-.054A1.869,1.869,0,0,0,4.3,1.849a.262.262,0,0,0-.18.078,7.128,7.128,0,0,0-1.073.906.718.718,0,0,0-.1.184,1.929,1.929,0,0,0,.152,1.709.534.534,0,0,1,.05.078.971.971,0,0,1-.028.959.026.026,0,0,0-.028.028,4.084,4.084,0,0,0-.292.52.026.026,0,0,0-.028.028.884.884,0,0,1-.8.492h-.1a1.863,1.863,0,0,0-1.505.7.466.466,0,0,0-.1.184A10.034,10.034,0,0,0,0,9.167a.54.54,0,0,0,.028.2A1.9,1.9,0,0,0,1.2,10.54a.6.6,0,0,1,.1.028.882.882,0,0,1,.586.726v.028a4.362,4.362,0,0,0,.1.6v.028a1,1,0,0,1-.308.906.142.142,0,0,0-.074.05,1.919,1.919,0,0,0-.714,1.479.668.668,0,0,0,.05.2,11.24,11.24,0,0,0,.766,1.323.24.24,0,0,0,.156.156,1.849,1.849,0,0,0,1.6.1.118.118,0,0,0,.1-.028.922.922,0,0,1,.308-.05.935.935,0,0,1,.328.058,1.175,1.175,0,0,1,.308.176l.46.4.028.028a.953.953,0,0,1,.332.878.184.184,0,0,0-.028.1,1.945,1.945,0,0,0,.36,1.579.5.5,0,0,0,.18.128,7.484,7.484,0,0,0,1.459.542.334.334,0,0,0,.23,0A1.861,1.861,0,0,0,8.8,19.018a.534.534,0,0,0,.05-.078.842.842,0,0,1,.82-.464h.64a.915.915,0,0,1,.82.464.534.534,0,0,1,.05.078,1.857,1.857,0,0,0,1.275.959h.1c.028,0,.074,0,.042-.054a13.792,13.792,0,0,0,1.455-.546.472.472,0,0,0,.18-.128,1.779,1.779,0,0,0,.418-1.163,1.9,1.9,0,0,0-.036-.364.324.324,0,0,0-.028-.156.923.923,0,0,1,.36-.854.028.028,0,0,1,.04-.04,3.65,3.65,0,0,0,.46-.4h.028a.9.9,0,0,1,.612-.234.839.839,0,0,1,.222.028.4.4,0,0,1,.086.028.6.6,0,0,1,.1.028,2.109,2.109,0,0,0,.336.094,1.827,1.827,0,0,0,.356.036,1.867,1.867,0,0,0,.921-.234l.156-.156a7.436,7.436,0,0,0,.766-1.323.462.462,0,0,0,.05-.2,1.963,1.963,0,0,0-.714-1.479c-.024-.024-.05-.05-.074-.078a.88.88,0,0,1-.308-.8v-.03a4.468,4.468,0,0,0,.1-.6v-.022a.842.842,0,0,1,.588-.74.118.118,0,0,0,.11-.036,1.875,1.875,0,0,0,1.165-1.163.54.54,0,0,0,.028-.2A9.007,9.007,0,0,0,19.731,7.74ZM9.992,14.212A4.078,4.078,0,1,1,12.884,13,4.1,4.1,0,0,1,9.992,14.212Z" transform="translate(0 0)"></path></g>
									</svg>
									Settings
								</a></li>
								<li><a href="#" data-toggle="modal" data-target="#logot-confirmation-popup" data-backdrop="static" data-keyboard="false">
									<svg xmlns="http://www.w3.org/2000/svg" width="20" height="17.634" viewBox="0 0 20 17.634">
										<g transform="translate(198.5 142.86)"><g transform="translate(-198.5 -142.86)"><path class="a" d="M7.642,17.613.716,16.223A.9.9,0,0,1,0,15.339V2.307a.885.885,0,0,1,.716-.884L7.642.013A.941.941,0,0,1,8.379.2.888.888,0,0,1,8.716.9V1.4h3.6a.89.89,0,0,1,.884.884V5.7a.884.884,0,0,1-1.769,0V3.192H8.716V14.413h2.737V11.907a.89.89,0,0,1,.884-.884.864.864,0,0,1,.863.905v3.411a.89.89,0,0,1-.884.884h-3.6v.505a.888.888,0,0,1-.337.695.83.83,0,0,1-.568.211A.525.525,0,0,1,7.642,17.613Zm-2.568-8.8a.947.947,0,1,0,.947-.947A.943.943,0,0,0,5.074,8.813Zm10.8,3.2a.877.877,0,0,1,0-1.263l1.074-1.074h-5.81a.884.884,0,0,1,0-1.768h5.81L15.874,6.834a.893.893,0,0,1,1.263-1.263l2.611,2.611a.916.916,0,0,1,0,1.263l-2.611,2.568a.916.916,0,0,1-1.263,0Z"></path></g></g>
									</svg>
									Logout
								</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>

<div id="content">