</div><!-- ID Content End -->

<footer class="footer">
	<div class="container">
		<div class="row">
			<div class="footer__col footer__col--1">
				<div class="footer__content">
					<h2>SHOUT ME</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed interdum id lectus ac eleifend. Fusce tincidunt erat lectus, eget euismod nibh porta nec. Donec sed felis pellentesque, vulputate sapien vitae, pulvinar lectus. Maecenas vitae vestibulum turpis, sit amet tincidunt sem. Nunc ac nibh at nisi imperdiet pellentesque.</p>
				</div>
			</div>
			<div class="footer__col footer__col--2">
				<div class="footer__contact-box">
					<div class="footer__title">Contact Us</div>
					<ul class="footer__contact-info">
						<li>
							<a href="#">
							<span class="footer__icon footer__icon--email">
								<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20">
									<g transform="translate(-2 -2)"><path class="a" d="M-2141-2325a1,1,0,0,1-1-1v-11.46l10,6.67,10-6.919V-2326a1,1,0,0,1-1,1Zm.3-14h1.7v-5a1,1,0,0,1,1-1h12a1,1,0,0,1,1,1v5h1.35l-8.35,5.791Zm4.7,1a1,1,0,0,0,1,1h6a1,1,0,0,0,1-1,1,1,0,0,0-1-1h-6A1,1,0,0,0-2136-2338Zm0-3a1,1,0,0,0,1,1h6a1,1,0,0,0,1-1,1,1,0,0,0-1-1h-6A1,1,0,0,0-2136-2341Z" transform="translate(2144 2347)"/></g>
								</svg>
							</span>
							contact@email.com</a>
						</li>
						<li>
							<a href="#">
							<span class="footer__icon">
								<svg xmlns="http://www.w3.org/2000/svg" width="10.833" height="20" viewBox="0 0 10.833 20">
									<path class="a" d="M125.25,3.333h2.5a.417.417,0,0,0,.417-.417V.417A.417.417,0,0,0,127.75,0h-2.5a4.588,4.588,0,0,0-4.583,4.583V7.5H117.75a.417.417,0,0,0-.417.417v2.5a.417.417,0,0,0,.417.417h2.917v8.75a.417.417,0,0,0,.417.417h2.5a.417.417,0,0,0,.417-.417v-8.75h2.917a.417.417,0,0,0,.395-.285l.833-2.5a.417.417,0,0,0-.4-.548H124V4.583A1.25,1.25,0,0,1,125.25,3.333Z" transform="translate(-117.333)"/>
								</svg>
							</span>
							Facebook</a>
						</li>
						<li>
							<a href="#">
							<span class="footer__icon">
								<svg xmlns="http://www.w3.org/2000/svg" width="20" height="16.254" viewBox="0 0 20 16.254">
									<path class="a" d="M6.29,16.754A11.6,11.6,0,0,0,17.965,5.079q0-.266-.012-.53A8.349,8.349,0,0,0,20,2.423a8.185,8.185,0,0,1-2.356.646A4.118,4.118,0,0,0,19.448.8a8.232,8.232,0,0,1-2.606,1A4.107,4.107,0,0,0,9.85,5.538,11.65,11.65,0,0,1,1.392,1.251a4.107,4.107,0,0,0,1.27,5.478A4.072,4.072,0,0,1,.8,6.216c0,.017,0,.034,0,.052A4.1,4.1,0,0,0,4.1,10.29a4.1,4.1,0,0,1-1.853.07,4.108,4.108,0,0,0,3.833,2.85,8.232,8.232,0,0,1-5.1,1.756A8.366,8.366,0,0,1,0,14.911a11.616,11.616,0,0,0,6.29,1.843" transform="translate(0 -0.5)"/>
								</svg>
							</span>
							Twitter</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="footer__copyright">
			<p>© 2021 Shout Me Limited. All rights reserved.</p>
		</div>
	</div>
</footer>

</div><!-- ID Wrapper End -->

<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script type="text/javascript" src="assets/js/landing-custom.js"></script>

</body>

</html>