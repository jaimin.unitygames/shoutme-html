<header class="header">
	<div class="container">
		<div class="header__inner d-flex flex-wrap align-items-center justify-content-between box-shadow">
			<div class="header__logo">
				<a href="index.php">SHOUT ME</a>
			</div>
			<div class="header__mobile-menu">
				<span></span>
				<span></span>
				<span></span>
			</div>
			<div class="header__buttons">
				<a href="login.php" class="btn-custom btn-blue">Login</a>
				<a href="register.php" class="btn-custom btn-black">Register</a>
			</div>
		</div>
	</div>
</header>

<div id="content">