</div><!-- ID Content End -->

<footer class="footer">

</footer>

<!-- Logout Confirmation popup -->
<div class="modal fade logot-confirmation-popup" id="logot-confirmation-popup" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<div class="logout-confirmation-box">
					<div class="text">Are you sure you want to logout now?</div>
					<form action="index.php">
						<div class="form-block d-flex align-items-center justify-content-between">
							<div class="form-group text-center mb-10">
								<button type="submit" class="btn-custom d-inline-block fw-600">Ok</button>
							</div>
							<div class="form-group text-center mb-10">
								<a data-dismiss="modal" class="btn-custom btn-black d-inline-block fw-600">Cancel</a>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- End Logout Confirmation popup -->

</div><!-- ID Wrapper End -->

<script type="text/javascript" src="assets/js/jquery-3.6.0.min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap-min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap-multiselect-min.js"></script>
<script type="text/javascript" src="assets/js/swiper-min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap-moment-with-locales-min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap-datetime-picker-min.js"></script>
<script type="text/javascript" src="assets/js/autosize-textarea-min.js"></script>
<script type="text/javascript" src="assets/js/scrolltofixed.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" src="assets/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.ui.touch-punch.min.js"></script>
<script type="text/javascript" src="assets/js/custom.js"></script>

</body>

</html>