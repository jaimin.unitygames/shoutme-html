<?php include 'include/head.php'; ?>

<?php include 'include/header.php'; ?>

<div class="account">
	<div class="container">
		<div class="row">
			<div class="col-xl-3 col-md-4">
				<div class="account-sidebar-wrap">
					<div class="account-menu">
						Account Settings <i class="fas fa-bars"></i>
					</div>
					<div class="account-sidebar bg-white">
						<div class="close">×</div>
						<ul class="account-sidebar__links">
							<li>
								<a href="account-verify-mobile.php"><i class="fas fa-mobile-alt"></i>Verify Mobile Number</a>
							</li>
							<li>
								<a href="account-favourite-post.php"><i class="fas fa-heart"></i>Favorite Post</a>
							</li>
							<li>
								<a href="account-saved-post.php"><i class="fas fa-bookmark"></i>Saved Post</a>
							</li>
							<li>
								<a href="account-transaction-history.php"><i class="fas fa-exchange-alt"></i>Transaction History</a>
							</li>
							<li>
								<a href="account-change-password.php"><i class="fas fa-lock"></i>Change Password</a>
							</li>
							<li>
								<a href="account-terms-conditions.php"><i class="fas fa-clipboard-list"></i>Terms & Conditions</a>
							</li>
							<li>
								<a href="account-privacy-policy.php"><i class="fas fa-mobile-alt"></i>Privacy Policy</a>
							</li>
							<li>
								<a href="account-faq.php" class="active"><i class="fas fa-comments"></i>FAQs</a>
							</li>
							<li>
								<a href="account-feedback.php"><i class="fas fa-thumbs-up"></i>Feedback</a>
							</li>
							<li>
								<a href="account-delete.php"><i class="fas fa-trash"></i>Delete My Account</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-xl-9 col-md-8">
				<div class="account-settings">
					<div class="white-title">
						<h3>FAQs</h3>
					</div>
					<div class="faq">
						<div id="account-faq">
							<div class="card">
								<div class="card-header" id="faq1">
									<a class="btn btn-link" data-toggle="collapse" href="#faqcollapse1" role="button" aria-expanded="true" aria-controls="faqcollapse1">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent in ultrices velit, nec venenatis ipsum.</a>
								</div>

								<div id="faqcollapse1" class="collapse show" aria-labelledby="faq1" data-parent="#account-faq">
									<div class="card-body">
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent in ultrices velit, nec venenatis ipsum. Nunc rutrum nibh massa, sit amet congue justo</p>
									</div>
								</div>
							</div>
							<div class="card">
								<div class="card-header" id="faq2">
									<a class="btn btn-link collapsed" data-toggle="collapse" href="#faqcollapse2" role="button" aria-expanded="false" aria-controls="faqcollapse2">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent in ultrices velit, nec venenatis ipsum.</a>
								</div>
								<div id="faqcollapse2" class="collapse" aria-labelledby="faq2" data-parent="#account-faq">
									<div class="card-body">
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent in ultrices velit, nec venenatis ipsum. Nunc rutrum nibh massa, sit amet congue justo</p>
									</div>
								</div>
							</div>
							<div class="card">
								<div class="card-header" id="faq3">
									<a class="btn btn-link collapsed" data-toggle="collapse" href="#faqcollapse3" role="button" aria-expanded="false" aria-controls="faqcollapse3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent in ultrices velit, nec venenatis ipsum.</a>
								</div>
								<div id="faqcollapse3" class="collapse" aria-labelledby="faq3" data-parent="#account-faq">
									<div class="card-body">
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent in ultrices velit, nec venenatis ipsum. Nunc rutrum nibh massa, sit amet congue justo</p>
									</div>
								</div>
							</div>
							<div class="card">
								<div class="card-header" id="faq4">
									<a class="btn btn-link collapsed" data-toggle="collapse" href="#faqcollapse4" role="button" aria-expanded="false" aria-controls="faqcollapse4">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent in ultrices velit, nec venenatis ipsum.</a>
								</div>
								<div id="faqcollapse4" class="collapse" aria-labelledby="faq4" data-parent="#account-faq">
									<div class="card-body">
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent in ultrices velit, nec venenatis ipsum. Nunc rutrum nibh massa, sit amet congue justo</p>
									</div>
								</div>
							</div>
							<div class="card">
								<div class="card-header" id="faq5">
									<a class="btn btn-link collapsed" data-toggle="collapse" href="#faqcollapse5" role="button" aria-expanded="false" aria-controls="faqcollapse5">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent in ultrices velit, nec venenatis ipsum.</a>
								</div>
								<div id="faqcollapse5" class="collapse" aria-labelledby="faq5" data-parent="#account-faq">
									<div class="card-body">
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent in ultrices velit, nec venenatis ipsum. Nunc rutrum nibh massa, sit amet congue justo</p>
									</div>
								</div>
							</div>
							<div class="card">
								<div class="card-header" id="faq6">
									<a class="btn btn-link collapsed" data-toggle="collapse" href="#faqcollapse6" role="button" aria-expanded="false" aria-controls="faqcollapse6">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent in ultrices velit, nec venenatis ipsum.</a>
								</div>
								<div id="faqcollapse6" class="collapse" aria-labelledby="faq6" data-parent="#account-faq">
									<div class="card-body">
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent in ultrices velit, nec venenatis ipsum. Nunc rutrum nibh massa, sit amet congue justo</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php include 'include/footer.php'; ?>