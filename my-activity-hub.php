<?php include 'include/head.php'; ?>

<?php include 'include/header.php'; ?>

<div class="my-activity">
	<div class="container">
		<div class="row">
			<div class="col-lg-9 col-md-8 order-2 order-md-1">
				<nav>
					<div class="nav nav-tabs" id="nav-tab" role="tablist">
						<a class="nav-link active" id="joined-activity-tab" data-toggle="tab" href="#joined-activity" role="tab" aria-controls="joined-activity" aria-selected="true">Joined Activities</a>
						<a class="nav-link" id="created-activity-tab" data-toggle="tab" href="#created-activity" role="tab" aria-controls="created-activity" aria-selected="false">Created Activities</a>
						<a class="nav-link" id="requested-activity-tab" data-toggle="tab" href="#requested-activity" role="tab" aria-controls="requested-activity" aria-selected="false">New Requests <span class="badge">4</span></a>
					</div>
				</nav>
			</div>
			<div class="col-lg-3 col-md-4 order-1 order-md-2">
				<div class="create-activity text-center text-md-left">
					<a href="create-activity.php" class="btn-custom btn-blue-gradient">Create New Activity</a>
				</div>
			</div>
		</div>
		<div class="tab-content" id="nav-tabContent">
			<div class="tab-pane fade show active" id="joined-activity" role="tabpanel" aria-labelledby="joined-activity-tab">
				<div class="row">
					<div class="col-md-6 col-lg-4">
						<div class="my-activity__box">
							<a href="#">
								<div class="my-activity__box-top">
									<div class="d-flex align-items-center">
										<img src="assets/images/football-ball-icon.svg" alt="">
										Football
									</div>
								</div>
								<div class="my-activity__box-center">
									<div class="d-flex align-items-center">
										<img src="assets/images/timer-icon.svg">
										12:00, 12th Nov 2020 (2 Hours)
									</div>
									<div class="d-flex align-items-center">
										<img src="assets/images/yellow-location-icon.svg">
										Public Ground, London
									</div>
								</div>
								<div class="my-activity__box-bottom">
									<div class="left">
										<div class="box green">
											<img src="assets/images/money-icon.svg">£10
										</div>
										<div class="box blue">
											<img src="assets/images/users-icon.svg">08/12
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>
					<div class="col-md-6 col-lg-4">
						<div class="my-activity__box">
							<a href="#">
								<div class="my-activity__box-top">
									<div class="d-flex align-items-center">
										<img src="assets/images/football-ball-icon.svg" alt="">
										Football
									</div>
								</div>
								<div class="my-activity__box-center">
									<div class="d-flex align-items-center">
										<img src="assets/images/timer-icon.svg">
										12:00, 12th Nov 2020 (2 Hours)
									</div>
									<div class="d-flex align-items-center">
										<img src="assets/images/yellow-location-icon.svg">
										Public Ground, London
									</div>
								</div>
								<div class="my-activity__box-bottom">
									<div class="left">
										<div class="box green">
											<img src="assets/images/money-icon.svg">£10
										</div>
										<div class="box blue">
											<img src="assets/images/users-icon.svg">08/12
										</div>
									</div>
									<div class="right">
										<div class="btn-custom btn-black-light">Expired</div>
									</div>
								</div>
							</a>
						</div>
					</div>
					<div class="col-md-6 col-lg-4">
						<div class="my-activity__box">
							<a href="#">
								<div class="my-activity__box-top">
									<div class="d-flex align-items-center">
										<img src="assets/images/football-ball-icon.svg" alt="">
										Football
									</div>
								</div>
								<div class="my-activity__box-center">
									<div class="d-flex align-items-center">
										<img src="assets/images/timer-icon.svg">
										12:00, 12th Nov 2020 (2 Hours)
									</div>
									<div class="d-flex align-items-center">
										<img src="assets/images/yellow-location-icon.svg">
										Public Ground, London
									</div>
								</div>
								<div class="my-activity__box-bottom">
									<div class="left">
										<div class="box green">
											<img src="assets/images/money-icon.svg">£10
										</div>
										<div class="box blue">
											<img src="assets/images/users-icon.svg">08/12
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>
					<div class="col-md-6 col-lg-4">
						<div class="my-activity__box">
							<a href="#">
								<div class="my-activity__box-top">
									<div class="d-flex align-items-center">
										<img src="assets/images/football-ball-icon.svg" alt="">
										Football
									</div>
								</div>
								<div class="my-activity__box-center">
									<div class="d-flex align-items-center">
										<img src="assets/images/timer-icon.svg">
										12:00, 12th Nov 2020 (2 Hours)
									</div>
									<div class="d-flex align-items-center">
										<img src="assets/images/yellow-location-icon.svg">
										Public Ground, London
									</div>
								</div>
								<div class="my-activity__box-bottom">
									<div class="left">
										<div class="box green">
											<img src="assets/images/money-icon.svg">£10
										</div>
										<div class="box blue">
											<img src="assets/images/users-icon.svg">08/12
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>
					<div class="col-md-6 col-lg-4">
						<div class="my-activity__box">
							<a href="#">
								<div class="my-activity__box-top">
									<div class="d-flex align-items-center">
										<img src="assets/images/football-ball-icon.svg" alt="">
										Football
									</div>
								</div>
								<div class="my-activity__box-center">
									<div class="d-flex align-items-center">
										<img src="assets/images/timer-icon.svg">
										12:00, 12th Nov 2020 (2 Hours)
									</div>
									<div class="d-flex align-items-center">
										<img src="assets/images/yellow-location-icon.svg">
										Public Ground, London
									</div>
								</div>
								<div class="my-activity__box-bottom">
									<div class="left">
										<div class="box green">
											<img src="assets/images/money-icon.svg">£10
										</div>
										<div class="box blue">
											<img src="assets/images/users-icon.svg">08/12
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>
					<div class="col-md-6 col-lg-4">
						<div class="my-activity__box">
							<a href="#">
								<div class="my-activity__box-top">
									<div class="d-flex align-items-center">
										<img src="assets/images/football-ball-icon.svg" alt="">
										Football
									</div>
								</div>
								<div class="my-activity__box-center">
									<div class="d-flex align-items-center">
										<img src="assets/images/timer-icon.svg">
										12:00, 12th Nov 2020 (2 Hours)
									</div>
									<div class="d-flex align-items-center">
										<img src="assets/images/yellow-location-icon.svg">
										Public Ground, London
									</div>
								</div>
								<div class="my-activity__box-bottom">
									<div class="left">
										<div class="box green">
											<img src="assets/images/money-icon.svg">£10
										</div>
										<div class="box blue">
											<img src="assets/images/users-icon.svg">08/12
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>
				</div>
			</div>
			<div class="tab-pane fade" id="created-activity" role="tabpanel" aria-labelledby="created-activity-tab">
				<div class="row">
					<div class="col-md-6 col-lg-4">
						<div class="my-activity__box">
							<a href="#">
								<div class="my-activity__box-top">
									<div class="d-flex align-items-center">
										<img src="assets/images/football-ball-icon.svg" alt="">
										Football
									</div>
								</div>
								<div class="my-activity__box-center">
									<div class="d-flex align-items-center">
										<img src="assets/images/timer-icon.svg">
										12:00, 12th Nov 2020 (2 Hours)
									</div>
									<div class="d-flex align-items-center">
										<img src="assets/images/yellow-location-icon.svg">
										Public Ground, London
									</div>
								</div>
								<div class="my-activity__box-bottom">
									<div class="left">
										<div class="box green">
											<img src="assets/images/money-icon.svg">£10
										</div>
										<div class="box blue">
											<img src="assets/images/users-icon.svg">08/12
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>
					<div class="col-md-6 col-lg-4">
						<div class="my-activity__box">
							<a href="#">
								<div class="my-activity__box-top">
									<div class="d-flex align-items-center">
										<img src="assets/images/football-ball-icon.svg" alt="">
										Football
									</div>
								</div>
								<div class="my-activity__box-center">
									<div class="d-flex align-items-center">
										<img src="assets/images/timer-icon.svg">
										12:00, 12th Nov 2020 (2 Hours)
									</div>
									<div class="d-flex align-items-center">
										<img src="assets/images/yellow-location-icon.svg">
										Public Ground, London
									</div>
								</div>
								<div class="my-activity__box-bottom">
									<div class="left">
										<div class="box green">
											<img src="assets/images/money-icon.svg">£10
										</div>
										<div class="box blue">
											<img src="assets/images/users-icon.svg">08/12
										</div>
									</div>
									<div class="right">
										<div class="btn-custom btn-black-light">Expired</div>
									</div>
								</div>
							</a>
						</div>
					</div>
					<div class="col-md-6 col-lg-4">
						<div class="my-activity__box">
							<a href="#">
								<div class="my-activity__box-top">
									<div class="d-flex align-items-center">
										<img src="assets/images/football-ball-icon.svg" alt="">
										Football
									</div>
								</div>
								<div class="my-activity__box-center">
									<div class="d-flex align-items-center">
										<img src="assets/images/timer-icon.svg">
										12:00, 12th Nov 2020 (2 Hours)
									</div>
									<div class="d-flex align-items-center">
										<img src="assets/images/yellow-location-icon.svg">
										Public Ground, London
									</div>
								</div>
								<div class="my-activity__box-bottom">
									<div class="left">
										<div class="box green">
											<img src="assets/images/money-icon.svg">£10
										</div>
										<div class="box blue">
											<img src="assets/images/users-icon.svg">08/12
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>
					<div class="col-md-6 col-lg-4">
						<div class="my-activity__box">
							<a href="#">
								<div class="my-activity__box-top">
									<div class="d-flex align-items-center">
										<img src="assets/images/football-ball-icon.svg" alt="">
										Football
									</div>
								</div>
								<div class="my-activity__box-center">
									<div class="d-flex align-items-center">
										<img src="assets/images/timer-icon.svg">
										12:00, 12th Nov 2020 (2 Hours)
									</div>
									<div class="d-flex align-items-center">
										<img src="assets/images/yellow-location-icon.svg">
										Public Ground, London
									</div>
								</div>
								<div class="my-activity__box-bottom">
									<div class="left">
										<div class="box green">
											<img src="assets/images/money-icon.svg">£10
										</div>
										<div class="box blue">
											<img src="assets/images/users-icon.svg">08/12
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>
					<div class="col-md-6 col-lg-4">
						<div class="my-activity__box">
							<a href="#">
								<div class="my-activity__box-top">
									<div class="d-flex align-items-center">
										<img src="assets/images/football-ball-icon.svg" alt="">
										Football
									</div>
								</div>
								<div class="my-activity__box-center">
									<div class="d-flex align-items-center">
										<img src="assets/images/timer-icon.svg">
										12:00, 12th Nov 2020 (2 Hours)
									</div>
									<div class="d-flex align-items-center">
										<img src="assets/images/yellow-location-icon.svg">
										Public Ground, London
									</div>
								</div>
								<div class="my-activity__box-bottom">
									<div class="left">
										<div class="box green">
											<img src="assets/images/money-icon.svg">£10
										</div>
										<div class="box blue">
											<img src="assets/images/users-icon.svg">08/12
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>
					<div class="col-md-6 col-lg-4">
						<div class="my-activity__box">
							<a href="#">
								<div class="my-activity__box-top">
									<div class="d-flex align-items-center">
										<img src="assets/images/football-ball-icon.svg" alt="">
										Football
									</div>
								</div>
								<div class="my-activity__box-center">
									<div class="d-flex align-items-center">
										<img src="assets/images/timer-icon.svg">
										12:00, 12th Nov 2020 (2 Hours)
									</div>
									<div class="d-flex align-items-center">
										<img src="assets/images/yellow-location-icon.svg">
										Public Ground, London
									</div>
								</div>
								<div class="my-activity__box-bottom">
									<div class="left">
										<div class="box green">
											<img src="assets/images/money-icon.svg">£10
										</div>
										<div class="box blue">
											<img src="assets/images/users-icon.svg">08/12
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>
				</div>
			</div>
			<div class="tab-pane fade" id="requested-activity" role="tabpanel" aria-labelledby="requested-activity-tab">
				<div class="row">
					<div class="col-md-6 col-lg-4">
						<div class="my-activity__box">
							<a href="#">
								<div class="my-activity__box-top">
									<div class="d-flex align-items-center">
										<img src="assets/images/football-ball-icon.svg" alt="">
										Football
									</div>
								</div>
								<div class="my-activity__box-center">
									<div class="d-flex align-items-center">
										<img src="assets/images/timer-icon.svg">
										12:00, 12th Nov 2020 (2 Hours)
									</div>
									<div class="d-flex align-items-center">
										<img src="assets/images/yellow-location-icon.svg">
										Public Ground, London
									</div>
								</div>
								<div class="my-activity__box-bottom">
									<div class="left">
										<div class="box green">
											<img src="assets/images/money-icon.svg">£10
										</div>
										<div class="box blue">
											<img src="assets/images/users-icon.svg">08/12
										</div>
									</div>
								</div>
								<div class="buttons d-flex align-items-center justify-content-between">
									<button class="btn-custom">Accept</button>
									<button class="btn-custom btn-black-light">Reject</button>
								</div>
							</a>
						</div>
					</div>
					<div class="col-md-6 col-lg-4">
						<div class="my-activity__box">
							<a href="#">
								<div class="my-activity__box-top">
									<div class="d-flex align-items-center">
										<img src="assets/images/football-ball-icon.svg" alt="">
										Football
									</div>
								</div>
								<div class="my-activity__box-center">
									<div class="d-flex align-items-center">
										<img src="assets/images/timer-icon.svg">
										12:00, 12th Nov 2020 (2 Hours)
									</div>
									<div class="d-flex align-items-center">
										<img src="assets/images/yellow-location-icon.svg">
										Public Ground, London
									</div>
								</div>
								<div class="my-activity__box-bottom">
									<div class="left">
										<div class="box green">
											<img src="assets/images/money-icon.svg">£10
										</div>
										<div class="box blue">
											<img src="assets/images/users-icon.svg">08/12
										</div>
									</div>
									<div class="right">
										<div class="btn-custom btn-black-light">Expired</div>
									</div>
								</div>
								<div class="buttons d-flex align-items-center justify-content-between">
									<button class="btn-custom">Accept</button>
									<button class="btn-custom btn-black-light">Reject</button>
								</div>
							</a>
						</div>
					</div>
					<div class="col-md-6 col-lg-4">
						<div class="my-activity__box">
							<a href="#">
								<div class="my-activity__box-top">
									<div class="d-flex align-items-center">
										<img src="assets/images/football-ball-icon.svg" alt="">
										Football
									</div>
								</div>
								<div class="my-activity__box-center">
									<div class="d-flex align-items-center">
										<img src="assets/images/timer-icon.svg">
										12:00, 12th Nov 2020 (2 Hours)
									</div>
									<div class="d-flex align-items-center">
										<img src="assets/images/yellow-location-icon.svg">
										Public Ground, London
									</div>
								</div>
								<div class="my-activity__box-bottom">
									<div class="left">
										<div class="box green">
											<img src="assets/images/money-icon.svg">£10
										</div>
										<div class="box blue">
											<img src="assets/images/users-icon.svg">08/12
										</div>
									</div>
								</div>
								<div class="buttons d-flex align-items-center justify-content-between">
									<button class="btn-custom">Accept</button>
									<button class="btn-custom btn-black-light">Reject</button>
								</div>
							</a>
						</div>
					</div>
					<div class="col-md-6 col-lg-4">
						<div class="my-activity__box">
							<a href="#">
								<div class="my-activity__box-top">
									<div class="d-flex align-items-center">
										<img src="assets/images/football-ball-icon.svg" alt="">
										Football
									</div>
								</div>
								<div class="my-activity__box-center">
									<div class="d-flex align-items-center">
										<img src="assets/images/timer-icon.svg">
										12:00, 12th Nov 2020 (2 Hours)
									</div>
									<div class="d-flex align-items-center">
										<img src="assets/images/yellow-location-icon.svg">
										Public Ground, London
									</div>
								</div>
								<div class="my-activity__box-bottom">
									<div class="left">
										<div class="box green">
											<img src="assets/images/money-icon.svg">£10
										</div>
										<div class="box blue">
											<img src="assets/images/users-icon.svg">08/12
										</div>
									</div>
								</div>
								<div class="buttons d-flex align-items-center justify-content-between">
									<button class="btn-custom">Accept</button>
									<button class="btn-custom btn-black-light">Reject</button>
								</div>
							</a>
						</div>
					</div>
					<div class="col-md-6 col-lg-4">
						<div class="my-activity__box">
							<a href="#">
								<div class="my-activity__box-top">
									<div class="d-flex align-items-center">
										<img src="assets/images/football-ball-icon.svg" alt="">
										Football
									</div>
								</div>
								<div class="my-activity__box-center">
									<div class="d-flex align-items-center">
										<img src="assets/images/timer-icon.svg">
										12:00, 12th Nov 2020 (2 Hours)
									</div>
									<div class="d-flex align-items-center">
										<img src="assets/images/yellow-location-icon.svg">
										Public Ground, London
									</div>
								</div>
								<div class="my-activity__box-bottom">
									<div class="left">
										<div class="box green">
											<img src="assets/images/money-icon.svg">£10
										</div>
										<div class="box blue">
											<img src="assets/images/users-icon.svg">08/12
										</div>
									</div>
								</div>
								<div class="buttons d-flex align-items-center justify-content-between">
									<button class="btn-custom">Accept</button>
									<button class="btn-custom btn-black-light">Reject</button>
								</div>
							</a>
						</div>
					</div>
					<div class="col-md-6 col-lg-4">
						<div class="my-activity__box">
							<a href="#">
								<div class="my-activity__box-top">
									<div class="d-flex align-items-center">
										<img src="assets/images/football-ball-icon.svg" alt="">
										Football
									</div>
								</div>
								<div class="my-activity__box-center">
									<div class="d-flex align-items-center">
										<img src="assets/images/timer-icon.svg">
										12:00, 12th Nov 2020 (2 Hours)
									</div>
									<div class="d-flex align-items-center">
										<img src="assets/images/yellow-location-icon.svg">
										Public Ground, London
									</div>
								</div>
								<div class="my-activity__box-bottom">
									<div class="left">
										<div class="box green">
											<img src="assets/images/money-icon.svg">£10
										</div>
										<div class="box blue">
											<img src="assets/images/users-icon.svg">08/12
										</div>
									</div>
								</div>
								<div class="buttons d-flex align-items-center justify-content-between">
									<button class="btn-custom">Accept</button>
									<button class="btn-custom btn-black-light">Reject</button>
								</div>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php include 'include/footer.php'; ?>