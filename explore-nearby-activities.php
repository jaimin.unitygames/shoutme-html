<?php include 'include/head.php';?>

<?php include 'include/header.php';?>

<div class="activity-map">
	<div id="map-canvas"></div>
	<div class="activity-filter-icon">
		<img src="assets/images/filter-icon.svg">
		Filter
	</div>
	<div class="filter-sidebar">
		<img class="close" src="assets/images/close-icon.svg">
		<a class="clearall" href="#">Clear All</a>
		<div class="top">
			<h3>Filter</h3>
		</div>
		<div class="filter-sidebar__form">
			<div class="form-group">
				<div class="label-main">Activity</div>
				<label>Select Date</label>
				<input type="text" class="form-control datepicker" placeholder="dd/mm/yyyy">
			</div>
			<div class="form-group">
				<div class="label-main">Participant</div>
				<label>Participante</label>
				<input type="text" class="form-control datepicker" placeholder="dd/mm/yyyy">
			</div>
			<div class="form-group time-available">
				<label>Time Availability</label>
				<div class="input-group d-flex flex-wrap align-items-center justify-content-between">
					<input type="text" class="timepicker form-control" placeholder="hh:mm">
					<span>to</span>
					<input type="text" class="timepicker form-control" placeholder="hh:mm">
				</div>
			</div>
			<div class="form-group select-age">
				<label>Select Age</label>
				<div class="input-group d-flex flex-wrap align-items-center justify-content-between">
					<select class="form-control">
						<option value="selected">From</option>
						<option value="10">10</option>
						<option value="11">11</option>
						<option value="12">12</option>
						<option value="13">13</option>
						<option value="14">14</option>
						<option value="15">15</option>
						<option value="16">16</option>
						<option value="17">17</option>
						<option value="18">18</option>
						<option value="19">19</option>
						<option value="20">20</option>
						<option value="21">21</option>
						<option value="22">22</option>
						<option value="23">23</option>
						<option value="24">24</option>
						<option value="25">25</option>
						<option value="26">26</option>
						<option value="27">27</option>
						<option value="28">28</option>
						<option value="29">29</option>
						<option value="30">30</option>
						<option value="31">31</option>
						<option value="32">32</option>
						<option value="33">33</option>
						<option value="34">34</option>
						<option value="35">35</option>
						<option value="36">36</option>
						<option value="37">37</option>
						<option value="38">38</option>
						<option value="39">39</option>
						<option value="40">40</option>
					</select>
					<span>to</span>
					<select class="form-control">
						<option value="selected">To</option>
						<option value="10">10</option>
						<option value="11">11</option>
						<option value="12">12</option>
						<option value="13">13</option>
						<option value="14">14</option>
						<option value="15">15</option>
						<option value="16">16</option>
						<option value="17">17</option>
						<option value="18">18</option>
						<option value="19">19</option>
						<option value="20">20</option>
						<option value="21">21</option>
						<option value="22">22</option>
						<option value="23">23</option>
						<option value="24">24</option>
						<option value="25">25</option>
						<option value="26">26</option>
						<option value="27">27</option>
						<option value="28">28</option>
						<option value="29">29</option>
						<option value="30">30</option>
						<option value="31">31</option>
						<option value="32">32</option>
						<option value="33">33</option>
						<option value="34">34</option>
						<option value="35">35</option>
						<option value="36">36</option>
						<option value="37">37</option>
						<option value="38">38</option>
						<option value="39">39</option>
						<option value="40">40</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label>Gender</label>
				<div class="input-group d-flex flex-wrap radio-custom">
					<div>
						<input type="radio" id="any" name="gender" value="any" checked="true">
						<label for="any">Any</label>
					</div>
					<div>
						<input type="radio" id="male" name="gender" value="male">
						<label for="male">Male</label>
					</div>
					<div>
						<input type="radio" id="female" name="gender" value="female">
						<label for="female">Female</label>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label>Interested Activities</label>
				<div class="multiple-select">
					<div class="individual-activity">
						<input type="checkbox" id="football" name="activity" value="football">
						<label for="football">Football</label>
					</div>
					<div class="individual-activity">
						<input type="checkbox" id="basketball" name="activity" value="basketball">
						<label for="basketball">Basketball</label>
					</div>
					<div class="individual-activity">
						<input type="checkbox" id="hockey" name="activity" value="hockey">
						<label for="hockey">Hockey</label>
					</div>
					<div class="individual-activity">
						<input type="checkbox" id="rugby" name="activity" value="rugby">
						<label for="rugby">Rugby</label>
					</div>
					<div class="individual-activity">
						<input type="checkbox" id="cricket" name="activity" value="cricket">
						<label for="cricket">Cricket</label>
					</div>
					<div class="individual-activity">
						<input type="checkbox" id="tennis" name="activity" value="tennis">
						<label for="tennis">Tennis</label>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label>Skill Level</label>
				<div class="input-group d-flex flex-wrap radio-custom skill">
					<div>
						<input type="radio" id="all" name="skilllevel" value="all" checked="true">
						<label for="all">All</label>
					</div>
					<div>
						<input type="radio" id="beginner" name="skilllevel" value="beginner">
						<label for="beginner">Beginner</label>
					</div>
					<div>
						<input type="radio" id="intermediate" name="skilllevel" value="intermediate">
						<label for="intermediate">Intermediate</label>
					</div>
					<div>
						<input type="radio" id="expert" name="skilllevel" value="expert">
						<label for="expert">Expert</label>
					</div>
				</div>
			</div>
			<div class="form-group">
				<button type="submit" class="btn-custom btn-black">Save <img src="assets/images/arrow.svg"></button>
			</div>
		</div>
	</div>
</div>

<?php include 'include/footer.php';?>

<script>
	var map;
	var lat_array = [51.5136816,
	51.4939958, 51.4583125];
	var lon_array = [0.1553043,
	0.0969054, 0.1293862];


	function initialize() {
		
        // The custom tooltip class
        // Constructor function
        function Tooltip(opts, marker)
        {
            // Initialization
            this.setValues(opts);
            this.map_ = opts.map;
            this.marker_ = marker;
            var div = this.div_ = document.createElement("div");
            // Class name of div element to style it via CSS
            div.className = "tooltip";
            this.markerDragging = false;
        }

        Tooltip.prototype =
        {
            // Define draw method to keep OverlayView happy
            draw: function() {},
            visible_changed: function()
            {
            	var vis = this.get("visible");
            	this.div_.style.visibility  = vis ? "visible" : "hidden";
            },

            getPos: function(e)
            {
            	var projection = this.getProjection();
                // Position of mouse cursor
                var pixel = projection.fromLatLngToDivPixel(e.latLng);
                var div = this.div_;

                // Adjust the tooltip's position
                var gap = 15;
                var posX = pixel.x + gap;
                var posY = pixel.y + gap;

                var menuwidth = div.offsetWidth;
                // Right boundary of the map
                var boundsNE = this.map_.getBounds().getNorthEast();
                boundsNE.pixel = projection.fromLatLngToDivPixel(boundsNE);

                if (menuwidth + posX > boundsNE.pixel.x)
                {
                	posX -= menuwidth + gap;
                }
                div.style.left = posX + "px";
                div.style.top = posY + "px";

                if (!this.markerDragging)
                {
                	this.set("visible", true);
                }
            },

            getPos2: function(latLng)
            {	// This is added to avoid using listener (Listener is not working when Map is quickly loaded with icons)
            	var projection = this.getProjection();
                // Position of mouse cursor
                var pixel = projection.fromLatLngToDivPixel(latLng);
                var div = this.div_;

                // Adjust the tooltip's position
                var gap = 5;
                var posX = pixel.x + gap;
                var posY = pixel.y + gap;

                var menuwidth = div.offsetWidth;
                // Right boundary of the map
                var boundsNE = this.map_.getBounds().getNorthEast();
                boundsNE.pixel = projection.fromLatLngToDivPixel(boundsNE);

                if (menuwidth + posX > boundsNE.pixel.x)
                {
                	posX -= menuwidth + gap;
                }
                div.style.left = posX + "px";
                div.style.top = posY + "px";

                if (!this.markerDragging)
                {
                	this.set("visible", true);
                }
            },

            addTip: function()
            {
            	var me = this;
            	var g = google.maps.event;
            	var div = me.div_;
            	div.innerHTML = me.get("text").toString();
                // Tooltip is initially hidden
                me.set("visible", false);
                // Append the tooltip's div to the floatPane
                me.getPanes().floatPane.appendChild(this.div_);
                // In IE this listener gets randomly lost after it's been cleared once.
                // So keep it out of the listeners array.
                g.addListener(me.marker_, "dragend", function()
                {
                	me.markerDragging = false; });

                // Register listeners
                me.listeners = [
                //   g.addListener(me.marker_, "dragend", function() {
                //    me.markerDragging = false; }),
                g.addListener(me.marker_, "position_changed", function() {
                	me.markerDragging = true;
                	me.set("visible", false); }),
                g.addListener(me.map_, "mousemove", function(e) {
                	me.getPos(e); })
                ];
            },

            removeTip: function()
            {
                // Clear the listeners to stop events when not needed.
                if (this.listeners)
                {
                	for (var i = 0, listener; listener = this.listeners[i]; i++)
                	{
                		google.maps.event.removeListener(listener);
                	}
                	delete this.listeners;
                }
                // Remove the tooltip from the map pane.
                var parent = this.div_.parentNode;
                if (parent) parent.removeChild(this.div_);
            }
        };


        function inherit(addTo, getFrom)
        {
            var from = getFrom.prototype;  // prototype object to get methods from
            var to = addTo.prototype;      // prototype object to add methods to
            for (var prop in from)
            {
            	if (typeof to[prop] == "undefined") to[prop] = from[prop];
            }
        }

        // Inherits from OverlayView from the Google Maps API
        inherit(Tooltip, google.maps.OverlayView);
        var bounds = new google.maps.LatLngBounds();
        var mapOptions = {
        	zoom: 10,
        	center: new google.maps.LatLng(7.170000076293945, 79.87000274658203)
        };
        map = new google.maps.Map(document.getElementById('map-canvas'),
        	mapOptions);
        var count_total = lat_array.length;
        for (i = 0; i < count_total; i++) {
        	(function(){
        		var myLatlng = new google.maps.LatLng(lat_array[i], lon_array[i]);
        		bounds.extend(myLatlng);
        		var image = 'http://45.79.169.241/shoutmehtml/assets/images/map-marker-icon.svg';
        		var marker = new google.maps.Marker({
        			position: myLatlng,
        			map: map,
        			title: 'I am here',
        			icon: image,
        			tooltip: '<B>This is a customized tooltip</B>'
        		});
        		var contentString = '<div class="map-content-box player">'+
'<div class="map-content-top">'+
'<img src="http://localhost/shoutmehtml/assets/images/client10.jpg">'+
'John Smith <span>21</span>'+
'</div>'+
'<div class="map-content-bottom">'+
'<div class="map-content-info">'+
'<p>It’s hard to beat a person who never gives up</p>'+
'</div>'+
'<div class="map-content-other-info">'+
'<div class="left">'+
'<ul class="sport-list">'+
'<li>'+
'Football'+
'£10'+
'</li>'+
'<li>'+
'Basketball'+
'</li>'+
'</ul>'+
'</div>'+
'<div class="right">'+
'<a href="#" class="btn-custom btn-blue-gradient"><img src="http://localhost/shoutmehtml/assets/images/arrow.svg"></a>'+
'</div>'+
'</div>'+
'</div>'
        		// '<h1 class="firstHeading">'+lon_array[i]+'</h1>'+
        		// '<div id="bodyContent">'+
        		// '<p><b>ICAO:</b>'+lat_array[i]+'.</p>'+
        		// '</div>'+
        		'</div>';
        		var infowindow = new google.maps.InfoWindow
        		(
        		{
        			content: contentString
        		}
        		);

        		var tooltip = new Tooltip({map: map}, marker);
        		tooltip.bindTo("text", marker, "tooltip");
        		google.maps.event.addListener(marker, 'mouseover', function()
        		{
        			tooltip.addTip();
        			tooltip.getPos2(marker.getPosition());
        		});

        		google.maps.event.addListener(marker, 'mouseout', function()
        		{
        			tooltip.removeTip();
        		});
        		
        		google.maps.event.addListener(marker, 'click', function()
        		{
        			infowindow.open(map,marker);
        		});
        		
        		map.fitBounds(bounds);
        	})();
        }
    }

    google.maps.event.addDomListener(window, 'load', initialize);
</script>